#!/usr/bin/env bash

export CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER=x86_64-unknown-linux-gnu-gcc
cargo build --no-default-features --features bot-cli --release --target=x86_64-unknown-linux-gnu

#asset_id=$(uuidgen)
#gsutil cp target/x86_64-unknown-linux-gnu/release/libquonsensus_task.so gs://quoridor/quonsensus/assets/${asset_id}

#echo "Built and deployed as asset_id=${asset_id}"