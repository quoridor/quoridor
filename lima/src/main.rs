use std::{env, thread};
use std::io;
use std::sync::Arc;
use std::time::{Duration, Instant};
use stretto::{Cache, DefaultKeyBuilder};
use quoridor_core::board::{Board, GameMove, GameMoveByPlayer, MoveDirection, Pawn, Wall, WallDirection};
use quoridor_core::iteration_limiter::IterationTimeLimiter;
use quoridor_core::notation::ai_tester::{AiTesterNotationDecoder, AiTesterNotationEncoder};
use quoridor_core::player::{Player, PlayerAction, PlayerId};
use quoridor_core::position::BoardPosition;
use agent_monte_carlo::MonteCarloBot;
use quortex::agent::QuortexBot;
use quortex::nets::t2::QuortexNetT2;
use quortex::nets::net_cache::QuortexNetCacheLocal;

fn main() {
    run();
}

fn run() {
    let time_per_move = env::var("TIME_PER_MOVE")
        .unwrap_or("4700".to_owned())
        .parse()
        .unwrap();

    let white = PlayerId::new(0);
    let black = PlayerId::new(1);

    // first of all, read player id (based on color of the pawn) for this player.
    let this_player_id = read_player_id(&white, &black);

    let start = Instant::now();
    let net = Box::new(QuortexNetT2::best());
    let mut player = QuortexBot::new_with_net(
        this_player_id.clone(),
        Box::new(IterationTimeLimiter::with_custom_first_limit(
            Duration::from_millis(time_per_move),
            Duration::from_millis(((time_per_move as i64) - (Instant::now() - start).as_millis() as i64).max(0) as u64)),
        ),
        net,
        false,
    );

    // origin for white is south
    // origin for black is north
    let mut board = Board::new_for_two_players(
        black,
        white.clone(),
        white
    );

    while board.winner().is_none() {
        let next_move_by = board.next_move_by();

        let next_move = if next_move_by == player.id() {
            let bot_pawn = board.pawns().iter().find(|pawn| pawn.player_id() == &this_player_id).unwrap();
            let bot_move = match player.perform_move(&board) {
                PlayerAction::PerformMove(mv) => mv,
                PlayerAction::Wait => panic!("Bot implementation for lima should not wait for turns"),
                PlayerAction::Error(err) => panic!("Bot returned an error: {:?}", err),
            };

            println!("{}", &bot_move.to_ai_tester_notation(&bot_pawn));

            bot_move
        } else {
            let enemy_pawn = board.pawns().iter().find(|pawn| pawn.player_id() != &this_player_id).unwrap();
            read_move(enemy_pawn)
        };

        board = board.apply_move_unchecked(GameMoveByPlayer {
            player_id: next_move_by.clone(),
            game_move: next_move
        });
    }

    println!("// game finished");
    thread::sleep(Duration::from_secs(60));
}

fn wall_move_to_string(wall: &Wall) -> String {
    format!("wall {}{}", wall_position_to_string(wall.position()), match wall.direction() {
        WallDirection::Vertical => "v",
        WallDirection::Horizontal => "h"
    })
}

fn wall_position_to_string(pos: &BoardPosition) -> String {
    format!("{}{}", match pos.column {
        0 => "S",
        1 => "T",
        2 => "U",
        3 => "V",
        4 => "W",
        5 => "X",
        6 => "Y",
        7 => "Z",
        other => panic!("Unexpected column for wall position: {}", other),
    }, pos.row)
}

fn read_move(pawn: &Pawn) -> GameMove {
    GameMove::from_ai_tester_notation(&read_line(), pawn).unwrap()
}

fn read_player_id(white: &PlayerId, black: &PlayerId) -> PlayerId {
    match read_line().as_str() {
        "white" => white.clone(),
        "black" => black.clone(),
        "" => std::process::exit(0),
        other => panic!("Unknown pawn color: {:?}", other),
    }
}

fn read_line() -> String {
    let mut line = String::new();
    io::stdin().read_line(&mut line).unwrap();
    line.replace("\n", "").trim().to_owned()
}