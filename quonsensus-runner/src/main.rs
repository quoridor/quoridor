use std::{env, fs, thread};
use std::collections::HashMap;
use std::io::Cursor;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};
use std::sync::atomic::AtomicU32;
use std::time::{Duration, Instant};
use std::io;
use agent_external::ExternalBot;
use env_logger::Env;
use log::{info, error, warn};
use rand::prelude::*;
use stretto::{Cache, DefaultKeyBuilder};
use quonsensus_core::{CheckpointMetadata, ConsumeTaskResponse, SubmitCheckpointRequest, SubmitSelfPlayOutcomeRequest, SubmitTournamentOutcomeRequest, Task};
use quoridor_core::board::{Board, Direction, GameMove, GameMoveByPlayer};
use quoridor_core::iteration_limiter::{IterationCountLimiter, IterationTimeLimiter};
use quortex::agent::QuortexBot;
use quortex::nets::t0::QuortexNetT0;
use quoridor_core::player::{Player, PlayerAction, PlayerId};
use rand::distributions::WeightedIndex;
use agent_monte_carlo::MonteCarloBot;
use quortex::nets::net_cache::QuortexNetCacheLocal;
use quortex::nets::t1::QuortexNetT1;
use quortex::nets::t2::QuortexNetT2;
use quortex::nets::t3::QuortexNetT3;
use quortex::nets::t4::QuortexNetT4;
use quortex::nets::t5::QuortexNetT5;
use quortex::quortex::{BoardEvaluation, QuortexNet};
use quortex::tensor_utils::{board_to_tensor, mirror_policy, policy_to_tensor};
use quortex::training_data::TrainingDataEntry;

mod intro;

const MAX_TOTAL_TURNS: u32 = 100;
const RUNNER_VERSION: u32 = 3;

#[derive(Eq, PartialEq)]
enum AssetState {
    NotPresent,
    Downloading,
    Ready
}

fn main() {
    intro::print_intro();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let assets: Arc<Mutex<HashMap<String, AssetState>>> = Arc::new(Mutex::new(HashMap::new()));

    let total_threads = env::var("QUONSENSUS_THREADS").map(|v| v.parse().unwrap()).unwrap_or(num_cpus::get());
    info!("Starting quonsensus, total threads: {}", total_threads);
    let started_at = Instant::now();
    let mut completed_tasks = Arc::new(Mutex::new(0));

    let mut threads = Vec::new();
    for i in 0..total_threads {
        let assets = assets.clone();
        let completed_tasks = completed_tasks.clone();

        threads.push(thread::spawn(move || {
            loop {
                match consume_task() {
                    Ok(v) => match v {
                        ConsumeTaskResponse::NoTasksAvailable => {
                            info!("No tasks available, waiting...");
                            thread::sleep(Duration::from_secs(60));
                        },
                        ConsumeTaskResponse::Ok { task } => {
                            info!("Received task: {:?}", task);
                            if let Err(_) = run_task(&assets, &task) {
                                break;
                            }

                            let mut completed_tasks = completed_tasks.lock().unwrap();
                            *completed_tasks += 1;
                            let seconds_passed = (Instant::now() - started_at).as_secs();
                            info!("Task completed, total: {}, tasks per second: {}", completed_tasks,  *completed_tasks as f32 / seconds_passed as f32);
                        }
                    },
                    Err(err) => {
                        error!("Failed to consume task: {:?}", err);
                        thread::sleep(Duration::from_secs(60));
                    }
                }
            }
        }));
    }

    for thread in threads {
        thread.join();
    }
}

fn run_task(assets: &Arc<Mutex<HashMap<String, AssetState>>>, task: &Task) -> Result<(), ()> {
    match task {
        Task::SelfPlay {
            baseline,
            opponent,
            net_turn_iterations,
        } => run_self_play(assets, baseline, opponent, *net_turn_iterations),
        Task::GenerateSelfPlayDataset {
            checkpoint,
            net_turn_iterations,
            era
        } => run_generate_self_play_dataset(assets, checkpoint, *net_turn_iterations, *era),
        Task::Tournament { opponent } => run_tournament(assets, opponent),
    }
}

fn run_generate_self_play_dataset(assets: &Arc<Mutex<HashMap<String, AssetState>>>, checkpoint: &CheckpointMetadata, net_turn_iterations: u64, era: u32) -> Result<(), ()> {
    let mut training_data = Vec::new();
    let mut started_at = Instant::now();
    let mut games_generated = 0;

    while training_data.len() < 10000 {
        training_data.append(&mut generate_self_play_dataset_entry(assets, checkpoint, net_turn_iterations)?);
        games_generated += 1;
    }

    let source = bincode::serialize(&training_data).unwrap();
    let compressed = zstd::stream::encode_all(Cursor::new(source), 10).unwrap();

    info!("Uploading self-training data batch, total {} games generated", games_generated);
    upload_self_training_data(era, compressed, games_generated);

    training_data.clear();

    Ok(())
}

fn generate_self_play_dataset_entry(assets: &Arc<Mutex<HashMap<String, AssetState>>>, checkpoint: &CheckpointMetadata, net_turn_iterations: u64) -> Result<Vec<TrainingDataEntry>, ()> {
    let player1 = PlayerId::new(0);
    let player2 = PlayerId::new(1);
    let mut board = Board::new_for_two_players(
        player1.clone(),
        player2.clone(),
        if thread_rng().gen_bool(0.5) {
            player1.clone()
        } else {
            player2.clone()
        }
    );

    let north_player_id = board.pawns().iter()
        .find(|pawn| pawn.origin() == &Direction::North)
        .unwrap()
        .player_id()
        .clone();

    let mut net_1 = Box::new(QuortexBot::new_with_net(player1.clone(), Box::new(IterationCountLimiter::new(net_turn_iterations)), net_for_metadata(assets, checkpoint)?, true));
    let mut net_2 = Box::new(QuortexBot::new_with_net(player2.clone(), Box::new(IterationCountLimiter::new(net_turn_iterations)), net_for_metadata(assets, checkpoint)?, true));

    // (training entry, is north player)
    let mut training_data: Vec<(TrainingDataEntry, bool)> = Vec::new();

    while board.winner().is_none() {
        if training_data.len() > MAX_TOTAL_TURNS as usize {
            warn!("Finishing game early, because it reached max turns");
            return Ok(Vec::new());
        }

        let next_move_by = board.next_move_by();

        let bot = if next_move_by == &player1 {
            &mut net_1
        } else {
            &mut net_2
        };

        let (best_move, root_node) = bot.perform_move_and_return_root(&board);
        let mut weights = Vec::new();
        let root_children = root_node.children();
        for child in root_children {
            weights.push(child.visit_count() as f32 / root_node.visit_count() as f32);
        }

        let mv = if board.moves_history().len() <= 16 {
            let dist = WeightedIndex::new(&weights).unwrap();
            let chosen_index = dist.sample(&mut rand::thread_rng());
            root_node.children()[chosen_index].game_move().unwrap().clone()
        } else {
            best_move
        };

        let moves_with_scores = (0..root_children.len()).into_iter()
            .map(|i| (root_children[i].game_move().unwrap().clone(), weights[i]))
            .collect();
        let policy = policy_to_tensor(&moves_with_scores);
        training_data.push((TrainingDataEntry {
            state: if next_move_by == &north_player_id {
                board_to_tensor(&board)
            } else {
                board_to_tensor(&board.mirror())
            },
            policy: if next_move_by == &north_player_id {
                policy
            } else {
                mirror_policy(&policy)
            },
            value: 0.0,
        }, next_move_by == &north_player_id));

        board = board.apply_move_unchecked(GameMoveByPlayer {
            game_move: mv.clone(),
            player_id: next_move_by.clone(),
        });
    }

    let winner = board.pawns().iter()
        .find(|pawn| pawn.player_id() == board.winner().unwrap()).unwrap()
        .origin();
    let value = if winner == &Direction::North {
        -1.0
    } else {
        1.0
    };

    Ok(training_data.into_iter().map(|(entry, is_north)| {
        TrainingDataEntry {
            state: entry.state,
            policy: entry.policy,
            value: if is_north {
                value
            } else {
                -value
            }
        }
    }).collect())
}

fn run_tournament(assets: &Arc<Mutex<HashMap<String, AssetState>>>, opponent: &str) -> Result<(), ()> {
    let baseline_net = Box::new(QuortexNetT2::best());

    let opponent_solution_player_id = PlayerId::new(0);
    let baseline_solution_player_id = PlayerId::new(1);

    let opponent_is_white = rand::thread_rng().gen_bool(0.5);
    let opponent_player: Box<dyn Player> = Box::new(ExternalBot::new(opponent_solution_player_id.clone(), opponent.to_owned(), resolve_external_bot(assets, opponent).unwrap(), opponent_is_white));
    let baseline_player: Box<dyn Player> = Box::new(QuortexBot::new_with_net(baseline_solution_player_id.clone(), Box::new(IterationTimeLimiter::new(Duration::from_millis(4900))), Box::new(QuortexNetT2::best()), false));

    let mut players = if opponent_is_white {
        vec![baseline_player, opponent_player]
    } else {
        vec![opponent_player, baseline_player]
    };

    let mut board = Board::new_for_two_players(
        players[0].id().clone(),
        players[1].id().clone(),
        players[1].id().clone(),
    );

    let mut total_turns = 0;
    let mut error = false;

    while board.winner().is_none() {
        let started_at = Instant::now();
        let next_move_by = board.next_move_by();

        let player_move = {
            let player_for_next_move = players.iter_mut()
                .find(|player| player.id() == next_move_by)
                .unwrap();

            match player_for_next_move.perform_move(&board) {
                PlayerAction::PerformMove(v) => v,
                PlayerAction::Wait => panic!("Bots participating in quolosseum are not supposed to wait!"),
                PlayerAction::Error(err) => {
                    error!("error while performing next move: {:?}", err);
                    error = true;
                    break;
                }
            }
        };

        let player_pawn = board.pawns().iter()
            .find(|pawn| pawn.player_id() == next_move_by)
            .unwrap();
        if next_move_by == players[0].id() {
            if let Err(err) = players[1].on_opponent_move(player_move.clone(), player_pawn) {
                error!("error in on_opponent_move: {:?}", err);
                error = true;
                break;
            }
        } else {
            if let Err(err) = players[0].on_opponent_move(player_move.clone(), player_pawn) {
                error!("error in on_opponent_move: {:?}", err);
                error = true;
                break;
            }
        }

        board = match board.apply_move(
            GameMoveByPlayer::new(next_move_by.clone(), player_move.clone())
        ) {
            Ok(v) => v,
            Err(err) => {
                error!("error while applying move: {:?}", err);
                error = true;
                break;
            },
        };

        total_turns += 1;
        if total_turns >= MAX_TOTAL_TURNS {
            break;
        }
    }

    submit_tournament_outcome(
        opponent.to_owned(),
        1,
        if board.winner().is_some() && board.winner().unwrap() == &opponent_solution_player_id {
            0
        } else {
            1
        },
        if error {
            1
        } else {
            0
        }
    );

    for player in players {
        player.shutdown();
    }

    Ok(())
}

fn run_self_play(assets: &Arc<Mutex<HashMap<String, AssetState>>>, baseline: &CheckpointMetadata, opponent: &CheckpointMetadata, net_turn_iterations: u64) -> Result<(), ()> {
    let baseline_net = net_for_metadata(assets, baseline)?;
    let opponent_net = net_for_metadata(assets, opponent)?;

    let opponent_solution_player_id = PlayerId::new(0);
    let baseline_solution_player_id = PlayerId::new(1);
    let player_1 = Box::new(QuortexBot::new_with_net(opponent_solution_player_id.clone(), Box::new(IterationCountLimiter::new(net_turn_iterations)), opponent_net, true));
    let player_2 = Box::new(QuortexBot::new_with_net(baseline_solution_player_id.clone(), Box::new(IterationCountLimiter::new(net_turn_iterations)), baseline_net, true));
    let mut players: Vec<Box<dyn Player>> = vec![player_1, player_2];

    let mut board = Board::new_for_two_players(
        players[0].id().clone(),
        players[1].id().clone(),
        players[rand::thread_rng().gen_range(0..=1) as usize].id().clone(),
    );

    let mut total_turns = 0;

    while board.winner().is_none() {
        let next_move_by = board.next_move_by();
        let player_for_next_move = players.iter_mut()
            .find(|player| player.id() == next_move_by)
            .unwrap();

        let player_move = match player_for_next_move.perform_move(&board) {
            PlayerAction::PerformMove(v) => v,
            PlayerAction::Wait => panic!("Bots participating in benchmark are not supposed to wait!"),
            PlayerAction::Error(err) => {
                panic!("bot error: {:?}", err);
            }
        };

        board = match board.apply_move(
            GameMoveByPlayer::new(player_for_next_move.id().clone(), player_move.clone())
        ) {
            Ok(v) => v,
            Err(err) => panic!("Failed to apply move ({:?}) to the board: {:?}", player_move, err),
        };

        total_turns += 1;
        if total_turns >= MAX_TOTAL_TURNS {
            break;
        }
    }

    if board.winner().is_some() {
        submit_self_play_outcome(
            baseline.clone(),
            opponent.clone(),
            board.winner().unwrap() == &opponent_solution_player_id,
        );
    }

    Ok(())
}

fn resolve_external_bot(assets: &Arc<Mutex<HashMap<String, AssetState>>>, bot_name: &str) -> Result<String, ()> {
    let asset_id = format!("external-{}", bot_name);

    {
        let mut assets = assets.lock().unwrap();
        if !assets.contains_key(&asset_id) {
            if !asset_exists(&asset_id) {
                assets.insert(asset_id.to_owned(), AssetState::Downloading);
                download_asset(&format!("{}.zip", asset_id))?;
                assets.insert(asset_id.to_owned(), AssetState::Ready);
            } else {
                assets.insert(asset_id.to_owned(), AssetState::Ready);
            }
        }
    }

    while assets.lock().unwrap().get(&asset_id).unwrap_or(&AssetState::Downloading) == &AssetState::Downloading {
        info!("Waiting for asset {} to finish downloading", asset_id);
        thread::sleep(Duration::from_secs(1));
    }

    let subpaths = fs::read_dir(asset_path(&asset_id)).unwrap();
    let subpaths: Vec<PathBuf> = subpaths.into_iter().map(|v| v.unwrap().path()).collect();
    let subpaths = if subpaths.len() == 1 {
        fs::read_dir(&subpaths[0]).unwrap()
    } else {
        fs::read_dir(asset_path(&asset_id)).unwrap()
    };

    if bot_name == "zurich" {
        return Ok(subpaths.into_iter()
            .map(|v| v.unwrap().path().to_string_lossy().to_string())
            .find(|v| v.contains("Controller.exe"))
            .unwrap());
    }

    let exe_path: Vec<String> = subpaths.into_iter()
        .filter(|v| v.as_ref().unwrap().file_name().to_string_lossy().to_string().ends_with(".exe"))
        .map(|v| v.unwrap().path().to_string_lossy().to_string())
        .collect();

    let exe_path = if exe_path.len() == 1 {
        exe_path[0].clone()
    } else {
        let exe_path: Vec<String> = exe_path.iter()
            .filter(|v| !v.to_lowercase().contains("aitester"))
            .cloned()
            .collect();

        if exe_path.len() == 1 {
            exe_path[0].clone()
        } else {
            let ai_tester: Vec<String> = exe_path.iter()
                .filter(|v| v.to_lowercase().contains("aiproject"))
                .cloned()
                .collect();

            if ai_tester.len() == 1 {
                ai_tester[0].clone()
            } else {
                panic!("Could not find executable: {:?}", exe_path);
            }
        }
    };

    Ok(exe_path)
}

fn net_for_metadata(assets: &Arc<Mutex<HashMap<String, AssetState>>>, checkpoint_metadata: &CheckpointMetadata) -> Result<Box<dyn QuortexNet + Send>, ()> {
    let asset_id = &checkpoint_metadata.checkpoint_asset_id;

    {
        let mut assets = assets.lock().unwrap();
        if !assets.contains_key(asset_id) {
            if !asset_exists(&asset_id) {
                assets.insert(asset_id.to_owned(), AssetState::Downloading);
                download_asset(&asset_id)?;
                assets.insert(asset_id.to_owned(), AssetState::Ready);
            } else {
                assets.insert(asset_id.to_owned(), AssetState::Ready);
            }
        }
    }

    while assets.lock().unwrap().get(asset_id).unwrap_or(&AssetState::Downloading) == &AssetState::Downloading {
        info!("Waiting for asset {} to finish downloading", asset_id);
        thread::sleep(Duration::from_secs(1));
    }

    // at this point asset is definitely ready
    let mut net = net_by_name(&checkpoint_metadata.net_name);
    net.load_model(&asset_path(asset_id));

    Ok(net)
}

fn net_by_name(net_name: &str) -> Box<dyn QuortexNet + Send> {
    match net_name {
        "t0" => Box::new(QuortexNetT0::new()),
        "t1" => Box::new(QuortexNetT1::new()),
        "t2" => Box::new(QuortexNetT2::new()),
        "t3" => Box::new(QuortexNetT3::new()),
        "t4" => Box::new(QuortexNetT4::new()),
        "t5" => Box::new(QuortexNetT5::new()),
        other => panic!("Unexpected net name: {:?}", other)
    }
}

fn asset_exists(asset_id: &str) -> bool {
    Path::new(&asset_path(asset_id)).exists()
}

fn download_asset(asset_id: &str) -> Result<(), ()> {
    info!("Downloading asset {}", asset_id);

    let assets_dir = Path::new("assets");
    if !assets_dir.exists() {
        fs::create_dir_all(assets_dir).unwrap();
    }

    let client = reqwest::blocking::Client::new();
    let res = client.get(format!("https://quoridor.nikitavbv.com/quonsensus/assets/{}", asset_id))
        .send()
        .unwrap()
        .bytes()
        .unwrap()
        .to_vec();

    if let Err(err) =  fs::write(asset_path(asset_id), &res) {
        error!("failed to write asset to disk: {:?}", err);
        return Err(());
    }

    if asset_path(asset_id).ends_with(".zip") {
        extract_asset(asset_id);
    }

    Ok(())
}

fn extract_asset(asset_id: &str) {
    let file = fs::File::open(&asset_path(asset_id)).unwrap();
    let mut archive = zip::ZipArchive::new(file).unwrap();

    for i in 0..archive.len() {
        let mut file = archive.by_index(i).unwrap();
        let outpath = match file.enclosed_name() {
            Some(path) => path.to_owned(),
            None => continue,
        };
        let outpath = format!("{}/{}", asset_path(&asset_id.replace(".zip", "")), outpath.to_string_lossy());
        let outpath = Path::new(&outpath);

        {
            let comment = file.comment();
            if !comment.is_empty() {
                println!("File {} comment: {}", i, comment);
            }
        }

        if (&*file.name()).ends_with('/') {
            println!("File {} extracted to \"{}\"", i, outpath.display());
            fs::create_dir_all(&outpath).unwrap();
        } else {
            println!(
                "File {} extracted to \"{}\" ({} bytes)",
                i,
                outpath.display(),
                file.size()
            );
            if let Some(p) = outpath.parent() {
                if !p.exists() {
                    fs::create_dir_all(&p).unwrap();
                }
            }
            let mut outfile = fs::File::create(&outpath).unwrap();
            io::copy(&mut file, &mut outfile).unwrap();
        }
    }
}

fn asset_path(asset_id: &str) -> String {
    format!("assets/{}", asset_id)
}

fn platform() -> String {
    if cfg!(windows) {
        "windows".to_string()
    } else {
        "linux".to_string()
    }
}

fn consume_task() -> Result<ConsumeTaskResponse, String> {
    let client = reqwest::blocking::Client::new();
    let res = client.post("https://quoridor.nikitavbv.com/api/v1/quonsensus/tasks/consume")
        .header("Content-Length", "0")
        .header("x-quonsensus-version", RUNNER_VERSION.to_string())
        .header("x-quonsensus-platform", platform())
        .send()
        .unwrap()
        .text()
        .unwrap();

    match serde_json::from_str(&res) {
        Ok(v) => Ok(v),
        Err(err) => {
            Err(format!("Failed to deserialize consumed task: {:?} for {}", err, res))
        }
    }
}

fn submit_tournament_outcome(opponent: String, total_games: u32, total_wins: u32, errors: u32) {
    let client = reqwest::blocking::Client::new();
    client.post("https://quoridor.nikitavbv.com/api/v1/quonsensus/tournament")
        .header("x-quonsensus-version", RUNNER_VERSION.to_string())
        .header("x-quonsensus-platform", platform())
        .json(&SubmitTournamentOutcomeRequest {
            opponent,
            total_games,
            total_wins,
            errors,
        })
        .send()
        .unwrap();
}

fn submit_self_play_outcome(baseline: CheckpointMetadata, opponent: CheckpointMetadata, outcome: bool) {
    let client = reqwest::blocking::Client::new();
    client.post("https://quoridor.nikitavbv.com/api/v1/quonsensus/self-play")
        .header("x-quonsensus-version", RUNNER_VERSION.to_string())
        .json(&SubmitSelfPlayOutcomeRequest {
            baseline,
            opponent,
            outcome,
        })
        .send()
        .unwrap();
}

fn upload_self_training_data(era: u32, data: Vec<u8>, games_generated: u32) {
    let client = reqwest::blocking::Client::new();

    client.post(format!("https://quoridor-api.nikitavbv.com/api/v1/quonsensus/self-play/{}", era))
        .header("x-quonsensus-version", RUNNER_VERSION.to_string())
        .header("x-quortex-total-games", games_generated.to_string())
        .body(data)
        .send()
        .unwrap();
}