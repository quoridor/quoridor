import {BoardPosition, MoveDirection} from './types';

export const applyDirection = (position: BoardPosition, direction: MoveDirection): BoardPosition => {
    if (direction as string === 'SouthEst') {
        direction = 'SouthEast';
    }

    switch (direction) {
        case 'North':
            return { row: position.row - 1, column: position.column };
        case 'NorthEast':
            return { row: position.row - 1, column: position.column + 1};
        case 'East':
            return { row: position.row, column: position.column + 1 };
        case 'SouthEast':
            return { row: position.row + 1, column: position.column + 1};
        case 'South':
            return { row: position.row + 1, column: position.column };
        case 'SouthWest':
            return { row: position.row + 1, column: position.column - 1 };
        case 'West':
            return { row: position.row, column: position.column - 1};
        case 'NorthWest':
            return { row: position.row - 1, column: position.column - 1};
        case 'JumpNorth':
            return { row: position.row - 2, column: position.column };
        case 'JumpEast':
            return { row: position.row, column: position.column + 2 };
        case 'JumpSouth':
            return { row: position.row + 2, column: position.column };
        case 'JumpWest':
            return { row: position.row, column: position.column - 2};
        default:
            throw new Error('Unknown direction: ' + direction);
    }
}