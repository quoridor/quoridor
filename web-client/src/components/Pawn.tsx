export const Pawn = (props: { pawnStyle: string, clickable?: boolean, onClick?: () => void }) => {
    const style = { backgroundImage: `url('/graphics/pawn-${props.pawnStyle}')`};
    let className = 'pawn';

    if (props.clickable) {
        className += ' pawn-clickable';
    }

    // noinspection CheckTagEmptyBody
    return (<div
        className={className}
        style={style}
        onClick={props.onClick}></div>
    );
};