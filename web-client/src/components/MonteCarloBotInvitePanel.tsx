import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {PlayerOptions} from "../types";

export const MonteCarloBotInvitePanel = (props: { onSelected: (options: PlayerOptions) => void }) => {
    return (
        <div className='invite-panel-entry' onClick={() => props.onSelected({ type: 'MonteCarloBot' })}>
            <FontAwesomeIcon icon={faPlus} color='#575fcf' />
            <div>
                Monte Carlo
                <span>This one is really good</span>
            </div>
        </div>
    );
};