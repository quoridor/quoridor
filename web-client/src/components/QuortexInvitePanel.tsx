import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {PlayerOptions} from "../types";

export const QuortexInvitePanel = (props: { onSelected: (options: PlayerOptions) => void }) => {
    return (
        <div className='invite-panel-entry' onClick={() => props.onSelected({ type: 'Quortex' })}>
            <FontAwesomeIcon icon={faPlus} color='#575fcf' />
            <div>
                Quortex
                <span>AlphaZero-style neural net</span>
            </div>
        </div>
    );
};