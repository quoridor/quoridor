import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {PlayerOptions} from "../types";

export const MinimaxBotInvitePanel = (props: { onSelected: (options: PlayerOptions) => void }) => {
    return (
        <div className='invite-panel-entry' onClick={() => props.onSelected({ type: 'MinimaxBot' })}>
            <FontAwesomeIcon icon={faPlus} color='#575fcf' />
            <div>
                Minimax bot
                <span>Simple implementation of minimax with alpha/beta pruning. We are still working on this one and it is really slow at the moment.</span>
            </div>
        </div>
    );
};