import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {PlayerOptions} from "../types";

export const RandomBotInvitePanel = (props: { onSelected: (options: PlayerOptions) => void }) => {
    return (
        <div className='invite-panel-entry' onClick={() => props.onSelected({ type: 'RandomBot' })}>
            <FontAwesomeIcon icon={faPlus} color='#575fcf' />
            <div>
                Random bot
                <span>Makes moves which are kind of... random</span>
            </div>
        </div>
    );
};