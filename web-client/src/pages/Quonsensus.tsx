import {useEffect, useState} from 'react';
import { Line } from 'react-chartjs-2';

type QuonsensusState = {
    checkpoints: Checkpoint[],
};

type Checkpoint = {
    run_id: string,
    epoch: number,
    value_loss: number,
    policy_loss: number,
    checkpoint_asset_id: string,
    total_games: number,
    total_wins: number,
};

export const QuonsensusPage = () => {
    const [state, setState] = useState<QuonsensusState>({
        checkpoints: [],
    });
    const [selectedModel, setSelectedModel] = useState<string | undefined>(undefined);

    useEffect(() => {
        fetch('https://quoridor-api.nikitavbv.com/api/v1/quonsensus').then(r => r.json()).then(setState);
    }, []);

    if (state.checkpoints.length === 0) {
        return (
            <div>
                loading...
            </div>
        );
    }

    const sortedCheckpoints = state.checkpoints.filter(c => c.total_games > 0).sort((a, b) => {
        const aWinRate = a.total_wins / a.total_games;
        const bWinRate = b.total_wins / b.total_games;
        return bWinRate - aWinRate;
    });

    const models = Array.from(new Set(state.checkpoints.map(checkpoint => checkpoint.run_id)));

    const filteredCheckpoints = state.checkpoints
        .filter(checkpoint => {
            const winRate = checkpoint.total_wins / checkpoint.total_games;
            if (winRate < 0.3 && checkpoint.total_games > 100) {
                return false;
            } else if (winRate < 0.4 && checkpoint.total_games > 200) {
                return false;
            }
            return true;
        }).map(checkpoint => checkpoint.total_games);
    const checkpointAvg = filteredCheckpoints.reduce((a, b) => a + b) / filteredCheckpoints.length;
    const below100 = filteredCheckpoints.filter(t => t < 100).length;

    let charts = [];

    if (selectedModel !== undefined) {
        const thisModelCheckpoints = state.checkpoints
            .filter(checkpoint => checkpoint.run_id === selectedModel)
            .sort((a, b) => a.epoch - b.epoch);

        console.log(thisModelCheckpoints);

        charts.push(<Line data={{
            labels: thisModelCheckpoints.map(checkpoint => checkpoint.epoch),
            datasets: [
                {
                    label: 'winrate',
                    data: thisModelCheckpoints.map(checkpoint => checkpoint.total_wins / checkpoint.total_games),
                    fill: false,
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgba(255, 99, 132, 0.2)',
                }
            ],
        }} />);
        charts.push(<Line data={{
            labels: thisModelCheckpoints.map(checkpoint => checkpoint.epoch),
            datasets: [
                {
                    label: 'value_loss',
                    data: thisModelCheckpoints.map(checkpoint => checkpoint.value_loss),
                    fill: false,
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgba(255, 99, 132, 0.2)',
                }
            ],
        }} />);
        charts.push(<Line data={{
            labels: thisModelCheckpoints.map(checkpoint => checkpoint.epoch),
            datasets: [
                {
                    label: 'policy_loss',
                    data: thisModelCheckpoints.map(checkpoint => checkpoint.policy_loss),
                    fill: false,
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgba(255, 99, 132, 0.2)',
                }
            ],
        }} />);
        charts.push(<Line data={{
            labels: thisModelCheckpoints.map(checkpoint => checkpoint.epoch),
            datasets: [
                {
                    label: 'policy_loss + value_loss',
                    data: thisModelCheckpoints.map(checkpoint => checkpoint.policy_loss + checkpoint.value_loss),
                    fill: false,
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgba(255, 99, 132, 0.2)',
                }
            ],
        }} />);
    } else {
        console.log('selected model is undefined');
    }

    return (
        <div className='simple-page'>
            <h1>Quonsensus</h1>
            <h2>Model overview</h2>
            <select value={selectedModel} onChange={e => setSelectedModel(e.target.value)}>
                { models.map(model => (<option key={`${model}-option-models`}>{ model }</option>)) }
            </select>
            <div style={{padding: '0 400px'}}>
                { charts }
            </div>
            <h2>Stats</h2>
            <div>Average games per checkpoint: {
                checkpointAvg
            }</div>
            <div>Checkpoints with less than 100 games: { below100 }</div>

            <h2>Win rate by checkpoint</h2>
            <table style={{width: '100%'}}>
                <tr>
                    <th>run id</th>
                    <th>epoch</th>
                    <th>total games</th>
                    <th>win rate</th>
                </tr>
                { sortedCheckpoints.map(checkpoint => (
                    <tr>
                        <td style={{fontSize: '15px'}}>{ checkpoint.run_id }</td>
                        <td>{ checkpoint.epoch }</td>
                        <td>{ checkpoint.total_games }</td>
                        <td>{ checkpoint.total_wins / checkpoint.total_games }</td>
                    </tr>
                )) }
            </table>
        </div>
    );
};