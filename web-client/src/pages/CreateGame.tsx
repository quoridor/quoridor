import {faDice} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { API_ENDPOINT } from '../api';

export const CreateGamePage = (props: { location: { search: string } }) => {
    const history = useHistory();
    const [isPublic, setIsPublic] = useState(true);
    const [roomName, setRoomName] = useState('');
    const urlParams = new URLSearchParams(props.location.search);
    const addBot = urlParams.get('add-bot') !== null;
    const addFriend = urlParams.get('add-friend') !== null;

    return (
        <div className='simple-page'>
            <h1>New game</h1>
            <div className='input-block-with-randomize'>
                <input className='new-game-name create-game-control' placeholder={'Game room name...'} value={roomName} onChange={e => setRoomName(e.target.value)} />
                <FontAwesomeIcon icon={faDice} onClick={() => {
                    fetch(`${API_ENDPOINT}/api/v1/names/game`, {
                        method: 'GET',
                    }).then(res => res.text()).then(setRoomName);
                }} />
            </div>
            <div className='create-game-control game-public-private-switch'>
                { isPublic ? (<>
                    Game will be <span>public</span> and available for anyone to join (<button className='link-btn' onClick={() => setIsPublic(false)}>make private instead</button>).
                </>) : (<>
                    Game will be <span>private</span> and your friends will be able to join it using an invite link (<button className='link-btn' onClick={() => setIsPublic(true)}>make public instead</button>).
                </>) }
            </div>
            <button className='create-game-control' onClick={() => {
                fetch(`${API_ENDPOINT}/api/v1/games`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        room_name: roomName,
                        is_public: isPublic
                    })
                }).then(res => res.json()).then(res => {
                    if (addBot) {
                        history.push(`/add-bot/${res.id}?join_token=${res.join_token}`);
                    } else {
                        history.push(`/games/${res.id}?join_token=${res.join_token}` + (addFriend ? '&add-friend=true' : ''));
                    }
                });
            }}>Create</button>
        </div>
    );
};