import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faGamepad, faPlus, faRobot} from '@fortawesome/free-solid-svg-icons';
import { API_ENDPOINT } from '../api';
import { PublicRoomMetadata } from '../types';

const rowStyle = {
    width: '100%',
};

const headerStyle: React.CSSProperties = {
    ...rowStyle, 
    borderTop: undefined 
};

const thStyle = {
    display: 'block',
    width: '100%',
};

export const HomePage = () => {
    const [loadingPublicGames, setLoadingPublicGames] = useState(true);
    const [publicGames, setPublicGames] = useState<PublicRoomMetadata[]>([]);

    useEffect(() => {
        fetch(`${API_ENDPOINT}/api/v1/games`)
            .then(r => r.json())
            .then(res => {
                setLoadingPublicGames(false);
                setPublicGames(res);
            });
    }, []);

    return (
        <div className='page'>
            <div className='empty-space-on-the-left'>
            </div>
            <div className='rooms-table'>
                <table style={{
                    width: '100%',
                }}>
                    <tr style={headerStyle}>
                        <th style={thStyle}>Public rooms</th>
                    </tr>
                    { loadingPublicGames ? (<tr style={headerStyle}>
                        <div className='public-rooms-loading'>Loading public rooms...</div>
                    </tr>) : undefined }
                    { (publicGames.length === 0 && !loadingPublicGames) ? (<tr style={headerStyle}>
                        <div className='no-public-rooms-message'>No public rooms. <Link to='/create'>Create one</Link> to play with your friends online!</div>
                    </tr>) : undefined }
                    {
                        publicGames.map(publicGame => (
                            <tr style={rowStyle}>
                                <td>{ publicGame.name }</td>
                                <td>
                                    { publicGame.can_join ? (<Link className='join-button' to={`/games/${publicGame.id}?join_token=${publicGame.join_token}`}>Join</Link>) : undefined }
                                    <Link className={'spectate-button ' + (publicGame.can_join ? 'spectate-or-join' : 'spectate-only') } to={`/games/${publicGame.id}`}>Spectate</Link>
                                </td>
                            </tr>
                        ))
                    }
                </table>
            </div>
            <div className='side-menu-buttons'>
                <Link className='btn' to='/create'>
                    <FontAwesomeIcon icon={faPlus} />
                    Create a game
                </Link>
                <Link className='btn' to='/create?add-friend=true'>
                    <FontAwesomeIcon icon={faGamepad} />
                    Play vs friend
                </Link>
                <Link className='btn' to='/create?add-bot=true'>
                    <FontAwesomeIcon icon={faRobot} />
                    Play vs computer
                </Link>
            </div>
        </div>
    )
};