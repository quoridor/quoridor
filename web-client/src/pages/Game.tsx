import {useEffect, useReducer, useRef, useState} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faLink, faEye, faRobot, faGamepad, faDice} from '@fortawesome/free-solid-svg-icons'
import {
    GameState,
    GameAction,
    WebsocketMessage,
    PlayerId,
    BoardPosition,
    MoveDirection,
    WallDirection,
    GameBoard,
    GameId,
    PlaceWall,
    GameStateWithBoard,
    GameClientState,
    LocalPlayerProfile,
} from '../types';
import { API_ENDPOINT, WEBSOCKET_ENDPOINT } from '../api';
import { useHistory, useParams } from 'react-router';
import {Pawn as PawnComponent} from '../components';
import { gameStateReducer } from '../reducer';
import { applyDirection } from '../utils';

type GameClientStateWithBoard = GameClientState & { board: GameBoard };

const initialState: GameClientState = {
    game_id: '',
    name: '',
    is_public: false,
    board: null,
    players: [],
    winner: null,
    admin_player: null,
    possible_moves: {},
    join_token: undefined,
    local_players: [],
    game_log: [],
    winner_modal_shown: false,
};

const pawnPacks = {
    'classic': [
        'blue.svg',
        'red.svg',
        'green.svg',
        'black.svg',
        'orange.svg',
    ],
    'emoji': [
        'cat.svg',
        'clown-face.svg',
        'cowboy-hat-face.svg',
        'dog.svg',
        'eggplant.svg',
        'face-with-monocle.svg',
        'face-with-rolling-eyes.svg',
        'face-with-steam-from-nose.svg',
        'face-with-tears-of-joy.svg',
        'face-tongue.svg',
        'fire.svg',
        'flushed-face.svg',
        'gaspar.svg',
        'gemstone.svg',
        'grimacing-face.svg',
        'grinning-face-big-eyes.svg',
        'heart.svg',
        'high-voltage.svg',
        'hugging-face.svg',
        'joker.svg',
        'loudly-crying-face.svg',
        'lion.svg',
        'melchor.svg',
        'monkey.svg',
        'nerd-face.svg',
        'neutral-face.svg',
        'panda.svg',
        'partying-face.svg',
        'peach.svg',
        'pile-of-poo.svg',
        'pouting-face.svg',
        'robot.svg',
        'rocket.svg',
        'santa-claus.svg',
        'slightly-smiling.svg',
        'smiling-face-with-halo.svg',
        'smiling-face-with-sunglasses.svg',
        'smiling-face.svg',
        'star.svg',
        'thinking-face.svg',
        'unicorn.svg',
        'upside-down-face.svg',
        'xmas-tree.svg',
        'smiling-devil.png',
        'blow-kiss.png',
        'fish.png',
        'squirrel.png',
        'crab.png',
        'llama.png',
    ],
    'fun': [
        'rick-roll.gif',
        'party-parrot.gif',
        'cat-vibing.gif',
        'think-about-it.jpeg',
        'hackerman.jpeg',
    ],
    'anime': [
        'anime-1.jpeg',
    ]
};

export const GamePage = (props: { location: { search: string }}) => {
    const history = useHistory();
    const { game_id } = useParams() as { game_id: string };
    const urlParams = new URLSearchParams(props.location.search);
    const joinToken = urlParams.get('join_token');
    const ws = useRef<WebSocket | null>(null);

    const [state, dispatch] = useReducer<(state: GameClientState, action: GameAction) => GameClientState>(
        gameStateReducer,
        loadState(game_id)
    );

    const [makingMove, setMakingMove] = useState(false);
    const [placingWallOrigin, setPlacingWallOrigin] = useState<BoardPosition | undefined>(undefined);
    const [placingWallOrientation, setPlacingWallOrientation] = useState<WallDirection>('Vertical');
    const [chatMessage, setChatMessage] = useState('');
    const gameLogRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        ws.current = new WebSocket(`${WEBSOCKET_ENDPOINT}/api/v1/games/${game_id}/ws`);
        ws.current.onopen = () => {
            console.log('connected to quoridor ws');
        };
        ws.current.onmessage = msg => {
            const message = JSON.parse(msg.data);
            processWebsocketMessage(dispatch, message);
        };
        return () => (ws.current as WebSocket).close();
    }, [game_id]);

    useEffect(() => {
        if (gameLogRef !== null && gameLogRef.current !== null) {
            gameLogRef.current.scrollTop = gameLogRef.current.scrollHeight;
        }
    }, [state.game_log]);

    if (joinToken !== undefined && joinToken !== null) {
        const addFriend = urlParams.get('add-friend') !== null;
        const addingFriend = urlParams.get('adding-friend') !== null;
        const navigateToNext = () => history.push(`/games/${game_id}` + (addFriend ? `?join_token=${joinToken}&adding-friend=true` : ''));

        if (state.join_token === undefined || joinToken !== state.join_token) {
            dispatch({
                type: 'join_token.set',
                joinToken,
            });
        }

        return (<PlayerJoinView 
            gameName={state.name} 
            gameId={game_id} 
            joinToken={joinToken}
            cancellable={urlParams.get('cancellable') !== null}
            onCancel={() => history.push(`/games/${game_id}`)}
            addingMultiple={addFriend || addingFriend}
            multipleStep={addingFriend ? 1 : 0}
            onJoin={(id, name, actionToken) => {
                dispatch({
                    type: 'local_players.add',
                    id,
                    name,
                    actionToken
                });
                navigateToNext();
            }} />);
    }

    let boardComponent: JSX.Element;
    if (state.board !== null) {
        const currentPlayer = state.local_players.find(player => player.id === (state as GameClientStateWithBoard).board.next_move_by);

        boardComponent = (
            <Board 
                state={state as GameClientStateWithBoard}
                makingMove={makingMove}
                setMakingMove={setMakingMove}
                placingWallOrigin={placingWallOrigin}
                setPlacingWallOrigin={setPlacingWallOrigin}
                placingWallDirection={placingWallOrientation}
                setPlacingWallDirection={setPlacingWallOrientation}
                onWallPlaced={(position, direction) => {
                    fetch(`${API_ENDPOINT}/api/v1/games/${ state.game_id }/moves`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            action_token: (currentPlayer as LocalPlayerProfile).actionToken,
                            game_move: {
                                type: 'PlaceWall',
                                position,
                                direction,
                            }
                        })
                    });
                }}/>
        );
    } else {
        let startGameComponent = undefined;
        if (state.local_players.length > 0) {
            if (state.players.length === 2 || state.players.length === 4) {
                const localAdmin = state.local_players.find(player => player.id === state.admin_player);

                if (localAdmin !== undefined) {
                    startGameComponent = (<button onClick={() => startGame(game_id, localAdmin.actionToken)}>Start</button>);
                } else {
                    const adminPlayer = state.players.find(player => player.id === state.admin_player);
                    if (adminPlayer !== undefined) {
                        startGameComponent = (<div>Ask { adminPlayer.name } to start the game</div>);
                    } else {
                        startGameComponent = (<div>Waiting for admin player to join this game...</div>);
                    }
                }
            } else {
                startGameComponent = (<div>To start a game there needs to be either two or four players</div>);
            }
        }

        boardComponent = (<div className='game-status-message'>
            Game has not been started yet
            { startGameComponent }
        </div>);
    }

    const gameLog = (state.game_log || []).map(logEntry => {
        const logEntryType = logEntry.type;
        if (logEntryType === 'game_event') {
            return (<div key={`game-event-${ logEntry.timestamp }`} className='game-log-entry game-event'>
                { logEntry.event }
            </div>);
        } else if (logEntryType === 'chat_message') {
            return (<div key={`game-event-${ logEntry.timestamp }`} className='game-log-entry'>
                { logEntry.author }: { logEntry.message }
            </div>);
        } else {
            throw new Error(`Unexpected log entry type: ${logEntryType}`);
        }
    });

    const messageAuthor = state.local_players.length === 1 ? state.local_players[0] :
        (state.board !== null ?
            state.local_players.find(player => player.id === (state as GameStateWithBoard).board.next_move_by) : undefined);

    const messageInput = messageAuthor !== undefined ? (
        <input placeholder='Send message...'
               value={chatMessage}
               onChange={e => setChatMessage(e.target.value)}
               onKeyDown={e => {
                   if (e.key === 'Enter') {
                       fetch(`${API_ENDPOINT}/api/v1/games/${ state.game_id }/messages`, {
                           method: 'POST',
                           headers: {
                               'Content-Type': 'application/json',
                           },
                           body: JSON.stringify({
                               action_token: messageAuthor.actionToken,
                               message: chatMessage,
                           })
                       });
                       setChatMessage('');
                   }
               }} />) : undefined;

    return (
        <div className='game-container'>
            <GameInfo state={state} />
            <div className='board-container'>
                { boardComponent }
            </div>
            <div className='game-log'>
                <div className='game-log-panel'>
                    <div className='logs' ref={gameLogRef}>
                        { gameLog.length > 0 ? (gameLog) : (<div className='game-log-entry game-event'>Game log is empty...</div>) }
                    </div>
                    { messageInput }
                </div>
            </div>
            { (!state.winner_modal_shown && state.winner !== null) ? (<div className='game-finished-modal'>
                <div className='game-finished-modal-content'>
                    <h2>Game Finished</h2>
                    <span className='winner-name'>{ state.players.find(player => player.id === state.winner)?.name || '???' } wins!</span>
                    <button onClick={() => dispatch({ type: 'winner_modal.shown' })}>Ok</button>
                </div>
            </div>) : undefined }
        </div>
    );
};

const GameInfo = (props: { state: GameClientState }) => {
    const history = useHistory();
    const [inviteLinkCopied, setInviteLinkCopied] = useState(false);
    const [joinLinkCopied, setJoinLinkCopied] = useState(false);

    const localAdmin = props.state.local_players.find(player => player.id === props.state.admin_player);
    const canInvitePlayers = (localAdmin !== undefined || (props.state.is_public && props.state.join_token !== undefined))
        && (props.state.board === null || props.state.winner !== null) && props.state.players.length < 4;

    let nextTurnMessage: JSX.Element;
    if (props.state.board === null) {
        nextTurnMessage = (<td>Game has not been started yet</td>);
    } else {
        const currentPlayer = props.state.local_players.find(player => player.id === (props.state as GameClientStateWithBoard).board.next_move_by);

        if (props.state.winner !== null) {
            nextTurnMessage = (<td>Game finished</td>);
        } else if (currentPlayer !== undefined && props.state.local_players.length === 1) {
            nextTurnMessage = (<td>Your turn</td>);
        } else if (currentPlayer !== undefined && props.state.local_players.length > 1) {
            nextTurnMessage = (<td>Your turn ({ currentPlayer.name })</td>);
        } else {
            nextTurnMessage = (<td>Next turn by { props.state.players.find(player => player.id === (props.state.board as GameBoard).next_move_by)?.name }</td>);
        }
    }

    return (
        <div className='game-info'>
            <table>
                <tr>
                    <th>{ props.state.name }</th>
                </tr>
                <tr className='turn-row'>
                    { nextTurnMessage }
                </tr>
                { props.state.players.map(player => (
                    <tr className='player-row'>
                        <td className='player-pawn'>
                            <PawnComponent pawnStyle={player.pawn_style} />
                        </td>
                        <td className='player-name'>{ player.name }</td>
                        { props.state.board !== null ? (<td className='remaining-bricks'>
                            <img src='/graphics/brick.svg' alt='remaining walls' />
                            { props.state.board.remaining_walls[player.id] }
                        </td>) : undefined }
                    </tr>
                )) }
            </table>

            { props.state.winner !== null ? (localAdmin !== undefined ? (
                <button className={'play-again-btn'} onClick={() => startGame(props.state.game_id, localAdmin.actionToken)}>Play again</button>
            ) : (<div className={'play-again-message'}>Ask { props.state.players.find(player => player.id === props.state.admin_player)?.name || '???' } to restart the game</div>)) : undefined }
            <div className='invite-links'>
                { canInvitePlayers ? (<>
                    <button onClick={() => history.push(`/add-bot/${ props.state.game_id }?join_token=${ props.state.join_token }&cancellable=true&skip-adding-local-player=true`) }>
                        <FontAwesomeIcon icon={faRobot}/>
                            Add bot
                        </button>
                        <button onClick={() => history.push(`/games/${ props.state.game_id }?join_token=${ props.state.join_token }&cancellable=true`)}>
                            <FontAwesomeIcon icon={faGamepad} />
                            Add local player
                        </button>
                        <button onClick={() => {
                            const inviteLink = `${ window.location.protocol}//${ window.location.host }/games/${ props.state.game_id }?join_token=${ props.state.join_token }`;
                            navigator.clipboard.writeText(inviteLink).then(() => {
                                setInviteLinkCopied(true);
                                setTimeout(() => setInviteLinkCopied(false), 3000);
                            });
                        }}>
                            <FontAwesomeIcon icon={faLink} />
                            { inviteLinkCopied ? 'Copied to clipboard!' : 'Invite friend link' }
                        </button>
                </>) : undefined }
                <button onClick={() => {
                    const joinLink = `${ window.location.protocol}//${ window.location.host }/games/${ props.state.game_id }`;
                    navigator.clipboard.writeText(joinLink).then(() => {
                        setJoinLinkCopied(true);
                        setTimeout(() => setJoinLinkCopied(false), 3000);
                    });
                }}>
                    <FontAwesomeIcon icon={faEye} />
                    { joinLinkCopied ? 'Copied to clipboard!' : 'Spectate link' }
                </button>
            </div>
        </div>
    );
};

const PlayerJoinView = (props: {
    gameName: string,
    gameId: string,
    joinToken: string,
    cancellable: boolean,
    addingMultiple: boolean,
    multipleStep: number,
    onCancel: () => void,
    onJoin: (id: PlayerId, name: string, actionToken: string) => void
}) => {
    const [playerName, setPlayerName] = useState('');
    const [pawnStyle, setPawnStyle] = useState(selectRandomPawnStyle(pawnPacks['classic']));
    const [selectingPawn, setSelectingPawn] = useState(false);

    useEffect(() => {
        setPawnStyle(selectRandomPawnStyle(pawnPacks['classic']));
        setPlayerName('');
    }, [props.multipleStep]);

    if (selectingPawn) {
        const onStyleSelected = (style: string) => {
            setPawnStyle(style);
            setSelectingPawn(false);
        };

        return (<div className='simple-page'>
            <h1>Select your pawn</h1>
            <PawnPack name={'Classic'} styles={pawnPacks['classic']} onStyleSelected={onStyleSelected} />
            <PawnPack name={'Emoji'} styles={pawnPacks['emoji']} onStyleSelected={onStyleSelected} />
            <PawnPack name={'Fun'} styles={pawnPacks['fun']} onStyleSelected={onStyleSelected} />
            <PawnPack name={'Anime'} styles={pawnPacks['anime']} onStyleSelected={onStyleSelected} />
            <button className='join-game-control' onClick={() => setSelectingPawn(false)}>Cancel</button>
        </div>);
    }

    return (
        <div className='simple-page'>
            <h1>
                Join { props.gameName !== '' ? props.gameName : 'game' }
                { props.addingMultiple ? (` - player ${ props.multipleStep + 1 }/2`) : undefined }
            </h1>
            <div className='input-block-with-randomize'>
                <PawnComponent pawnStyle={pawnStyle} onClick={() => setSelectingPawn(true)} />
                <input
                    className='join-game-control player-name-input'
                    placeholder='Your name...'
                    value={playerName}
                    onChange={e => setPlayerName(e.target.value)} />
                <FontAwesomeIcon icon={faDice} onClick={() => {
                    fetch(`${API_ENDPOINT}/api/v1/names/player`, {
                        method: 'GET',
                    }).then(res => res.text()).then(setPlayerName);
                }} />
            </div>
            <div className='join-game-control-row'>
                { props.cancellable ? (<button className='cancel-button' onClick={() => props.onCancel()}>Cancel</button>) : undefined }
                <button onClick={() => {
                    fetch(`${API_ENDPOINT}/api/v1/games/${props.gameId}/join`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            join_token: props.joinToken,
                            player: {
                                type: 'Human',
                                name: playerName,
                                pawn_style: pawnStyle,
                            }
                        })
                    }).then(res => res.json()).then(res => props.onJoin(res.player_id, playerName, res.action_token));
                }}>Join</button>
            </div>
        </div>
    );
};

const PawnPack = (props: { name: string, styles: string[], onStyleSelected: (style: string) => void }) => {
    return (
        <div className='pawn-pack'>
            <h2>{ props.name } pack</h2>
            { props.styles.map(style => (<PawnComponent
                pawnStyle={style}
                onClick={() => props.onStyleSelected(style)} />)) }
        </div>
    )
}

const BoardCell = (props: { 
    state: GameClientStateWithBoard,
    row: number, 
    column: number, 
    makingMove: boolean,
    setMakingMove: (makingMove: boolean) => void 
}) => {
    const pawn = props.state.board.pawns
        .find(pawn => pawn.position.column === props.column && pawn.position.row === props.row);
    const currentPlayer = props.state.local_players.find(player => player.id === props.state.board.next_move_by);

    let pawnComponent = undefined;
    if (pawn !== undefined) {
        const pawnPlayer = props.state.players.find(player => player.id === pawn.player_id);
        if (pawnPlayer === undefined) {
            throw new Error(`Could not find player by id for pawn: ${pawn.player_id}`);
        }

        const currentPlayerPawn = currentPlayer !== undefined && pawn.player_id === currentPlayer.id;
        let onClickAction = () => {};

        if (currentPlayerPawn) {
            onClickAction = () => props.setMakingMove(!props.makingMove);
        }

        pawnComponent = (<PawnComponent
            pawnStyle={pawnPlayer.pawn_style}
            clickable={currentPlayerPawn}
            onClick={onClickAction} />);
    }

    let possibleMoveComponent = undefined;
    if (currentPlayer !== undefined && props.makingMove) {
        const currentPlayerPawn = props.state.board.pawns.find(pawn => pawn.player_id === currentPlayer.id);
        if (currentPlayerPawn !== undefined) {
            const nextPositionInThisCell = (props.state.possible_moves[currentPlayer.id] || [])
                .filter(move => move.type === 'MovePawn')
                .map(move => move.direction)
                .map(direction => [ direction, applyDirection(currentPlayerPawn.position, direction as MoveDirection) ])
                .find(t => (t[1] as BoardPosition).column === props.column && (t[1] as BoardPosition).row === props.row);
            if (nextPositionInThisCell) {
                const makeAMove = () => {
                    props.setMakingMove(false);
                    fetch(`${API_ENDPOINT}/api/v1/games/${ props.state.game_id }/moves`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            action_token: currentPlayer.actionToken,
                            game_move: {
                                type: 'MovePawn',
                                direction: nextPositionInThisCell[0]
                            }
                        })
                    });
                };
                possibleMoveComponent = (<div className='possible-move' onClick={makeAMove}></div>);
            }
        }
    }

    return (<div className='cell'>
        { pawnComponent }
        { possibleMoveComponent }
    </div>);
};

const BoardCellBorderV = (props: { 
    state: GameClientStateWithBoard,
    row: number,
    column: number,
    makingMove: boolean,
    placingWallOrigin: BoardPosition | undefined, 
    setPlacingWallOrigin: (wallOrigin: BoardPosition | undefined) => void,
    placingWallDirection: WallDirection,
    setPlacingWallDirection: (wallDirection: WallDirection) => void,
    onWallPlaced: (position: BoardPosition, direction: WallDirection) => void,
}) => {
    const thisPosition = { column: props.column, row: props.row };
    const currentPlayer = props.state.local_players.find(player => player.id === props.state.board.next_move_by);
    const currentPlayerHasWallsRemaining = currentPlayer !== undefined ? props.state.board.remaining_walls[currentPlayer.id] > 0 : false;
    const bottomCell = { column: thisPosition.column, row: thisPosition.row + 1 };
    const wall = props.state.board.walls.find(wall => wall.direction === 'Vertical' &&
        (positionsEqual(wall.position, thisPosition) || positionsEqual(wall.position, bottomCell)));

    let className = 'cell-border-v';
    if (currentPlayerHasWallsRemaining && !props.makingMove && props.placingWallOrigin !== undefined && props.placingWallDirection === 'Vertical') {
        const aboveOrigin = { column: props.placingWallOrigin.column, row: props.placingWallOrigin.row - 1 };
        if (positionsEqual(thisPosition, props.placingWallOrigin) || positionsEqual(thisPosition, aboveOrigin)) {
            className += ' cell-border-wall cell-border-placeable';
        }
    }

    if (wall !== undefined) {
        className += ' cell-border-wall';
    }

    const canPlaceWallHere = currentPlayerHasWallsRemaining && currentPlayer !== undefined &&
        props.state.possible_moves[currentPlayer.id]
            .filter(possibleMove => possibleMove.type === 'PlaceWall')
            .filter(placeWallMove => placeWallMove.direction === 'Vertical')
            .find(placeWallMove => positionsEqual((placeWallMove as PlaceWall).position, thisPosition)) !== undefined;
    const mouseEnterListener = canPlaceWallHere ? () => {
        props.setPlacingWallOrigin(thisPosition);
        props.setPlacingWallDirection('Vertical');
    } : undefined;
    const mouseLeaveListener = canPlaceWallHere ? () => {
        props.setPlacingWallOrigin(undefined);
    } : undefined;
    const onClickListener = canPlaceWallHere ?
        () => props.onWallPlaced(thisPosition, 'Vertical') :
        undefined;

    // noinspection CheckTagEmptyBody
    return (
        <div className={className}
            onMouseEnter={mouseEnterListener}
            onMouseLeave={mouseLeaveListener}
            onClick={onClickListener}></div>
    );
}

const BoardCellBorderH = (props: {
    state: GameClientStateWithBoard,
    row: number,
    column: number,
    makingMove: boolean,
    placingWallOrigin: BoardPosition | undefined,
    setPlacingWallOrigin: (wallOrigin: BoardPosition | undefined) => void,
    placingWallDirection: WallDirection,
    setPlacingWallDirection: (wallDirection: WallDirection) => void,
    onWallPlaced: (position: BoardPosition, direction: WallDirection) => void,
}) => {
    const thisPosition = { column: props.column, row: props.row };
    const currentPlayer = props.state.local_players.find(player => player.id === props.state.board.next_move_by)
    const currentPlayerHasWallsRemaining = currentPlayer !== undefined ? props.state.board.remaining_walls[currentPlayer.id] > 0 : false;
    const leftCell = { column: thisPosition.column - 1, row: thisPosition.row };
    const wall = props.state.board.walls.find(wall => wall.direction === 'Horizontal' &&
        (positionsEqual(wall.position, thisPosition) || positionsEqual(wall.position, leftCell)));

    let className = 'cell-border-h';
    if (currentPlayerHasWallsRemaining && !props.makingMove && props.placingWallOrigin !== undefined && props.placingWallDirection === 'Horizontal') {
        const originRightCell = { column: props.placingWallOrigin.column + 1, row: props.placingWallOrigin.row };
        if (positionsEqual(thisPosition, props.placingWallOrigin) || positionsEqual(thisPosition, originRightCell)) {
            className += ' cell-border-wall cell-border-placeable';
        }
    }

    if (wall !== undefined) {
        className += ' cell-border-wall';
    }

    const canPlaceWallHere = currentPlayerHasWallsRemaining && currentPlayer !== undefined &&
        props.state.possible_moves[currentPlayer.id]
            .filter(possibleMove => possibleMove.type === 'PlaceWall')
            .filter(placeWallMove => placeWallMove.direction === 'Horizontal')
            .find(placeWallMove => positionsEqual((placeWallMove as PlaceWall).position, thisPosition)) !== undefined;

    // noinspection CheckTagEmptyBody
    return (
        <div className={className}
            onMouseEnter={canPlaceWallHere ? () => {
                props.setPlacingWallOrigin(thisPosition);
                props.setPlacingWallDirection('Horizontal');
            } : undefined}
            onMouseLeave={canPlaceWallHere ?
                () => props.setPlacingWallOrigin(undefined) :
                undefined}
            onClick={canPlaceWallHere ?
                () => props.onWallPlaced(thisPosition, 'Horizontal') :
                undefined}></div>
    );
};

const BoardCellBorderHV = (props: {
    state: GameClientStateWithBoard,
    row: number,
    column: number,
    makingMove: boolean,
    placingWallOrigin: BoardPosition | undefined,
    setPlacingWallOrigin: (wallOrigin: BoardPosition | undefined) => void,
}) => {
    const thisPosition = { column: props.column, row: props.row };
    const currentPlayer = props.state.local_players.find(player => player.id === props.state.board.next_move_by);
    const bottomCell = { column: thisPosition.column, row: thisPosition.row  };
    const leftCell = { column: thisPosition.column, row: thisPosition.row };
    const verticalWall = props.state.board.walls.find(wall => wall.direction === 'Vertical' &&
        (positionsEqual(wall.position, thisPosition) || positionsEqual(wall.position, bottomCell)));
    const horizontalWall = props.state.board.walls.find(wall => wall.direction === 'Horizontal' &&
        (positionsEqual(wall.position, thisPosition) || positionsEqual(wall.position, leftCell)));

    let className = 'cell-border-hv ';
    if (currentPlayer !== undefined && !props.makingMove && props.placingWallOrigin !== undefined && positionsEqual(thisPosition, props.placingWallOrigin)) {
        className += ' cell-border-wall';
    }

    if (verticalWall !== undefined || horizontalWall !== undefined) {
        className += ' cell-border-wall';
    }

    // noinspection CheckTagEmptyBody
    return (
        <div className={className}></div>
    );
};

const BoardCellRow = (props: { 
    state: GameClientStateWithBoard,
    row: number, 
    makingMove: boolean,
    setMakingMove: (makingMove: boolean) => void,
    placingWallOrigin: BoardPosition | undefined,
    setPlacingWallOrigin: (wallOrigin: BoardPosition | undefined) => void,
    placingWallDirection: WallDirection,
    setPlacingWallDirection: (wallDirection: WallDirection) => void,
    onWallPlaced: (position: BoardPosition, direction: WallDirection) => void,
}) => {
    const row = [];

    for (let i = 0; i < 9; i++) {
        if (i !== 0) {
            row.push(<BoardCellBorderV
                state={props.state}
                row={props.row} 
                column={i - 1}
                makingMove={props.makingMove}
                placingWallOrigin={props.placingWallOrigin}
                setPlacingWallOrigin={props.setPlacingWallOrigin}
                placingWallDirection={props.placingWallDirection}
                setPlacingWallDirection={props.setPlacingWallDirection}
                onWallPlaced={props.onWallPlaced} />);
        }
        row.push(<BoardCell 
            state={props.state}
            row={props.row} 
            column={i} 
            makingMove={props.makingMove}
            setMakingMove={props.setMakingMove} />);
    }

    return (
        <div className='board-row'>
            { row }
        </div>
    );
};

const BoardCellBorderRow = (props: {
    state: GameClientStateWithBoard,
    row: number,
    makingMove: boolean,
    placingWallOrigin: BoardPosition | undefined,
    setPlacingWallOrigin: (wallOrigin: BoardPosition | undefined) => void,
    placingWallDirection: WallDirection,
    setPlacingWallDirection: (wallDirection: WallDirection) => void,
    onWallPlaced: (position: BoardPosition, direction: WallDirection) => void,
}) => {
    const row = [];

    for (let i = 0; i < 9; i++) {
        if (i !== 0) {
            row.push(<BoardCellBorderHV
                state={props.state}
                row={props.row}
                column={i - 1}
                makingMove={props.makingMove}
                placingWallOrigin={props.placingWallOrigin}
                setPlacingWallOrigin={props.setPlacingWallOrigin} />);
        }
        row.push(<BoardCellBorderH
            state={props.state}
            row={props.row}
            column={i}
            makingMove={props.makingMove}
            placingWallOrigin={props.placingWallOrigin}
            setPlacingWallOrigin={props.setPlacingWallOrigin}
            placingWallDirection={props.placingWallDirection}
            setPlacingWallDirection={props.setPlacingWallDirection}
            onWallPlaced={props.onWallPlaced} />);
    }

    return (
        <div className='board-row'>{ row }</div>
    );
};

const Board = (props: { 
    state: GameClientStateWithBoard,
    makingMove: boolean,
    setMakingMove: (makingMove: boolean) => void,
    placingWallOrigin: BoardPosition | undefined,
    setPlacingWallOrigin: (wallOrigin: BoardPosition | undefined) => void,
    placingWallDirection: WallDirection,
    setPlacingWallDirection: (wallDirection: WallDirection) => void,
    onWallPlaced: (position: BoardPosition, direction: WallDirection) => void,
}) => {
    const rows = [];

    for (let i = 0; i < 9; i++) {
        if (i !== 0) {
            rows.push(<BoardCellBorderRow
                state={props.state}
                row={i}
                makingMove={props.makingMove}
                placingWallOrigin={props.placingWallOrigin}
                setPlacingWallOrigin={props.setPlacingWallOrigin}
                placingWallDirection={props.placingWallDirection}
                setPlacingWallDirection={props.setPlacingWallDirection}
                onWallPlaced={props.onWallPlaced} />);
        }
        rows.push(<BoardCellRow 
            state={props.state}
            row={i} 
            makingMove={props.makingMove}
            setMakingMove={props.setMakingMove}
            placingWallOrigin={props.placingWallOrigin}
            setPlacingWallOrigin={props.setPlacingWallOrigin}
            placingWallDirection={props.placingWallDirection}
            setPlacingWallDirection={props.setPlacingWallDirection}
            onWallPlaced={props.onWallPlaced} />);
    }

    return (
        <div className='board'>
            { rows }
        </div>
    );
};

type ReducerDispatch = (action: GameAction) => void;

const loadState = (gameId: GameId): GameClientState => {
    const state = localStorage.state !== undefined ? JSON.parse(localStorage.state) : undefined;
    if (state === undefined || state.game_id !== gameId) {
        return { ...initialState, game_id: gameId };
    } else {
        return state;
    }
};

const processWebsocketMessage = (dispatch: ReducerDispatch, message: WebsocketMessage) => {
    const messageType = message.type;

    switch (messageType) {
        case 'GameStateUpdated':
            dispatch({
                type: 'state.set',
                state: { ...message, type: undefined } as GameState,
            });
            break;
        case 'GameEvent':
            dispatch({
                type: 'state.event',
                event: message
            });
            break;
        default:
            console.error('Unexpected message type:', messageType);
    }
}

const startGame = (game_id: string, action_token: string) => {
    fetch(`${API_ENDPOINT}/api/v1/games/${game_id}/start`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            action_token,
        }),
    }).then(res => res.json());
};

const positionsEqual = (a: BoardPosition, b: BoardPosition): boolean =>
    a.column === b.column && a.row === b.row;

const selectRandomPawnStyle = (styles: string[]): string =>
    styles[Math.floor(Math.random() * styles.length)];