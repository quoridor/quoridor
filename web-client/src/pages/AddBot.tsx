import {MinimaxBotInvitePanel, RandomBotInvitePanel, MonteCarloBotInvitePanel, QuortexInvitePanel} from '../components';
import {useParams} from 'react-router';
import {API_ENDPOINT} from '../api';
import {PlayerOptions} from '../types';
import {useHistory} from 'react-router-dom';

export const AddBotPage = (props: { location: { search: string } }) => {
    const history = useHistory();
    const { game_id } = useParams() as { game_id: string };
    const urlParams = new URLSearchParams(props.location.search);
    const joinToken = urlParams.get('join_token');
    const cancellable = urlParams.get('cancellable') !== null;
    const skipAddingLocalPlayer = urlParams.get('skip-adding-local-player') !== null;

    const navigateToNextPage = () =>
        history.push(skipAddingLocalPlayer ? `/games/${ game_id }` : `/games/${ game_id }?join_token=${joinToken}`);

    const onBotSelected = (player: PlayerOptions) => {
        fetch(`${API_ENDPOINT}/api/v1/games/${game_id}/join`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                join_token: joinToken,
                player,
            })
        });
        navigateToNextPage();
    };

    return (
        <div className='simple-page'>
            <h1>Add bot</h1>
            <div className='create-game-control add-player-control'>
                <RandomBotInvitePanel onSelected={onBotSelected} />
                <MinimaxBotInvitePanel onSelected={onBotSelected} />
                <MonteCarloBotInvitePanel onSelected={onBotSelected} />
                <QuortexInvitePanel onSelected={onBotSelected} />
            </div>
            { cancellable ? (<button className='create-game-control cancel-button' onClick={() => navigateToNextPage()}>Cancel</button>) : undefined }
        </div>
    );
};