export type GameClientState = GameState & {
    game_id: GameId,
    join_token: string | undefined,
    local_players: LocalPlayerProfile[],
    game_log: GameLogEntry[],
    winner_modal_shown: boolean,
};

export type LocalPlayerProfile = {
    id: PlayerId,
    name: string,
    actionToken: string,
};

export type GameLogEntry = { timestamp: number } & (GameEventGameLogEntry | ChatMessageGameLogEntry);
export type GameEventGameLogEntry = {
    type: 'game_event',
    event: string,
};
export type ChatMessageGameLogEntry = {
    type: 'chat_message',
    author: string,
    message: string,
};

// home
export type PublicRoomMetadata = {
    id: GameId,
    name: string,
    can_join: boolean,
    join_token: JoinToken | null,
};
export type GameId = string;
export type JoinToken = string;

// game
export type GameAction = SetJoinTokenAction | SetStateAction | GameEventAction | AddLocalPlayerAction | WinnerModalShown;
export type SetJoinTokenAction = {
    type: 'join_token.set',
    joinToken: string,
};
export type SetStateAction = {
    type: 'state.set',
    state: GameState,
};
export type GameEventAction = {
    type: 'state.event',
    event: WebsocketEvent,
};
export type AddLocalPlayerAction = {
    type: 'local_players.add',
    id: PlayerId,
    name: string,
    actionToken: string,
};
export type WinnerModalShown = {
    type: 'winner_modal.shown',
};

export type WebsocketMessage = GameStateUpdatedMessage | WebsocketEvent;
export type WebsocketEvent = PlayerJoinedEvent | GameStartedEvent | GameMoveByPlayerEvent | ChatMessageEvent
    | GameFinishedEvent;

export type GameStateUpdatedMessage = {
    type: 'GameStateUpdated',
    event: undefined,
} & GameState;

export type GameEvent = {
    type: 'GameEvent',
};

export type PlayerJoinedEvent = GameEvent & {
    event: 'PlayerJoined',
    player: Player,
    admin_player: PlayerId | null,
};

export type GameStartedEvent = GameEvent & {
    event: 'GameStarted',
    board: GameBoard,
    possible_moves: { [key: PlayerId]: GameMove[] },
};

export type GameMoveByPlayerEvent = GameEvent & {
    event: 'GameMoveByPlayer',
    game_move: GameMoveByPlayer,
    remaining_walls: number,
    next_player: PlayerId,
    possible_moves_for_next_player: GameMove[],
};

export type ChatMessageEvent = GameEvent & {
    event: 'ChatMessage',
    author: PlayerId,
    message: string,
};

export type GameFinishedEvent = GameEvent & {
    event: 'GameFinished',
    winner: PlayerId,
};

export type GameMoveByPlayer = {
    player_id: PlayerId,
    game_move: GameMove,
};

export type GameState = {
    name: string,
    is_public: boolean,
    players: Player[],
    admin_player: PlayerId | null,
    winner: PlayerId | null,
    board: GameBoard | null,
    possible_moves: { [key: PlayerId]: GameMove[] },
};

export type GameStateWithBoard = GameState & { board: GameBoard };

export type PlayerId = string;

export type GameBoard = {
    next_move_by: PlayerId,
    pawns: Pawn[],
    remaining_walls: RemainingWalls;
    walls: Wall[],
};

export type Player = {
    id: PlayerId,
    name: string,
    pawn_style: string,
};

export type Pawn = {
    player_id: PlayerId,
    origin: Direction,
    position: BoardPosition,
};

export type Wall = {
    direction: WallDirection,
    position: BoardPosition,
};

export type RemainingWalls = {
    [key: PlayerId]: number,
};

export type Direction = 'North' | 'South' | 'East' | 'West';
export type MoveDirection = Direction | 'NorthEast' | 'NorthWest' | 'SouthEast' | 'SouthWest'
    | 'JumpNorth' | 'JumpEast' | 'JumpSouth' | 'JumpWest';
export type WallDirection = 'Horizontal' | 'Vertical';

export type BoardPosition = {
    column: number,
    row: number,
};

export type GameMove = MovePawn | PlaceWall;
export type MovePawn = {
    type: 'MovePawn',
    direction: MoveDirection,
};
export type PlaceWall = {
    type: 'PlaceWall',
    position: BoardPosition,
    direction: WallDirection,
};

// bots
export type PlayerOptions = HumanPlayerOptions | RandomBotPlayerOptions | MinimaxBotPlayerOptions | MonteCarloBotPlayerOptions | QuortexPlayerOptions;

export type HumanPlayerOptions = {
    type: 'Human',
    name: string,
    pawn_style: string,
};

export type RandomBotPlayerOptions = {
    type: 'RandomBot',
};

export type MinimaxBotPlayerOptions = {
    type: 'MinimaxBot',
};

export type MonteCarloBotPlayerOptions = {
    type: 'MonteCarloBot',
};

export type QuortexPlayerOptions = {
    type: 'Quortex',
}
