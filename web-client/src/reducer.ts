import {GameAction, WebsocketEvent, GameClientState, Pawn, GameMoveByPlayerEvent, MoveDirection} from './types';
import {applyDirection} from './utils';

export const gameStateReducer = (state: GameClientState, action: GameAction): GameClientState => {
    switch (action.type) {
        case 'join_token.set':
            return {
                ...state,
                join_token: action.joinToken,
            };
        case 'state.set':
            return { ...state, ...action.state };
        case 'state.event':
            return reduceWithGameEvent(state, action.event);
        case 'local_players.add':
            return saveState({
                ...state,
                local_players: [
                    ...state.local_players,
                    {
                        id: action.id,
                        name: action.name,
                        actionToken: action.actionToken
                    }
                ]
            });
        case 'winner_modal.shown':
            return {
                ...state,
                winner_modal_shown: true,
            };
        default:
            return state;
    }
};

const reduceWithGameEvent = (state: GameClientState, event: WebsocketEvent): GameClientState => {
    const eventType = event.event;
    switch (eventType) {
        case 'PlayerJoined':
            return {
                ...state,
                players: [...state.players, event.player],
                admin_player: event.admin_player,
                game_log: [ ...state.game_log, {
                    timestamp: new Date().getTime(),
                    type: 'game_event',
                    event: `${ event.player.name } has joined`,
                } ],
            };
        case 'GameStarted':
            return {
                ...state,
                board: event.board,
                winner: null,
                winner_modal_shown: false,
                possible_moves: event.possible_moves,
                game_log: [ ...state.game_log, {
                    timestamp: new Date().getTime(),
                    type: 'game_event',
                    event: 'New game has started',
                } ],
            };
        case 'GameMoveByPlayer':
            const gameMoveType = event.game_move.game_move.type;
            switch (gameMoveType) {
                case 'PlaceWall':
                    const prevBoard = (state.board || { remaining_walls: {}, walls: [], pawns: [] });

                    return {
                        ...state,
                        board: {
                            ...prevBoard,
                            next_move_by: event.next_player,
                            walls: [ ...prevBoard.walls, {
                                position: event.game_move.game_move.position,
                                direction: event.game_move.game_move.direction
                            } ],
                            remaining_walls: {
                                ...prevBoard.remaining_walls,
                                [ event.game_move.player_id ]: event.remaining_walls,
                            },
                        },
                        possible_moves: {
                            ...state.possible_moves,
                            [event.next_player]: event.possible_moves_for_next_player,
                        },
                    };
                case 'MovePawn':
                    return {
                        ...state,
                        board: {
                            ...(state.board || { remaining_walls: {}, walls: [] }),
                            next_move_by: event.next_player,
                            pawns: (state.board || { pawns: [] }).pawns.map(pawn => reducePawn(pawn, event)),
                        },
                        possible_moves: {
                            ...state.possible_moves,
                            [event.next_player]: event.possible_moves_for_next_player,
                        }
                    };
                default:
                    console.error('unexpected game move type', gameMoveType);
                    return state;
            }
        case 'ChatMessage':
            return {
                ...state,
                game_log: [
                    ...state.game_log,
                    {
                        type: 'chat_message',
                        timestamp: new Date().getTime(),
                        author: state.players.find(player => player.id === event.author)?.name || '???',
                        message: event.message,
                    }
                ]
            };
        case 'GameFinished':
            return {
                ...state,
                winner: event.winner,
                game_log: [
                    ...state.game_log,
                    {
                        type: 'game_event',
                        timestamp: new Date().getTime(),
                        event: `Game finished. ${ state.players.find(player => player.id === event.winner)?.name || '???' } wins!`
                    }
                ]
            };
        default:
            console.error('unknown game event', eventType);
            return state;
    }
};

const saveState = (state: GameClientState): GameClientState => {
    localStorage.state = JSON.stringify(state);
    return state;
}

const reducePawn = (pawn: Pawn, event: GameMoveByPlayerEvent) => {
    if (pawn.player_id !== event.game_move.player_id) {
        return pawn;
    }

    return {
        ...pawn,
        position: applyDirection(pawn.position, event.game_move.game_move.direction as MoveDirection),
    };
}