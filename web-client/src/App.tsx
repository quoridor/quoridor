import { BrowserRouter, Link, Route, useHistory } from 'react-router-dom';
import { AddBotPage, GamePage, HomePage, CreateGamePage, QuonsensusPage } from './pages';

const Header = () => {
  const history = useHistory();

  return (
    <header style={{ fontSize: '42px', paddingLeft: '40px' }}>
        <span className='app-name' onClick={() => history.push('/')}>quoridor</span>
        <Link to='/'>Home</Link>
        <Link to='/create'>New game</Link>
    </header>
  );
};

const App = () => {
  return (
    <>
      <BrowserRouter>
        <Header />
        <main>
          <Route key='/' path='/' exact component={HomePage} />
          <Route key='/create' path='/create' component={CreateGamePage} />
          <Route key='/add-bot/:game_id' path='/add-bot/:game_id' component={AddBotPage} />
          <Route key='/games/:game_id' path='/games/:game_id' component={GamePage} />
          <Route key='/quonsensus' path='/quonsensus' component={QuonsensusPage} />
        </main>
      </BrowserRouter>
    </>
  )
};

export default App;