const isLocal = window.location.hostname === 'localhost';

export const API_ENDPOINT = isLocal ? 'http://localhost:8080' : 'https://quoridor-api.nikitavbv.com';
export const WEBSOCKET_ENDPOINT = isLocal ? 'ws://localhost:8080' : 'wss://quoridor-api.nikitavbv.com';