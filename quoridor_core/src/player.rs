use core::fmt::Debug;
use std::cmp::Ordering;
use dyn_clone::DynClone;
use serde::{Serialize, Deserialize, Serializer, Deserializer};
use crate::board::{Board, GameMove, Pawn};

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct PlayerId(usize);

impl PlayerId {

    pub fn new(id: usize) -> Self {
        Self(id)
    }

    pub fn generate_next(&self) -> Self {
        Self(self.0 + 1)
    }
}

impl ToString for PlayerId {

    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl PartialOrd<Self> for PlayerId {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl Ord for PlayerId {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl Serialize for PlayerId {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        serializer.serialize_str(&self.0.to_string())
    }
}

impl<'de> Deserialize<'de> for PlayerId {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        String::deserialize(deserializer).map(|v| PlayerId::new(v.parse().unwrap()))
    }
}

#[derive(Debug)]
pub enum PlayerAction {
    PerformMove(GameMove),
    Wait,
    Error(String),
}

pub trait Player: DynClone + Debug {
    fn id(&self) -> &PlayerId;
    fn name(&self) -> String;
    fn perform_move(&mut self, board: &Board) -> PlayerAction;

    // generally, only humans can admins to start/stop games, etc.
    fn can_be_game_admin(&self) -> bool {
        false
    }

    // intended to be used by various interactive players:
    fn can_set_next_move(&self) -> bool {
        false
    }
    fn set_next_move(&mut self, _next_move: GameMove) {}

    // optionally, used in quolosseum:
    fn on_opponent_move(&self, _opponent_move: GameMove, _opponent_pawn_before_move: &Pawn) -> Result<(), String> {
        Ok(())
    }

    fn shutdown(&self) {
    }
}

dyn_clone::clone_trait_object!(Player);

#[derive(Clone, Debug)]
pub struct InteractivePlayer {
    id: PlayerId,
    name: String,
    next_move: Option<GameMove>,
}

impl InteractivePlayer {
    pub fn new(id: PlayerId, name: &str) -> Self {
        Self { id, name: name.to_owned(), next_move: None }
    }
}

impl Player for InteractivePlayer {
    fn id(&self) -> &PlayerId {
        &self.id
    }

    fn name(&self) -> String {
        self.name.to_owned()
    }

    fn perform_move(&mut self, _board: &Board) -> PlayerAction {
        match self.next_move {
            Some(v) => {
                self.next_move = None;
                PlayerAction::PerformMove(v)
            }
            None => PlayerAction::Wait,
        }
    }

    fn can_be_game_admin(&self) -> bool {
        true
    }
    
    fn can_set_next_move(&self) -> bool {
        true
    }

    fn set_next_move(&mut self, game_move: GameMove) {
        self.next_move = Some(game_move);
    }
}
