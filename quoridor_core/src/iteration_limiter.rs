use std::time::{Duration, Instant};
use std::fmt::Debug;
use dyn_clone::DynClone;

pub trait IterationLimiter: DynClone + Debug + Send {

    fn reset(&mut self);

    fn has_reached_limit(&self) -> bool;

    fn on_iteration_complete(&mut self);
}

dyn_clone::clone_trait_object!(IterationLimiter);

#[derive(Clone, Debug)]
pub struct IterationCountLimiter {
    completed_iterations: u64,
    iterations_per_cycle: u64,
}

#[derive(Clone, Debug)]
pub struct IterationTimeLimiter {
    started_at: Instant,
    time_limit: Duration,
    first_limit: Duration,
    first_limit_used: bool,
}

impl IterationCountLimiter {

    pub fn new(iteration_per_cycle: u64) -> Self {
        Self {
            completed_iterations: 0,
            iterations_per_cycle: iteration_per_cycle,
        }
    }
}

impl IterationLimiter for IterationCountLimiter {

    fn reset(&mut self) {
        self.completed_iterations = 0;
    }

    fn has_reached_limit(&self) -> bool {
        self.completed_iterations >= self.iterations_per_cycle
    }

    fn on_iteration_complete(&mut self) {
        self.completed_iterations += 1;
    }
}

impl IterationTimeLimiter {

    pub fn new(time_limit: Duration) -> Self {
        Self::with_custom_first_limit(time_limit, time_limit)
    }

    pub fn with_custom_first_limit(time_limit: Duration, first_limit: Duration) -> Self {
        Self {
            started_at: Instant::now(),
            time_limit,
            first_limit,
            first_limit_used: false,
        }
    }
}

impl IterationLimiter for IterationTimeLimiter {

    fn reset(&mut self) {
        self.first_limit_used = true;
        self.started_at = Instant::now();
    }

    fn has_reached_limit(&self) -> bool {
        Instant::now() - self.started_at >= if !self.first_limit_used {
            self.first_limit
        } else {
            self.time_limit
        }
    }

    fn on_iteration_complete(&mut self) {
        // Do nothing
    }
}