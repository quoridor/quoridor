pub mod astar;

use std::fmt::Debug;
use crate::board::{Board, Direction};
use crate::position::BoardPosition;
use crate::player::PlayerId;
use crate::reachability_checker::astar::AStarChecker;

pub trait ReachabilityChecker: Debug {
    fn is_cell_reachable(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &BoardPosition,
        player_id: &PlayerId,
    ) -> bool;

    fn is_side_reachable(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &Direction,
        player_id: &PlayerId,
    ) -> bool;

    fn clone(&self) -> Box<dyn ReachabilityChecker + Send>;
}

impl Default for Box<dyn ReachabilityChecker + Send> {
    fn default() -> Self {
        Box::new(AStarChecker::new())
    }
}

pub fn get_default_reachability_checker() -> Box<dyn ReachabilityChecker + Send> {
    Box::new(AStarChecker::new())
}

pub trait DistanceChecker: Debug {
    fn distance_to_cell(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &BoardPosition,
        player_id: &PlayerId,
    ) -> u8;

    fn distance_to_side(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &Direction,
        player_id: &PlayerId,
    ) -> u8;

    fn clone(&self) -> Box<dyn DistanceChecker + Send>;
}
