use std::collections::HashSet;
use crate::board::{Wall, WallDirection, Direction, Board, Pawn};
use crate::position::BoardPosition;
use crate::player::PlayerId;
use crate::reachability_checker::{ReachabilityChecker, DistanceChecker};

pub const INFINITE_DISTANCE: u8 = u8::MAX;

#[derive(Clone, Debug, PartialEq)]
pub struct AStarChecker;

impl Default for AStarChecker {
    fn default() -> Self {
        AStarChecker::new()
    }
}

fn check_for_wall_east(pos: &BoardPosition, walls: &[Wall]) -> Option<BoardPosition> {
    if pos.column() == 8 {
        return None;
    }
    for wall in walls.iter() {
        if let (WallDirection::Vertical, BoardPosition { column, row }) =
            (wall.direction(), wall.position())
        {
            if *column == pos.column() && (*row == pos.row() || *row == pos.row() + 1) {
                return None;
            }
        }
    }
    Some(BoardPosition::new(pos.column() + 1, pos.row()))
}

fn check_for_wall_west(pos: &BoardPosition, walls: &[Wall]) -> Option<BoardPosition> {
    if pos.column() == 0 {
        return None;
    }
    for wall in walls.iter() {
        if let (WallDirection::Vertical, BoardPosition { column, row }) =
            (wall.direction(), wall.position())
        {
            if *column + 1 == pos.column() && (*row == pos.row() || *row == pos.row() + 1) {
                return None;
            }
        }
    }
    Some(BoardPosition::new(pos.column() - 1, pos.row()))
}

fn check_for_wall_north(pos: &BoardPosition, walls: &[Wall]) -> Option<BoardPosition> {
    if pos.row() == 0 {
        return None;
    }
    for wall in walls.iter() {
        if let (WallDirection::Horizontal, BoardPosition { column, row }) =
            (wall.direction(), wall.position())
        {
            if *row == pos.row() && (*column == pos.column() || *column + 1 == pos.column()) {
                return None;
            }
        }
    }
    Some(BoardPosition::new(pos.column(), pos.row() - 1))
}

fn check_for_wall_south(pos: &BoardPosition, walls: &[Wall]) -> Option<BoardPosition> {
    if pos.row() == 8 {
        return None;
    }
    for wall in walls.iter() {
        if let (WallDirection::Horizontal, BoardPosition { column, row }) =
            (wall.direction(), wall.position())
        {
            if *row == pos.row() + 1 && (*column == pos.column() || *column + 1 == pos.column()) {
                return None;
            }
        }
    }
    Some(BoardPosition::new(pos.column(), pos.row() + 1))
}

fn get_check_for_wall_fn(
    direction: &Direction,
) -> fn(&BoardPosition, &[Wall]) -> Option<BoardPosition> {
    match direction {
        Direction::East => check_for_wall_east,
        Direction::North => check_for_wall_north,
        Direction::South => check_for_wall_south,
        Direction::West => check_for_wall_west,
    }
}

pub fn check_move(
    position: &BoardPosition,
    direction: &Direction,
    board: &Board,
) -> Option<BoardPosition> {
    let check_for_wall = get_check_for_wall_fn(direction);
    check_for_wall(position, board.walls())
}

pub fn  check_move_with_players(
    position: &BoardPosition,
    direction: &Direction,
    board: &Board,
    player_id: &PlayerId,
) -> Vec<BoardPosition> {
    let check_for_wall = get_check_for_wall_fn(direction);
    let new_pos = check_for_wall(position, board.walls());
    if new_pos.is_none() {
        return Vec::new();
    }
    let new_pos = new_pos.unwrap();
    if check_foreign_player(&new_pos, player_id, board.pawns()) {
        let new_pos_over = check_for_wall(&new_pos, board.walls());
        match new_pos_over {
            Some(pos) => {
                if !check_foreign_player(&pos, player_id, board.pawns()) {
                    return vec![pos];
                }
            },
            None => {},
        }
        let mut res = Vec::new();
        if let Some(pos) =
        get_check_for_wall_fn(&direction.perpendicular())(&new_pos, board.walls())
        {
            res.push(pos);
        }
        if let Some(pos) =
        get_check_for_wall_fn(&direction.perpendicular().opposite())(&new_pos, board.walls())
        {
            res.push(pos);
        }
        res
    } else {
        vec![new_pos]
    }
}


impl AStarChecker {
    pub fn new() -> Self {
        AStarChecker {}
    }

    fn calculate_distance(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &HashSet<BoardPosition>,
    ) -> u8 {
        let mut positions = HashSet::new();
        positions.insert(*from);
        if positions.intersection(to).next().is_some() {
            return 0;
        }
        let mut matrix = vec![vec![0 as u8; 9]; 9];
        let directions = vec![Direction::South, Direction::West, Direction::North, Direction::East];
        // println!("{:?}", &to);
        for i in 1..82 {
            // println!("i");
            let mut new_positions = HashSet::new();
            for position in positions.into_iter() {
                let column = position.column() as usize;
                let row = position.row() as usize;
                if matrix[column][row] != 0 {
                    continue;
                }
                matrix[column][row] = 1;
                for direction in directions.iter() {
                    for new_position in
                    check_move(&position, direction, board).into_iter()
                    {
                        if matrix[new_position.column() as usize][new_position.row() as usize] == 0
                        {
                            new_positions.insert(new_position);
                        }
                    }
                }
            }
            positions = new_positions;
            // print!("{{");
            // for p in positions.iter() {
            //     print!("[{}, {}], ", p.column, p.row);
            // }
            // println!("}}");
            if positions.intersection(to).next().is_some() {
                return i;
            }
            if positions.is_empty() {
                return INFINITE_DISTANCE;
            }
        }
        return INFINITE_DISTANCE;
    }
}

impl ReachabilityChecker for AStarChecker {
    fn is_cell_reachable(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &BoardPosition,
        player_id: &PlayerId,
    ) -> bool {
        self.calculate_distance(
            board,
            from,
            &vec![*to].into_iter().collect::<HashSet<_>>(),
        ) < INFINITE_DISTANCE
    }

    fn is_side_reachable(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &Direction,
        player_id: &PlayerId,
    ) -> bool {
        self.calculate_distance(board, from, &to.winner_positions()) < INFINITE_DISTANCE
    }

    fn clone(&self) -> Box<dyn ReachabilityChecker + Send> {
        Box::new(AStarChecker::new())
    }
}

impl DistanceChecker for AStarChecker {
    fn distance_to_cell(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &BoardPosition,
        player_id: &PlayerId,
    ) -> u8 {
        self.calculate_distance(
            board,
            from,
            &vec![*to].into_iter().collect::<HashSet<_>>(),
        )
    }

    fn distance_to_side(
        &self,
        board: &Board,
        from: &BoardPosition,
        to: &Direction,
        player_id: &PlayerId,
    ) -> u8 {
        self.calculate_distance(board, from, &to.winner_positions())
    }

    fn clone(&self) -> Box<dyn DistanceChecker + Send> {
        Box::new(AStarChecker::new())
    }
}

fn check_foreign_player(pos: &BoardPosition, player_id: &PlayerId, players: &[Pawn]) -> bool {
    for player in players.iter() {
        if player.position() == pos {
            return player.player_id() != player_id;
        }
    }
    false
}

#[cfg(test)]
mod tests {
    use crate::board::*;
    use crate::reachability_checker::astar::*;
    use crate::reachability_checker::ReachabilityChecker;
    use crate::notation::fen_notation::*;

    #[test]
    fn check_for_wall_east_should_return_none() {
        let walls = vec![
            Wall::new(BoardPosition::new(6, 4), WallDirection::Vertical),
            Wall::new(BoardPosition::new(5, 5), WallDirection::Vertical),
        ];
        assert_eq!(None, check_for_wall_east(&BoardPosition::new(6, 4), &walls));

        let walls = vec![
            Wall::new(BoardPosition::new(6, 5), WallDirection::Vertical),
            Wall::new(BoardPosition::new(5, 5), WallDirection::Vertical),
        ];
        assert_eq!(None, check_for_wall_east(&BoardPosition::new(6, 4), &walls));

        let walls = vec![
            Wall::new(BoardPosition::new(6, 6), WallDirection::Vertical),
            Wall::new(BoardPosition::new(5, 5), WallDirection::Vertical),
        ];
        assert_eq!(None, check_for_wall_east(&BoardPosition::new(8, 4), &walls));
    }

    #[test]
    fn check_for_wall_west_should_return_none() {
        let walls = vec![
            Wall::new(BoardPosition::new(1, 3), WallDirection::Vertical),
            Wall::new(BoardPosition::new(2, 4), WallDirection::Vertical),
        ];
        assert_eq!(None, check_for_wall_west(&BoardPosition::new(2, 3), &walls));

        let walls = vec![
            Wall::new(BoardPosition::new(1, 4), WallDirection::Vertical),
            Wall::new(BoardPosition::new(2, 4), WallDirection::Vertical),
        ];
        assert_eq!(None, check_for_wall_west(&BoardPosition::new(2, 3), &walls));

        let walls = vec![
            Wall::new(BoardPosition::new(6, 6), WallDirection::Vertical),
            Wall::new(BoardPosition::new(5, 5), WallDirection::Vertical),
        ];
        assert_eq!(None, check_for_wall_west(&BoardPosition::new(0, 3), &walls));
    }

    #[test]
    fn check_for_wall_north_should_return_none() {
        let walls = vec![
            Wall::new(BoardPosition::new(4, 4), WallDirection::Horizontal),
            Wall::new(BoardPosition::new(3, 5), WallDirection::Horizontal),
        ];
        assert_eq!(None, check_for_wall_north(&BoardPosition::new(4, 4), &walls));

        let walls = vec![
            Wall::new(BoardPosition::new(3, 4), WallDirection::Horizontal),
            Wall::new(BoardPosition::new(3, 5), WallDirection::Horizontal),
        ];
        assert_eq!(None, check_for_wall_north(&BoardPosition::new(4, 4), &walls));

        let walls = vec![
            Wall::new(BoardPosition::new(6, 6), WallDirection::Horizontal),
            Wall::new(BoardPosition::new(5, 5), WallDirection::Horizontal),
        ];
        assert_eq!(None, check_for_wall_north(&BoardPosition::new(5, 0), &walls));
    }

    #[test]
    fn check_for_wall_south_should_return_none() {
        let walls = vec![
            Wall::new(BoardPosition::new(5, 2), WallDirection::Horizontal),
            Wall::new(BoardPosition::new(4, 4), WallDirection::Horizontal),
        ];
        assert_eq!(None, check_for_wall_south(&BoardPosition::new(5, 1), &walls));

        let walls = vec![
            Wall::new(BoardPosition::new(4, 2), WallDirection::Horizontal),
            Wall::new(BoardPosition::new(4, 4), WallDirection::Horizontal),
        ];
        assert_eq!(None, check_for_wall_south(&BoardPosition::new(5, 1), &walls));

        let walls = vec![
            Wall::new(BoardPosition::new(6, 6), WallDirection::Horizontal),
            Wall::new(BoardPosition::new(5, 5), WallDirection::Horizontal),
        ];
        assert_eq!(None, check_for_wall_south(&BoardPosition::new(5, 8), &walls));
    }

    #[test]
    fn is_reachable_should_return_true_1() {
        let board =
            Board::from_fen_notation("H: e4c8f5h5g2e2c2 / V: d5d7b9g4h3 / e6 b4 f6 e7  /  / 0")
                .unwrap();
        let checker = AStarChecker::new();

        assert!(checker.is_cell_reachable(
            &board,
            &BoardPosition::new(4, 5),
            &BoardPosition::new(4, 0),
            &PlayerId::new(0)
        ));
        assert!(checker.is_side_reachable(
            &board,
            &BoardPosition::new(4, 5),
            &Direction::North,
            &PlayerId::new(0)
        ));
        assert!(checker.is_side_reachable(&board, &BoardPosition::new(4, 5), &Direction::East, &PlayerId::new(0)));
        assert!(checker.is_side_reachable(
            &board,
            &BoardPosition::new(4, 5),
            &Direction::South,
            &PlayerId::new(0)
        ));
        assert!(checker.is_side_reachable(&board, &BoardPosition::new(4, 5), &Direction::West, &PlayerId::new(0)));
    }

    #[test]
    fn is_reachable_should_return_false_1() {
        let board =
            Board::from_fen_notation("H: e4c8f5h5g2e2c2 / V: d5d7b9g4h3e5 / e6 b4 f6 e7  /  / 0")
                .unwrap();
        let checker = AStarChecker::new();

        assert!(!checker.is_cell_reachable(
            &board,
            &BoardPosition::new(4, 5),
            &BoardPosition::new(4, 0),
            &PlayerId::new(0)
        ));
    }

    #[test]
    fn is_reachable_should_return_true_2() {
        let board = Board::from_fen_notation(
            "H: b4c7e7g7g9g6e6e5c3e2c2 / V: a2a4c5f9h8h6f5d4f3 / h6 e6 d5 c3  /  / 0",
        )
        .unwrap();
        let checker = AStarChecker::new();

        assert!(checker.is_cell_reachable(
            &board,
            &BoardPosition::new(7, 5),
            &BoardPosition::new(4, 0),
            &PlayerId::new(0)
        ));
    }

    #[test]
    fn is_reachable_should_return_false_2() {
        let board = Board::from_fen_notation(
            "H: b4c7e7g7g9g6e6e5c3e2c2 / V: a2a4c5f9h8h6f5d4f3b3 / h6 e6 d5 c3  /  / 0",
        )
        .unwrap();
        let checker = AStarChecker::new();

        assert!(!checker.is_cell_reachable(
            &board,
            &BoardPosition::new(7, 5),
            &BoardPosition::new(4, 0),
            &PlayerId::new(0)
        ));
    }

    #[test]
    fn is_reachable_should_return_true_3() {
        let board = Board::from_fen_notation(
            "H: b3d2g2h3e3d5c4b6g5e6c7e8a8f7c9 / V: a2d4b5f5g8 / d8 g3 e5 c3  /  / 0",
        )
        .unwrap();
        let checker = AStarChecker::new();

        assert!(checker.is_cell_reachable(
            &board,
            &BoardPosition::new(3, 7),
            &BoardPosition::new(4, 0),
            &PlayerId::new(0)
        ));
    }

    #[test]
    fn is_reachable_should_return_true_4() {
        let board = Board::from_fen_notation(
            "H: b2d2f2b3b9d9f9g8g3 / V: c3f3a4a6a8f8h8h6h4 / e9 h9 i2 a2  /  / 0",
        )
        .unwrap();
        let checker = AStarChecker::new();

        assert!(checker.is_cell_reachable(
            &board,
            &BoardPosition::new(4, 8),
            &BoardPosition::new(4, 0),
            &PlayerId::new(0)
        ));
    }

    #[test]
    fn is_reachable_should_return_false_4() {
        let board = Board::from_fen_notation(
            "H: b2d2f2b3b9d9f9g8g3h2 / V: c3f3a4a6a8f8h8h6h4a2/ e9 h9 i2 a2  /  / 0",
        )
        .unwrap();
        let checker = AStarChecker::new();

        assert!(!checker.is_cell_reachable(
            &board,
            &BoardPosition::new(4, 8),
            &BoardPosition::new(4, 0),
            &PlayerId::new(0)
        ));
    }
}
