use serde::{Serialize, Deserialize};
use crate::board::{MoveDirection, Wall, WallDirection};
use crate::position::BoardPosition;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Route {
    positions: Vec<BoardPosition>,
}

impl Route {

    pub fn new(positions: Vec<BoardPosition>) -> Self {
        Self {
            positions
        }
    }

    pub fn head(&self) -> &BoardPosition {
        &self.positions[0]
    }

    pub fn next_cell(&self) -> &BoardPosition {
        &self.positions[1]
    }

    pub fn head_direction(&self) -> Option<MoveDirection> {
        let current_position = self.head();
        let next_position = self.next_cell();
        current_position.direction_to_neighbor_cell(*next_position)
    }

    pub fn length(&self) -> usize {
        self.positions.len()
    }

    pub fn subroute_with_direction(&self, direction: &MoveDirection) -> Option<Self> {
        self.subroute_with_position(&self.head().apply_move(direction.clone()))
    }

    pub fn subroute_with_position(&self, position: &BoardPosition) -> Option<Self> {
        let index = self.positions.iter().position(|pos| pos == position)?;
        Some(Self {
            positions: self.positions[index..].to_vec(),
        })
    }

    pub fn is_blocked_by_wall(&self, wall: &Wall) -> bool {
        match wall.direction() {
            WallDirection::Vertical => self.contains_transition_or_reverse(
                &wall.position(),
                &wall.position().apply_move(MoveDirection::East)
            ) || self.contains_transition_or_reverse(
                &wall.position().apply_move(MoveDirection::North),
                &wall.position()
                    .apply_move(MoveDirection::North).
                    apply_move(MoveDirection::East)
            ),
            WallDirection::Horizontal => self.contains_transition_or_reverse(
                &wall.position(),
                &wall.position().apply_move(MoveDirection::North),
            ) || self.contains_transition_or_reverse(
                &wall.position().apply_move(MoveDirection::East),
                &wall.position().apply_move(MoveDirection::East).apply_move(MoveDirection::North)
            )
        }
    }

    fn contains_transition_or_reverse(&self, from: &BoardPosition, to: &BoardPosition) -> bool {
        for i in 0..self.positions.len() - 1 {
            let pos = &self.positions[i];
            let next_pos = &self.positions[i + 1];

            if (pos == from && next_pos == to) || (pos == to && next_pos == from) {
                return true;
            }
        }

        false
    }
}