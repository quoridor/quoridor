use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::collections::binary_heap::BinaryHeap;
use crate::board::{Board, Direction, MoveDirection};
use crate::position::BoardPosition;
use crate::reachability_checker::astar::check_move;
use crate::route::Route;

#[derive(Clone, Debug, Eq, PartialEq)]
struct SearchState {
    cost: usize,
    positions: Vec<BoardPosition>,
}

impl Ord for SearchState {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for SearchState {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct SinglePositionSearchState {
    cost: usize,
    position: BoardPosition,
}

impl Ord for SinglePositionSearchState {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for SinglePositionSearchState {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn find_shortest_route_to_edge(board: &Board, start: &BoardPosition, end: &Direction) -> Option<Route> {
    let directions = vec![
        Direction::South,
        Direction::West,
        Direction::North,
        Direction::East
    ];

    let mut heap = BinaryHeap::new();
    heap.push(SinglePositionSearchState {
        cost: 0,
        position: start.clone(),
    });

    let mut came_from: HashMap<BoardPosition, Option<BoardPosition>> = HashMap::new();
    let mut cost_so_far: HashMap<BoardPosition, usize> = HashMap::new();
    came_from.insert(start.clone(), None);
    cost_so_far.insert(start.clone(), 0);

    while let Some(state) = heap.pop() {
        let position = state.position;
        if end.winner_positions().contains(&position) {
            let mut positions = Vec::new();
            positions.insert(0, position.clone());
            while let Some(next_position) = came_from[&positions[0]] {
                positions.insert(0, next_position);
            }
            return Some(Route::new(positions));
        }

        let new_cost = cost_so_far[&position] + 1;

        for direction in &directions {
            let new_position = match check_move(&position, direction, board) {
                Some(v) => v,
                None => continue,
            };

            if !cost_so_far.contains_key(&new_position) || &new_cost < cost_so_far.get(&new_position).unwrap() {
                cost_so_far.insert(new_position.clone(), new_cost);
                came_from.insert(new_position.clone(), Some(position.clone()));
                heap.push(SinglePositionSearchState {
                    cost: new_cost + distance_heuristic(&new_position, &end),
                    position: new_position,
                });
            }
        }
    }

    None
}

pub fn find_path_to_edge(board: &Board, start: BoardPosition, end: Direction) -> Option<Vec<BoardPosition>> {
    let directions = vec![
        Direction::South,
        Direction::West,
        Direction::North,
        Direction::East
    ];

    let mut heap = BinaryHeap::new();
    heap.push(SearchState {
        cost: 0,
        positions: vec![start.clone()],
    });

    let mut visited: HashSet<BoardPosition> = HashSet::new();

    while let Some(state) = heap.pop() {
        let position = state.positions.last().unwrap();
        if end.winner_positions().contains(position) {
            return Some(state.positions);
        }

        for direction in &directions {
            let new_position = match check_move(position, direction, board) {
                Some(v) => v,
                None => continue,
            };

            if visited.contains(&new_position) {
                continue;
            }

            let new_state = SearchState {
                cost: state.positions.len() + distance_heuristic(&position, &end),
                positions: {
                    let mut pos = state.positions.clone();
                    pos.push(new_position.clone());
                    pos
                }
            };

            visited.insert(new_position.clone());
            heap.push(new_state);
        }

    }

    None
}

fn distance_heuristic(from: &BoardPosition, end: &Direction) -> usize {
    let res: isize = end.winner_positions().iter()
        .map(|v| (v.row as isize - from.row as isize).pow(2) + (v.column as isize - from.column as isize).pow(2))
        .sum();
    res as usize / end.winner_positions().len()
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_priority() {
        let mut heap = BinaryHeap::new();
        heap.push(SearchState {
            cost: 42,
            positions: Vec::new(),
        });
        heap.push(SearchState {
            cost: 23,
            positions: Vec::new(),
        });

        assert_eq!(heap.pop().unwrap().cost, 23);
    }

    #[test]
    fn test_winner_positions() {
        println!("{:?}", Direction::North.winner_positions());
    }
}