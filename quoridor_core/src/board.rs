use std::borrow::BorrowMut;
use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;
use std::hash::{Hash, Hasher};
use maplit::hashmap;
use serde::{Serialize, Deserialize};
use thiserror::Error;
use fxhash::FxHasher64;
use crate::player::PlayerId;
use crate::notation::error::NotationError;
use crate::reachability_checker::astar::{AStarChecker, check_move, check_move_with_players};
use crate::reachability_checker::{ReachabilityChecker, get_default_reachability_checker};
use crate::possible_moves::{find_path_to_edge, find_shortest_route_to_edge};
use crate::position::BoardPosition;
use crate::route::Route;

// see https://quoridorstrats.wordpress.com/notation/ for notation,
// which we do not follow, lol.

const MAX_WALLS_PER_BOARD: usize = 20;

#[derive(Debug, Serialize, Deserialize)]
pub struct Board {
    next_move_by: PlayerId,
    pawns: Vec<Pawn>,
    walls: Vec<Wall>,
    remaining_walls: HashMap<PlayerId, u8>,

    #[serde(skip)]
    reachability_checker: Box<dyn ReachabilityChecker + Send>,
    moves_history: Vec<GameMoveByPlayer>,

    #[serde(skip)]
    shortest_route: HashMap<PlayerId, Route>,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(tag = "direction")]
pub enum MoveDirection {
    North,
    JumpNorth,
    NorthEast,
    JumpEast,
    NorthWest,
    East,
    SouthEast,
    JumpSouth,
    SouthWest,
    South,
    West,
    JumpWest,
}

impl MoveDirection {
    pub fn get_deg(&self) -> i16 {
        match self {
            MoveDirection::East => 0,
            MoveDirection::JumpEast => 0,
            MoveDirection::NorthEast => 45,
            MoveDirection::North => 90,
            MoveDirection::JumpNorth => 90,
            MoveDirection::NorthWest => 134,
            MoveDirection::West => 180,
            MoveDirection::JumpWest => 180,
            MoveDirection::SouthWest => 225,
            MoveDirection::South => 270,
            MoveDirection::JumpSouth => 270,
            MoveDirection::SouthEast => 315,
        }
    }

    pub fn is_parallel(&self, direction: &MoveDirection) -> bool {
        let deg = (self.get_deg() - direction.get_deg()).abs();
        deg == 0 || deg == 180
    }

    pub fn is_45_deg(&self, direction: &MoveDirection) -> bool {
        (self.get_deg() - direction.get_deg()).abs() == 45
    }
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Pawn {
    player_id: PlayerId, // id of the player this pawn belongs to

    // which side of the board this pawn originates from, origin.opposite() is the side of the board
    // it needs to reach to win
    origin: Direction,

    position: BoardPosition,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize, Copy, Hash, Eq)]
pub struct Wall {
    // For vertical walls:
    // 1 | 2
    // 3 | 4  <--- we use cell #3 to reference this wall location
    //
    // For horizontal walls:
    // 1 2
    // ---
    // 3 4 <--- we use cell #3 to reference this wall location
    position: BoardPosition,
    direction: WallDirection,
}

#[derive(Clone, Debug, PartialEq, Hash, Eq, Serialize, Deserialize, Copy, Ord, PartialOrd)]
pub enum WallDirection {
    Vertical,
    Horizontal,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GameMoveByPlayer {
    pub player_id: PlayerId,
    pub game_move: GameMove,
}

#[derive(Clone, Debug, Serialize, Deserialize, Copy, PartialEq, Eq, Hash)]
#[serde(tag = "type")]
pub enum GameMove {
    MovePawn(MoveDirection),
    PlaceWall(Wall),
}

impl GameMove {
    pub fn as_pawn_move(&self) -> Option<MoveDirection> {
        match self {
            GameMove::MovePawn(direction) => Some(*direction),
            _ => None,
        }
    }

    pub fn is_move_pawn(&self) -> bool {
        match self {
            GameMove::MovePawn(_) => true,
            _ => false,
        }
    }

    pub fn is_move_wall(&self) -> bool {
        match self {
            GameMove::PlaceWall(_) => true,
            _ => false,
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum GameStateValidity {
    Valid,
    InvalidNextMovePlayerId,
    // when next_move_by is invalid. example: two players and next_move_by = 3
    InvalidPawnsAmount,
    // when pawns amount is not 2 and not 4
    PawnsOverlap,
    PawnLocatedOutsideOfTheBoard,
    InvalidPawnMove {
        position: BoardPosition,
        direction: MoveDirection,
    },
    InvalidPawnsOrigins,
    // when pawns origins have wrong directions. For example: two origins have one direction or two origins are perpendicular
    InvalidWallsAmount,
    // when sum (remaining + walls on the board) of all walls is not equal to 20
    InvalidWallsPositions,
    // when walls overlap or are located outside of the board
    SideIsNotReachable, // when some player is unable to reach the needed board side
}

#[derive(Debug, Error, Clone)]
pub enum MoveError {
    #[error("Invalid pawn move: {0:?}")]
    PawnMoveError(GameStateValidity),
    #[error("Invalid wall move: {0:?}")]
    WallMoveError(GameStateValidity),
    #[error("Results in invalid board")]
    ResultsInInvalidBoardError(GameStateValidity),
}

fn board_coordinate_to_letter(coordinate: u8) -> char {
    assert!(coordinate < 9);
    // 97 = a in ASCII table
    char::from(coordinate + 97)
}

impl ToString for BoardPosition {
    fn to_string(&self) -> String {
        format!("{}{}", board_coordinate_to_letter(self.column), self.row + 1)
    }
}

impl Wall {
    pub fn new(position: BoardPosition, direction: WallDirection) -> Self {
        Wall { position, direction }
    }

    pub fn direction(&self) -> &WallDirection {
        &self.direction
    }

    pub fn position(&self) -> &BoardPosition {
        &self.position
    }

    pub fn get_exclusion_walls(&self) -> HashSet<Wall> {
        let mut res = HashSet::new();
        res.insert(*self);
        match self.direction {
            WallDirection::Horizontal => {
                res.insert(Wall::new(*self.position(), WallDirection::Vertical));
                if self.position.column > 0 {
                    res.insert(Wall::new(
                        BoardPosition::new(self.position.column - 1, self.position.row),
                        WallDirection::Horizontal,
                    ));
                }
                if self.position.column < 7 {
                    res.insert(Wall::new(
                        BoardPosition::new(self.position.column + 1, self.position.row),
                        WallDirection::Horizontal,
                    ));
                }
            }
            WallDirection::Vertical => {
                res.insert(Wall::new(*self.position(), WallDirection::Horizontal));
                if self.position.row > 1 {
                    res.insert(Wall::new(
                        BoardPosition::new(self.position.column, self.position.row - 1),
                        WallDirection::Vertical,
                    ));
                }
                if self.position.row < 8 {
                    res.insert(Wall::new(
                        BoardPosition::new(self.position.column, self.position.row + 1),
                        WallDirection::Vertical,
                    ));
                }
            }
        };
        res
    }
}

impl Board {
    pub fn construct(
        next_move_by: PlayerId,
        walls: Vec<Wall>,
        pawns: Vec<Pawn>,
        remaining_walls: HashMap<PlayerId, u8>,
        player_moves: Vec<GameMoveByPlayer>,
    ) -> Self {
        Board {
            next_move_by,
            pawns,
            walls,
            remaining_walls,
            reachability_checker: get_default_reachability_checker(),
            moves_history: player_moves,
            shortest_route: HashMap::new(),
        }
    }

    pub fn construct_full(
        next_move_by: PlayerId,
        walls: Vec<Wall>,
        pawns: Vec<Pawn>,
        remaining_walls: HashMap<PlayerId, u8>,
        reachability_checker: Box<dyn ReachabilityChecker + Send>,
        player_moves: Vec<GameMoveByPlayer>,
        shortest_route: HashMap<PlayerId, Route>,
    ) -> Self {
        Board {
            next_move_by,
            pawns,
            walls,
            remaining_walls,
            reachability_checker,
            moves_history: player_moves,
            shortest_route,
        }
    }

    pub fn new() -> Self {
        Self::new_for_two_players(
            PlayerId::new(0),
            PlayerId::new(1),
            PlayerId::new(0),
        )
    }

    pub fn set_reachability_checker(
        &mut self,
        reachability_checker: Box<dyn ReachabilityChecker + Send>,
    ) {
        self.reachability_checker = reachability_checker;
    }

    pub fn walls(&self) -> &Vec<Wall> {
        &self.walls
    }

    pub fn pawns(&self) -> &Vec<Pawn> {
        &self.pawns
    }

    pub fn remaining_walls(&self) -> &HashMap<PlayerId, u8> {
        &self.remaining_walls
    }

    pub fn next_move_by(&self) -> &PlayerId {
        &self.next_move_by
    }

    pub fn new_for_two_players(
        first_player: PlayerId,
        second_player: PlayerId,
        first_move_by: PlayerId,
    ) -> Self {
        Self {
            moves_history: Vec::new(),
            next_move_by: first_move_by,
            pawns: vec![
                Pawn::new(first_player.clone(), Direction::North),
                Pawn::new(second_player.clone(), Direction::South),
            ],
            walls: Vec::new(),
            remaining_walls: hashmap! {
                first_player => 10,
                second_player => 10
            },
            reachability_checker: get_default_reachability_checker(),
            shortest_route: HashMap::new(),
        }
    }

    pub fn new_for_four_players(
        first_player: PlayerId,
        second_player: PlayerId,
        third_player: PlayerId,
        fourth_player: PlayerId,
        first_move_by: PlayerId,
    ) -> Self {
        Self {
            moves_history: Vec::new(),
            next_move_by: first_move_by,
            pawns: vec![
                Pawn::new(first_player.clone(), Direction::North),
                Pawn::new(second_player.clone(), Direction::South),
                Pawn::new(third_player.clone(), Direction::West),
                Pawn::new(fourth_player.clone(), Direction::East),
            ],
            walls: Vec::new(),
            remaining_walls: hashmap! {
                first_player => 5,
                second_player => 5,
                third_player => 5,
                fourth_player => 5,
            },
            reachability_checker: get_default_reachability_checker(),
            shortest_route: HashMap::new(),
        }
    }

    fn is_valid(&self) -> GameStateValidity {
        if self.pawns.len() != 2 && self.pawns.len() != 4 {
            return GameStateValidity::InvalidPawnsAmount;
        }
        if self.pawns.iter().find(|pawn| pawn.player_id == self.next_move_by).is_none() {
            return GameStateValidity::InvalidNextMovePlayerId;
        }
        let reachability_checker = AStarChecker::new();
        let mut positions = HashSet::new();
        let mut directions = HashSet::new();
        for Pawn { position, player_id, origin } in self.pawns.iter() {
            if position.column > 8 || position.row > 8 {
                return GameStateValidity::PawnLocatedOutsideOfTheBoard;
            }
            if !positions.insert(position) {
                return GameStateValidity::PawnsOverlap;
            }
            if !directions.insert(origin) {
                return GameStateValidity::InvalidPawnsOrigins;
            }
            // println!("player_id: {:?}; player position: {:?}; direction to: {:?}", player_id, &position, origin.opposite());
            if !reachability_checker.is_side_reachable(
                self,
                position,
                &origin.opposite(),
                player_id,
            ) {
                // println!("is not reachable");
                return GameStateValidity::SideIsNotReachable;
            } else {
                // println!("reachable");
            }
        }
        if self.pawns.len() == 2
            && !directions.eq(&HashSet::from_iter(vec![&Direction::South, &Direction::North]))
        {
            return GameStateValidity::InvalidPawnsOrigins;
        }
        if self.walls.len()
            + self.remaining_walls().iter().fold(0, |sum, (_, walls)| sum + walls) as usize
            != MAX_WALLS_PER_BOARD
        {
            return GameStateValidity::InvalidWallsAmount;
        }
        let mut positions = HashSet::new();
        for Wall { position, direction } in self.walls.iter() {
            if position.column > 7 || position.row < 1 || position.row > 8 {
                return GameStateValidity::InvalidWallsPositions;
            }
            if !positions.insert(position) {
                return GameStateValidity::InvalidWallsPositions;
            }
            match direction {
                WallDirection::Vertical => {
                    if self.walls.iter().any(|wall| {
                        wall.direction == WallDirection::Vertical
                            && wall.position().column == position.column
                            && (wall.position().row as i16 - position.row as i16).abs() == 1
                    }) {
                        return GameStateValidity::InvalidWallsPositions;
                    }
                }
                WallDirection::Horizontal => {
                    if self.walls.iter().any(|wall| {
                        wall.direction == WallDirection::Horizontal
                            && wall.position().row == position.row
                            && (wall.position().column as i16 - position.column as i16).abs() == 1
                    }) {
                        return GameStateValidity::InvalidWallsPositions;
                    }
                }
            };
        }
        GameStateValidity::Valid
    }

    pub fn apply_move(&self, game_move: GameMoveByPlayer) -> Result<Self, MoveError> {
        match game_move.game_move {
            GameMove::MovePawn(direction) => {
                let pawn = self.pawns.iter()
                    .find(|pawn| pawn.player_id == game_move.player_id)
                    .unwrap();
                if pawn.apply_move_checked(direction).is_none() {
                    return Err(MoveError::PawnMoveError(
                        GameStateValidity::InvalidPawnMove {
                            position: pawn.position.clone(),
                            direction: direction.clone(),
                        },
                    ));
                }
            }
            GameMove::PlaceWall(_) => {
                match self.remaining_walls().get(&game_move.player_id) {
                    Some(walls) => {
                        if *walls == 0 {
                            return Err(MoveError::WallMoveError(
                                GameStateValidity::InvalidWallsAmount,
                            ));
                        }
                    }
                    None => {
                        return Err(MoveError::WallMoveError(
                            GameStateValidity::InvalidWallsAmount,
                        ));
                    }
                };
            }
        };

        let new_board = self.apply_move_unchecked(game_move);

        match new_board.is_valid() {
            GameStateValidity::Valid => Ok(new_board),
            err => Err(MoveError::ResultsInInvalidBoardError(err)),
        }
    }

    pub fn apply_move_unchecked(&self, game_move: GameMoveByPlayer) -> Self {
        match game_move.game_move {
            GameMove::MovePawn(direction) => {
                Self {
                    moves_history: self.add_move_to_history(game_move.clone()),
                    next_move_by: self.player_after(&game_move.player_id).clone(),
                    pawns: {
                        let mut new_pawns = self.pawns.clone();
                        for pawn in new_pawns.iter_mut() {
                            if pawn.player_id == game_move.player_id {
                                *pawn = pawn.apply_move(direction);
                            }
                        }
                        new_pawns
                    },
                    walls: self.walls.clone(),
                    remaining_walls: self.remaining_walls.clone(),
                    reachability_checker: self.reachability_checker.clone(),
                    shortest_route: {
                        let mut shortest_route = self.shortest_route.clone();

                        if let Some(route) = shortest_route.remove(&game_move.player_id) {
                            if let Some(subroute) = route.subroute_with_direction(&direction) {
                                shortest_route.insert(game_move.player_id.clone(), subroute);
                            }
                        }

                        shortest_route
                    },
                }
            }
            GameMove::PlaceWall(wall) => {
                let mut remaining_walls = self.remaining_walls.clone();
                remaining_walls.insert(
                    game_move.player_id.clone(),
                    remaining_walls.get(&game_move.player_id).expect("expected player with this id to be present") - 1,
                );
                Self {
                    moves_history: self.add_move_to_history(game_move.clone()),
                    next_move_by: self.player_after(&game_move.player_id).clone(),
                    pawns: self.pawns.clone(),
                    walls: {
                        let mut walls = self.walls.clone();
                        walls.push(wall.clone());
                        walls
                    },
                    remaining_walls,
                    reachability_checker: self.reachability_checker.clone(),
                    shortest_route: {
                        let mut new_shortest_route = HashMap::new();

                        for (player, route) in &self.shortest_route {
                            if route.is_blocked_by_wall(&wall) {
                                continue;
                            }

                            new_shortest_route.insert(player.clone(), route.clone());
                        }

                        new_shortest_route
                    },
                }
            }
        }
    }

    fn player_after(&self, player_id: &PlayerId) -> PlayerId {
        self.pawns
            .iter()
            .map(|v| v.player_id.clone())
            .filter(|v| v > &player_id)
            .min()
            .unwrap_or_else(|| self.pawns.iter().map(|v| v.player_id.clone()).min().unwrap())
    }

    pub fn winner(&self) -> Option<&PlayerId> {
        for pawn in self.pawns.iter() {
            if pawn.origin.opposite().winner_positions().contains(pawn.position()) {
                return Some(&pawn.player_id);
            }
        }
        None
    }

    pub fn get_possible_moves(&self, player_id: &PlayerId) -> HashSet<GameMove> {
        let mut moves = HashSet::new();

        if *self.remaining_walls().get(player_id).unwrap_or(&0) > 0 {
            moves.extend(
                self.get_likely_possible_wall_moves()
                    .iter()
                    .filter(|m| self.is_likely_move_actually_possible(m))
            );
        }

        self.get_possible_pawn_moves(player_id).iter().for_each(|mv| {
            moves.insert(mv.clone());
        });

        moves
    }

    // Such moves will probably be valid, but you need to perform additional checks to know for sure.
    pub fn get_likely_possible_wall_moves(&self) -> Vec<GameMove> {
        let mut bad_walls = HashSet::new();
        for wall in self.walls.iter() {
            bad_walls.extend(wall.get_exclusion_walls());
        }

        ALL_POSSIBLE_WALLS
            .clone()
            .difference(&bad_walls)
            .map(|w| GameMove::PlaceWall(*w))
            .collect()
    }

    pub fn get_possible_pawn_moves(&self, player_id: &PlayerId) -> Vec<GameMove> {
        let mut moves = Vec::new();

        let pos = match self.pawns.iter().find(|p| p.player_id == *player_id) {
            Some(v) => v,
            None => panic!("Could not find player with id {} for pawns: {:?}", player_id.to_string(), self.pawns),
        };
        let directions = vec![Direction::South, Direction::West, Direction::North, Direction::East];
        for direction in directions.iter() {
            for position in check_move_with_players(&pos.position, direction, self, player_id) {
                moves.push(GameMove::MovePawn(
                    pos.position.direction_to_neighbor_cell(position).unwrap(),
                ));
            }
        }

        moves
    }

    fn add_move_to_history(&self, game_move: GameMoveByPlayer) -> Vec<GameMoveByPlayer> {
        let mut moves: Vec<GameMoveByPlayer> = self.moves_history.clone();
        moves.push(game_move);
        moves
    }

    pub fn moves_history(&self) -> &Vec<GameMoveByPlayer> {
        &self.moves_history
    }

    pub fn is_likely_move_actually_possible(&self, game_move: &GameMove) -> bool {
        let board = self.apply_move_unchecked(GameMoveByPlayer::new(
            self.next_move_by().clone(), game_move.clone(),
        ));

        for pawn in self.pawns() {
            if board.shortest_route_for_pawn(pawn).is_none() &&
                find_path_to_edge(&board, pawn.position().clone(), pawn.origin().opposite()).is_none() {
                return false;
            }
        }

        true
    }

    pub fn shortest_route_for_pawn(&self, pawn: &Pawn) -> Option<&Route> {
        self.shortest_route_for_player(pawn.player_id())
    }

    pub fn shortest_route_for_player(&self, player_id: &PlayerId) -> Option<&Route> {
        self.shortest_route.get(player_id)
    }

    pub fn compute_shortest_routes(&mut self) {
        for pawn in &self.pawns {
            if self.shortest_route.contains_key(pawn.player_id()) {
                continue;
            }

            self.shortest_route.insert(
                pawn.player_id().clone(),
                find_shortest_route_to_edge(&self, pawn.position(), &pawn.origin().opposite())
                    .expect("expected for route between pawn and edge to exist"),
            );
        }
    }

    pub fn force_compute_shortest_routes(&mut self) {
        self.shortest_route.clear();
        self.compute_shortest_routes();
    }

    pub fn mirror(&self) -> Self {
        let mut mirrored_walls = Vec::new();
        for wall in self.walls() {
            let position = wall.position();
            let mirrored_position = BoardPosition::new(7 - position.column(), 9 - position.row());
            mirrored_walls.push(Wall::new(mirrored_position, wall.direction().clone()));
        }

        let mut mirrored_pawns = Vec::new();
        for pawn in self.pawns() {
            let position = pawn.position();
            let mirrored_position = BoardPosition::new(8 - position.column(), 8 - position.row());
            mirrored_pawns.push(Pawn::construct(pawn.player_id().clone(), pawn.origin().opposite(), mirrored_position));
        }

        Board::construct_full(
            self.next_move_by.clone(),
            mirrored_walls,
            mirrored_pawns,
            self.remaining_walls.clone(),
            get_default_reachability_checker(),
            Vec::new(),
            HashMap::new(),
        )
    }

    pub fn hash(&self) -> u64 {
        let mut hasher = FxHasher64::default();

        let pawn1 = &self.pawns()[0];
        let pawn2 = &self.pawns()[1];
        let (pawn1, pawn2) = if pawn1.player_id() < pawn2.player_id() {
            (pawn1, pawn2)
        } else {
            (pawn2, pawn1)
        };

        hasher.write_u8(pawn1.position().row);
        hasher.write_u8(pawn1.position().column);
        hasher.write_u8(pawn2.position().row);
        hasher.write_u8(pawn2.position().column);

        let mut walls = self.walls().clone();
        walls.sort_by(|a, b| a.position().cmp(b.position()).then_with(|| a.direction().cmp(b.direction())));

        for wall in &walls {
            hasher.write_u8(wall.position().row);
            hasher.write_u8(wall.position().column);
            hasher.write_u8(if wall.direction() == &WallDirection::Vertical { 1 } else { 0 });
        }

        hasher.write_u8(*self.remaining_walls().get(pawn1.player_id()).unwrap());
        hasher.write_u8(*self.remaining_walls().get(pawn2.player_id()).unwrap());

        hasher.finish()
    }
}

impl Default for Board {
    fn default() -> Self {
        Board::new()
    }
}

impl Clone for Board {
    fn clone(&self) -> Self {
        Board::construct_full(
            self.next_move_by.clone(),
            self.walls.clone(),
            self.pawns.clone(),
            self.remaining_walls.clone(),
            get_default_reachability_checker(),
            self.moves_history.clone(),
            self.shortest_route.clone(),
        )
    }
}

impl PartialEq for Board {
    fn eq(&self, other: &Self) -> bool {
        self.pawns == other.pawns
            && self.walls == other.walls
            && self.remaining_walls == other.remaining_walls
            && self.next_move_by == other.next_move_by
    }
}

lazy_static! {
    static ref NORTH_WINNER_POSITIONS: HashSet<BoardPosition> = {
        let mut possible_positions = HashSet::new();
        for column in 0..=8 {
            possible_positions.insert(BoardPosition::new(column, 0));
        }
        possible_positions
    };
    static ref SOUTH_WINNER_POSITIONS: HashSet<BoardPosition> = {
        let mut possible_positions = HashSet::new();
        for column in 0..=8 {
            possible_positions.insert(BoardPosition::new(column, 8));
        }
        possible_positions
    };
    static ref EAST_WINNER_POSITIONS: HashSet<BoardPosition> = {
        let mut possible_positions = HashSet::new();
        for row in 0..=8 {
            possible_positions.insert(BoardPosition::new(8, row));
        }
        possible_positions
    };
    static ref WEST_WINNER_POSITIONS: HashSet<BoardPosition> = {
        let mut possible_positions = HashSet::new();
        for row in 0..=8 {
            possible_positions.insert(BoardPosition::new(0, row));
        }
        possible_positions
    };
    static ref ALL_POSSIBLE_WALLS: HashSet<Wall> = {
        let mut walls = HashSet::new();
        for col in 0..8 {
            for row in 1..9 {
                walls.insert(Wall::new(BoardPosition::new(col, row), WallDirection::Vertical));
                walls.insert(Wall::new(BoardPosition::new(col, row), WallDirection::Horizontal));
            }
        }
        walls
    };
}

impl Into<MoveDirection> for Direction {
    fn into(self) -> MoveDirection {
        match &self {
            Direction::North => MoveDirection::North,
            Direction::South => MoveDirection::North,
            Direction::West => MoveDirection::East,
            Direction::East => MoveDirection::West,
        }
    }
}

impl Direction {
    pub fn opposite(&self) -> Direction {
        match self {
            Direction::North => Direction::South,
            Direction::South => Direction::North,
            Direction::East => Direction::West,
            Direction::West => Direction::East,
        }
    }

    pub fn perpendicular(&self) -> Direction {
        match self {
            Direction::North => Direction::East,
            Direction::South => Direction::West,
            Direction::East => Direction::South,
            Direction::West => Direction::North,
        }
    }

    pub fn winner_positions(&self) -> &HashSet<BoardPosition> {
        match &self {
            Direction::West => &WEST_WINNER_POSITIONS,
            Direction::North => &NORTH_WINNER_POSITIONS,
            Direction::East => &EAST_WINNER_POSITIONS,
            Direction::South => &SOUTH_WINNER_POSITIONS,
        }
    }
}

impl GameMoveByPlayer {
    pub fn new(player_id: PlayerId, game_move: GameMove) -> Self {
        GameMoveByPlayer { player_id, game_move }
    }
}

impl Pawn {
    pub fn construct(player_id: PlayerId, origin: Direction, position: BoardPosition) -> Self {
        Self { player_id, origin, position }
    }

    pub fn new(player_id: PlayerId, origin: Direction) -> Self {
        Self {
            player_id,
            origin,
            position: match origin {
                Direction::North => BoardPosition::new(4, 0),
                Direction::South => BoardPosition::new(4, 8),
                Direction::West => BoardPosition::new(0, 4),
                Direction::East => BoardPosition::new(8, 4),
            },
        }
    }

    pub fn apply_move(&self, direction: MoveDirection) -> Self {
        Self {
            player_id: self.player_id.clone(),
            origin: self.origin,
            position: self.position.apply_move(direction),
        }
    }

    pub fn direction_to_neighbor(&self, to_pawn: BoardPosition) -> Option<MoveDirection> {
        self.position().direction_to_neighbor_cell(to_pawn)
    }

    pub fn apply_move_checked(&self, direction: MoveDirection) -> Option<Self> {
        let position = self.position.apply_move_checked(direction)?;
        Some(Self { player_id: self.player_id.clone(), origin: self.origin, position })
    }

    pub fn player_id(&self) -> &PlayerId {
        &self.player_id
    }

    pub fn position(&self) -> &BoardPosition {
        &self.position
    }

    pub fn origin(&self) -> &Direction {
        &self.origin
    }

    pub fn set_origin(&mut self, direction: Direction) {
        self.origin = direction;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::notation::fen_notation::FenNotationDecoder;

    #[test]
    fn winner_no_winner() {
        let game = Board::new();
        assert_eq!(game.winner(), None);
    }

    #[test]
    fn is_valid_should_return_side_is_not_reachable() {
        let board = Board::from_fen_notation(
            "H: c5e5h5g7 / V: b2b4f8b8f6g6 / e9 e1 / 5 5  / 0",
        )
            .unwrap();

        assert_eq!(GameStateValidity::SideIsNotReachable, board.is_valid());
    }

    #[test]
    fn is_valid_should_return_side_is_not_rechable2() {
        let board = Board::from_fen_notation(
            "H: b2d2f2b3b9d9f9g8g3h2a2 / V: c3f3a4a6a8f8h8h6h4 / e9 h9 i2 a2  /  / 0",
        )
            .unwrap();

        assert_eq!(GameStateValidity::SideIsNotReachable, board.is_valid());
    }

    #[test]
    fn is_valid_should_return_invalid_next_player() {
        let board = Board::construct(
            PlayerId::new(5),
            vec![],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::South)],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
            Vec::new(),

        );
        assert_eq!(GameStateValidity::InvalidNextMovePlayerId, board.is_valid());
    }

    #[test]
    fn is_valid_should_return_invalid_pawns_origins() {
        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::East)],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidPawnsOrigins, board.is_valid());
    }

    #[test]
    fn is_valid_should_return_invalid_pawns_positions() {
        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::North)],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::PawnsOverlap, board.is_valid());

        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![
                Pawn::construct(PlayerId::new(0), Direction::North, BoardPosition::new(0, 9)),
                Pawn::construct(PlayerId::new(1), Direction::South, BoardPosition::new(9, 4)),
            ],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::PawnLocatedOutsideOfTheBoard, board.is_valid());
    }

    #[test]
    fn is_valid_should_return_invalid_pawns_amount() {
        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![Pawn::new(PlayerId::new(0), Direction::West)],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidPawnsAmount, board.is_valid());

        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![
                Pawn::new(PlayerId::new(0), Direction::West),
                Pawn::new(PlayerId::new(1), Direction::North),
                Pawn::new(PlayerId::new(2), Direction::South),
            ],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidPawnsAmount, board.is_valid());

        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![
                Pawn::new(PlayerId::new(0), Direction::West),
                Pawn::new(PlayerId::new(1), Direction::North),
                Pawn::new(PlayerId::new(2), Direction::South),
                Pawn::new(PlayerId::new(1), Direction::East),
                Pawn::new(PlayerId::new(2), Direction::South),
            ],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidPawnsAmount, board.is_valid());
    }

    #[test]
    fn is_valid_should_return_invalid_walls_amount() {
        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::South)],
            Default::default(),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidWallsAmount, board.is_valid());

        let board = Board::construct(
            PlayerId::new(0),
            vec![
                Wall::new(BoardPosition::new(1, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(2, 3), WallDirection::Vertical),
            ],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::South)],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidWallsAmount, board.is_valid());
    }

    #[test]
    fn is_valid_should_return_invalid_walls_positions() {
        let board = Board::construct(
            PlayerId::new(0),
            vec![
                Wall::new(BoardPosition::new(1, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(1, 1), WallDirection::Vertical),
                Wall::new(BoardPosition::new(4, 4), WallDirection::Vertical),
            ],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::South)],
            HashMap::from_iter(vec![(PlayerId::new(0), 9), (PlayerId::new(1), 8)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidWallsPositions, board.is_valid());

        let board = Board::construct(
            PlayerId::new(0),
            vec![
                Wall::new(BoardPosition::new(1, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(2, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(4, 4), WallDirection::Vertical),
            ],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::South)],
            HashMap::from_iter(vec![(PlayerId::new(0), 9), (PlayerId::new(1), 8)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidWallsPositions, board.is_valid());

        let board = Board::construct(
            PlayerId::new(0),
            vec![
                Wall::new(BoardPosition::new(1, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(0, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(4, 4), WallDirection::Vertical),
            ],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::South)],
            HashMap::from_iter(vec![(PlayerId::new(0), 9), (PlayerId::new(1), 8)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidWallsPositions, board.is_valid());

        let board = Board::construct(
            PlayerId::new(0),
            vec![
                Wall::new(BoardPosition::new(1, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(4, 5), WallDirection::Vertical),
                Wall::new(BoardPosition::new(4, 4), WallDirection::Vertical),
            ],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::South)],
            HashMap::from_iter(vec![(PlayerId::new(0), 9), (PlayerId::new(1), 8)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidWallsPositions, board.is_valid());

        let board = Board::construct(
            PlayerId::new(0),
            vec![
                Wall::new(BoardPosition::new(1, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(4, 5), WallDirection::Vertical),
                Wall::new(BoardPosition::new(4, 6), WallDirection::Vertical),
            ],
            vec![Pawn::new(PlayerId::new(0), Direction::North), Pawn::new(PlayerId::new(1), Direction::South)],
            HashMap::from_iter(vec![(PlayerId::new(0), 9), (PlayerId::new(1), 8)].into_iter()),
            Vec::new(),
        );
        assert_eq!(GameStateValidity::InvalidWallsPositions, board.is_valid());
    }

    #[test]
    fn test_winner() {
        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![
                Pawn::construct(PlayerId::new(1), Direction::North, BoardPosition::new(2, 7)),
                Pawn::construct(PlayerId::new(0), Direction::North, BoardPosition::new(2, 8)),
            ],
            HashMap::new(),
            Vec::new(),
        );
        assert_eq!(Some(&PlayerId::new(0)), board.winner());

        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![
                Pawn::construct(PlayerId::new(1), Direction::South, BoardPosition::new(2, 1)),
                Pawn::construct(PlayerId::new(0), Direction::South, BoardPosition::new(2, 0)),
            ],
            HashMap::new(),
            Vec::new(),
        );
        assert_eq!(Some(&PlayerId::new(0)), board.winner());

        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![
                Pawn::construct(PlayerId::new(0), Direction::West, BoardPosition::new(7, 2)),
                Pawn::construct(PlayerId::new(0), Direction::West, BoardPosition::new(8, 2)),
            ],
            HashMap::new(),
            Vec::new(),
        );
        assert_eq!(Some(&PlayerId::new(0)), board.winner());

        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![
                Pawn::construct(PlayerId::new(0), Direction::East, BoardPosition::new(1, 2)),
                Pawn::construct(PlayerId::new(0), Direction::East, BoardPosition::new(0, 2)),
            ],
            HashMap::new(),
            Vec::new(),
        );
        assert_eq!(Some(&PlayerId::new(0)), board.winner());
    }

    #[test]
    fn test_get_possible_moves_1() {
        let board =
            Board::from_fen_notation("H: e4c8f5h5g2e2c2 / V: d5d7b9g4h3 / e6 b4 f6 e7  /  / 0")
                .unwrap();
        println!("{:?}", board.get_possible_moves(&PlayerId::new(0)));
    }

    #[test]
    fn test_get_possible_moves_2() {
        let board =
            Board::from_fen_notation("H: / V: / f5 f6  /  / 0")
                .unwrap();
        println!("{:?}", board.get_possible_moves(&PlayerId::new(0)).into_iter().map(|m| match m {
            GameMove::MovePawn(direction) => Some(GameMove::MovePawn(direction)),
            GameMove::PlaceWall(_) => None,
        }).filter(|e| e.is_some()).map(|e| e.unwrap()).collect::<HashSet<GameMove>>());
    }

    #[test]
    fn all_possible_walls_should_include_column7() {
        // why? see docs/assets/1.png
        assert!(ALL_POSSIBLE_WALLS.iter().find(|wall| wall.position.column == 7).is_some());
    }
}
