use serde::{Serialize, Deserialize};
use crate::board::MoveDirection;
use crate::notation::error::NotationError;

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, Copy, Ord, PartialOrd)]
pub struct BoardPosition {
    pub column: u8,
    // A = 0 (left), I = 8 (right)
    pub row: u8, // 0 (top) to 8 (bottom).
}

impl BoardPosition {
    pub fn new(column: u8, row: u8) -> Self {
        Self { column, row }
    }

    pub fn from_bytes([column, row]: [char; 2]) -> Result<BoardPosition, NotationError> {
        match (column, row) {
            ('a'..='i', '1'..='9') => {
                Ok(Self { column: (column as u8) - 97, row: (row as u8) - 49 })
            }
            _ => Err(NotationError::PositionFormat(format!("{}{}", column, row))),
        }
    }

    pub fn column(&self) -> u8 {
        self.column
    }

    pub fn row(&self) -> u8 {
        self.row
    }

    pub fn apply_move(&self, direction: MoveDirection) -> Self {
        match direction {
            MoveDirection::North => Self { column: self.column, row: self.row - 1 },
            MoveDirection::JumpNorth => Self { column: self.column, row: self.row - 2 },
            MoveDirection::NorthWest => Self { column: self.column - 1, row: self.row - 1 },
            MoveDirection::NorthEast => Self { column: self.column + 1, row: self.row - 1 },
            MoveDirection::East => Self { column: self.column + 1, row: self.row },
            MoveDirection::JumpEast => Self { column: self.column + 2, row: self.row },
            MoveDirection::South => Self { column: self.column, row: self.row + 1 },
            MoveDirection::JumpSouth => Self { column: self.column, row: self.row + 2 },
            MoveDirection::SouthEast => Self { column: self.column + 1, row: self.row + 1 },
            MoveDirection::SouthWest => Self { column: self.column - 1, row: self.row + 1 },
            MoveDirection::West => Self { column: self.column - 1, row: self.row },
            MoveDirection::JumpWest => Self { column: self.column - 2, row: self.row },
        }
    }

    pub fn direction_to_neighbor_cell(&self, to_cell: BoardPosition) -> Option<MoveDirection> {
        let col_diff = self.column as i16 - to_cell.column as i16;
        let row_diff = self.row as i16 - to_cell.row as i16;
        match (col_diff, row_diff) {
            (0, 1) => Some(MoveDirection::North),
            (0, 2) => Some(MoveDirection::JumpNorth),
            (1, 1) => Some(MoveDirection::NorthWest),
            (-1, 1) => Some(MoveDirection::NorthEast),
            (-1, 0) => Some(MoveDirection::East),
            (-2, 0) => Some(MoveDirection::JumpEast),
            (0, -1) => Some(MoveDirection::South),
            (0, -2) => Some(MoveDirection::JumpSouth),
            (1, -1) => Some(MoveDirection::SouthWest),
            (-1, -1) => Some(MoveDirection::SouthEast),
            (1, 0) => Some(MoveDirection::West),
            (2, 0) => Some(MoveDirection::JumpWest),
            _ => None,
        }
    }

    pub fn apply_move_checked(&self, direction: MoveDirection) -> Option<Self> {
        match direction {
            MoveDirection::North => {
                if self.row > 0 {
                    Some(Self { column: self.column, row: self.row - 1 })
                } else {
                    None
                }
            }
            MoveDirection::JumpNorth => {
                if self.row > 1 {
                    Some(Self { column: self.column, row: self.row - 2 })
                } else {
                    None
                }
            }
            MoveDirection::NorthWest => {
                if self.row > 0 && self.column > 0 {
                    Some(Self { column: self.column - 1, row: self.row - 1 })
                } else {
                    None
                }
            }
            MoveDirection::NorthEast => {
                if self.row > 0 && self.column < 8 {
                    Some(Self { column: self.column + 1, row: self.row - 1 })
                } else {
                    None
                }
            }
            MoveDirection::East => {
                if self.column < 8 {
                    Some(Self { column: self.column + 1, row: self.row })
                } else {
                    None
                }
            }
            MoveDirection::JumpEast => {
                if self.column < 7 {
                    Some(Self { column: self.column + 2, row: self.row })
                } else {
                    None
                }
            }
            MoveDirection::South => {
                if self.row < 8 {
                    Some(Self { column: self.column, row: self.row + 1 })
                } else {
                    None
                }
            }
            MoveDirection::JumpSouth => {
                if self.row < 7 {
                    Some(Self { column: self.column, row: self.row + 2 })
                } else {
                    None
                }
            }
            MoveDirection::SouthWest => {
                if self.row < 8 && self.column > 0 {
                    Some(Self { column: self.column - 1, row: self.row + 1 })
                } else {
                    None
                }
            }
            MoveDirection::SouthEast => {
                if self.row < 8 && self.column < 8 {
                    Some(Self { column: self.column + 1, row: self.row + 1 })
                } else {
                    None
                }
            }
            MoveDirection::West => {
                if self.column > 0 {
                    Some(Self { column: self.column - 1, row: self.row })
                } else {
                    None
                }
            }
            MoveDirection::JumpWest => {
                if self.column > 1 {
                    Some(Self { column: self.column - 2, row: self.row })
                } else {
                    None
                }
            }
        }
    }

    pub fn distance_to(&self, other: &BoardPosition) -> u32 {
        (
            (other.column as i32 - self.column as i32).pow(2) +
                (other.row as i32 - self.row as i32).pow(2)
        ) as u32
    }
}
