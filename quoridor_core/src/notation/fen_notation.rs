use std::collections::{BTreeMap, HashMap};
use std::convert::TryFrom;
use crate::board::{Board, Direction, Pawn, Wall, WallDirection};
use crate::position::BoardPosition;
use crate::notation::error::NotationError;
use crate::player::PlayerId;

pub trait FenNotationEncoder {
    fn to_fen_notation(&self) -> String;
}

pub trait FenNotationDecoder {
    fn from_fen_notation(fen: &str) -> Result<Board, NotationError>;
}

fn parse_walls(coordinates: &str, direction: WallDirection) -> Result<Vec<Wall>, NotationError> {
    if coordinates.len() % 2 != 0 {
        return Err(NotationError::BadWallsCoordinates);
    }
    let mut h = Vec::new();
    for position in coordinates.chars().collect::<Vec<_>>().chunks(2) {
        h.push(Wall::new(
            BoardPosition::from_bytes(<[char; 2]>::try_from(position).unwrap())?,
            direction,
        ));
    }
    Ok(h)
}

impl FenNotationEncoder for Board {
    fn to_fen_notation(&self) -> String {
        let mut h_walls = "".to_owned();
        let mut v_walls = "".to_owned();
        let mut players = "".to_owned();
        let mut r_walls = "".to_owned();

        self.walls().iter().for_each(|wall| match wall.direction() {
            WallDirection::Horizontal => h_walls.push_str(&wall.position().to_string()),
            WallDirection::Vertical => v_walls.push_str(&wall.position().to_string()),
        });

        let mut pawns = self.pawns().clone();
        pawns.sort_by_key(|a| a.player_id().clone());
        pawns.iter().for_each(|pawn| {
            let mut p = pawn.position().to_string();
            p.push(' ');
            players.push_str(&p);
        });

        self.remaining_walls().iter().collect::<BTreeMap<_, _>>().iter().for_each(|(_, v)| {
            let mut v = v.to_string();
            v.push(' ');
            r_walls.push_str(&v);
        });

        format!(
            "H: {} / V: {} / {} / {} / {}",
            h_walls,
            v_walls,
            players,
            r_walls,
            self.next_move_by().to_string()
        )
    }
}

impl FenNotationDecoder for Board {
    fn from_fen_notation(fen: &str) -> Result<Board, NotationError> {
        let data = fen.split('/').collect::<Vec<_>>();
        if data.len() != 5 {
            return Err(NotationError::NotationFormat(
                "expected 5 parts separated by '/'".to_owned(),
            ));
        }

        let h_walls = data[0].trim();
        if !h_walls.starts_with("H:") {
            return Err(NotationError::NotationFormat(
                "first part should start with 'H:'".to_owned(),
            ));
        }
        let mut walls = parse_walls(&h_walls[2..].trim(), WallDirection::Horizontal)?;

        let v_walls = data[1].trim();
        if !v_walls.starts_with("V:") {
            return Err(NotationError::NotationFormat(
                "second part should start with 'V:'".to_owned(),
            ));
        }
        let v_walls = parse_walls(&v_walls[2..].trim(), WallDirection::Vertical)?;
        walls.extend_from_slice(&v_walls);

        let mut player_id = 0;
        let mut pawns = Vec::new();
        for position in data[2].trim().split(' ').into_iter().map(|b| b.chars().collect::<Vec<_>>())
        {
            if position.len() != 2 {
                return Err(NotationError::PositionFormat(position.into_iter().collect()));
            }
            pawns.push(Pawn::construct(
                PlayerId::new(player_id),
                Direction::North,
                BoardPosition::from_bytes(<[char; 2]>::try_from(position).unwrap())?,
            ));
            player_id += 1;
        }
        match pawns.len() {
            2 => {
                pawns[0].set_origin(Direction::South);
                pawns[1].set_origin(Direction::North);
            }
            4 => {
                pawns[0].set_origin(Direction::South);
                pawns[1].set_origin(Direction::West);
                pawns[2].set_origin(Direction::North);
                pawns[3].set_origin(Direction::East);
            }
            _ => return Err(NotationError::PlayersAmount(pawns.len() as u8)),
        };

        player_id = 0;
        let mut remaining_walls = HashMap::new();
        for walls in data[3].trim().split(' ').collect::<Vec<_>>() {
            if walls.is_empty() {
                continue;
            }
            let walls =
                walls.parse::<u8>().map_err(|err| NotationError::NumberFormat(err.to_string()))?;
            match walls {
                0..=10 => remaining_walls.insert(PlayerId::new(player_id), walls),
                _ => return Err(NotationError::RemainingWalls(walls)),
            };
            player_id += 1;
        }

        let next_move_by = data[4]
            .trim()
            .parse::<usize>()
            .map(|v| PlayerId::new(v))
            .map_err(|err| NotationError::NumberFormat(err.to_string()))?;

        Ok(Board::construct(next_move_by, walls, pawns, remaining_walls, Vec::new()))
    }
}

#[cfg(test)]
mod tests {
    use std::array::IntoIter;
    use std::iter::FromIterator;
    use super::*;

    #[test]
    fn test_serialize() {
        // this board may not be valid. It's just for serialization testing
        let board_data = "H: c4 / V:  / e7 f2  / 8 7  / 1";
        let board = Board::construct(
            PlayerId::new(1),
            vec![Wall::new(BoardPosition::new(2, 3), WallDirection::Horizontal)],
            vec![
                Pawn::construct(PlayerId::new(0), Direction::South, BoardPosition::new(4, 6)),
                Pawn::construct(PlayerId::new(1), Direction::North, BoardPosition::new(5, 1)),
            ],
            HashMap::from_iter(IntoIter::new([(PlayerId::new(0), 8), (PlayerId::new(1), 7)])),
            Vec::new(),
        );
        dbg!(&board);
        let res = board.to_fen_notation();
        dbg!(&res);
        assert_eq!(res, board_data);
    }

    #[test]
    fn test_deserialize_simple() {
        // this board may not be valid. It's just for deserialization testing
        let board_data = "H: c4 / V:  / e7 f2  / 8 7  / 1";
        let expected_board = Board::construct(
            PlayerId::new(1),
            vec![Wall::new(BoardPosition::new(2, 3), WallDirection::Horizontal)],
            vec![
                Pawn::construct(PlayerId::new(0), Direction::South, BoardPosition::new(4, 6)),
                Pawn::construct(PlayerId::new(1), Direction::North, BoardPosition::new(5, 1)),
            ],
            HashMap::from_iter(IntoIter::new([(PlayerId::new(0), 8), (PlayerId::new(1), 7)])),
            Vec::new(),
        );
        let board = Board::from_fen_notation(board_data);
        dbg!(&board);
        assert!(board.is_ok());
        assert_eq!(expected_board, board.unwrap());
    }

    #[test]
    fn test_deserialize_2_complex() {
        let board_data = "H: e4c8f5h5g2e2c2 / V: d5d7b9g4h3 / e6 b4 f6 e7  /  / 0";
        let expected_board = Board::construct(
            PlayerId::new(0),
            vec![
                Wall::new(BoardPosition::new(4, 3), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(2, 7), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(5, 4), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(7, 4), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(6, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(4, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(2, 1), WallDirection::Horizontal),
                Wall::new(BoardPosition::new(3, 4), WallDirection::Vertical),
                Wall::new(BoardPosition::new(3, 6), WallDirection::Vertical),
                Wall::new(BoardPosition::new(1, 8), WallDirection::Vertical),
                Wall::new(BoardPosition::new(6, 3), WallDirection::Vertical),
                Wall::new(BoardPosition::new(7, 2), WallDirection::Vertical),
            ],
            vec![
                Pawn::construct(PlayerId::new(0), Direction::South, BoardPosition::new(4, 5)),
                Pawn::construct(PlayerId::new(1), Direction::West, BoardPosition::new(1, 3)),
                Pawn::construct(PlayerId::new(2), Direction::North, BoardPosition::new(5, 5)),
                Pawn::construct(PlayerId::new(3), Direction::East, BoardPosition::new(4, 6)),
            ],
            HashMap::new(),
            Vec::new(),
        );
        let board = Board::from_fen_notation(board_data);
        dbg!(&board);
        assert!(board.is_ok());
        assert_eq!(expected_board, board.unwrap());
    }
}
