use thiserror::Error;

#[derive(Error, Debug)]
pub enum NotationError {
    #[error("bad walls coordinates")]
    BadWallsCoordinates,
    #[error("bad notation format: {0}")]
    NotationFormat(String),
    #[error("wrong position format: expected two valid symbols but got {0}")]
    PositionFormat(String),
    #[error("wrong players amount: expected 2 or 4 players but find {0}")]
    PlayersAmount(u8),
    #[error("wrong amount of remaining walls: expected from 0 to 10 but got {0}")]
    RemainingWalls(u8),
    #[error("Can not parse number: {0}")]
    NumberFormat(String),
}
