use thiserror::Error;
use crate::board::{GameMove, Wall, Pawn, WallDirection, MoveDirection};
use crate::notation::ai_tester::AiTesterNotationError::UnexpectedMoveType;
use crate::position::BoardPosition;

#[derive(Debug, Error, Clone)]
pub enum AiTesterNotationError {
    #[error("Unexpected move type: {0:?}")]
    UnexpectedMoveType(String),
    #[error("Unexpected wall column: {0:?}")]
    UnexpectedWallColumn(String),
}

pub trait AiTesterNotationDecoder {
    fn from_ai_tester_notation(ai_tester_notation: &str, pawn: &Pawn) -> Result<Self, AiTesterNotationError> where Self: Sized;
}

pub trait AiTesterNotationEncoder {
    fn to_ai_tester_notation(&self, pawn: &Pawn) -> String;
}

impl AiTesterNotationDecoder for GameMove {
    fn from_ai_tester_notation(ai_tester_notation: &str, pawn: &Pawn) -> Result<Self, AiTesterNotationError> {
        let line: Vec<&str> = ai_tester_notation.split(" ").collect();

        Ok(match line[0] {
            "move" => read_pawn_move(pawn, line[1]),
            "jump" => read_pawn_move(pawn, line[1]),
            "wall" => read_wall_move(line[1])?,
            other => return Err(UnexpectedMoveType(other.to_string())),
        })
    }
}

impl AiTesterNotationEncoder for GameMove {
    fn to_ai_tester_notation(&self, pawn: &Pawn) -> String {
        match &self {
            GameMove::MovePawn(move_direction) => pawn_move_to_string(pawn, move_direction),
            GameMove::PlaceWall(wall) => wall_move_to_string(wall),
        }
    }
}

fn pawn_move_to_string(pawn: &Pawn, move_direction: &MoveDirection) -> String {
    let move_type = match move_direction {
        MoveDirection::North | MoveDirection::East | MoveDirection::South | MoveDirection::West => "move".to_string(),
        _ => "jump".to_string()
    };

    format!("{} {}", move_type, pawn.position().apply_move(*move_direction).to_string().to_uppercase())
}

fn wall_move_to_string(wall: &Wall) -> String {
    format!("wall {}{}", wall_position_to_string(wall.position()), match wall.direction() {
        WallDirection::Vertical => "v",
        WallDirection::Horizontal => "h"
    })
}

fn wall_position_to_string(pos: &BoardPosition) -> String {
    format!("{}{}", match pos.column {
        0 => "S",
        1 => "T",
        2 => "U",
        3 => "V",
        4 => "W",
        5 => "X",
        6 => "Y",
        7 => "Z",
        other => panic!("Unexpected column for wall position: {}", other),
    }, pos.row)
}

fn read_pawn_move(pawn: &Pawn, position: &str) -> GameMove {
    let position = position.to_lowercase();
    let pos = BoardPosition::from_bytes([
        position.chars().nth(0).expect("Expected position to contain char for column"),
        position.chars().nth(1).expect("Expected position to contain char for row")
    ]).expect("Failed to parse board position");

    let pawn_position = pawn.position();
    let move_direction = pawn_position.direction_to_neighbor_cell(pos)
        .expect(&format!("failed to get direction from {:?} to {:?}", pawn_position, pos));
    GameMove::MovePawn(move_direction)
}

fn read_wall_move(wall_move: &str) -> Result<GameMove, AiTesterNotationError> {
    let wall_direction = if wall_move.ends_with("h") {
        WallDirection::Horizontal
    } else if wall_move.ends_with("v") {
        WallDirection::Vertical
    } else {
        panic!("Unknown wall direction for wall coordinates: {}", wall_move);
    };

    let column: u8 = match &wall_move.to_uppercase().chars().nth(0).unwrap() {
        'S' => 0,
        'T' => 1,
        'U' => 2,
        'V' => 3,
        'W' => 4,
        'X' => 5,
        'Y' => 6,
        'Z' => 7,
        _  => return Err(AiTesterNotationError::UnexpectedWallColumn(wall_move.to_owned())),
    };
    let row: u8 = wall_move[1..2].parse().unwrap();
    Ok(GameMove::PlaceWall(Wall::new(BoardPosition::new(column, row), wall_direction)))
}

#[cfg(test)]
mod tests {
    use crate::board::{GameMove, Wall, WallDirection};
    use crate::position::BoardPosition;
    use super::*;

    pub fn test_read_wall_move() {
        assert_eq!(
            GameMove::PlaceWall(Wall::new(BoardPosition::new(2, 8), WallDirection::Vertical)),
            read_wall_move("U8v").unwrap()
        );
    }
}