use std::borrow::Borrow;
use std::collections::HashMap;
use cursive::traits::Nameable;
use cursive::view::{Margins, Selector};
use cursive::views::{NamedView, PaddedView, ResizedView};
use cursive::{
    views::{Button, Dialog, LinearLayout, TextView},
    Cursive, View,
};
use quoridor_core::position::BoardPosition;
use quoridor_core::board::{Direction, GameMove, Wall, WallDirection};
use quoridor_core::player::PlayerId;
use crate::consts;
use crate::consts::BOARD_SIZE;
use crate::board_layout::BoardState;

pub fn run() {
    let mut siv = cursive::default();
    siv.load_toml(include_str!("../Theme.toml")).unwrap();

    siv.add_global_callback('q', |s| s.quit());
    siv.add_layer(wrap_into_dialog(create_start_window(), "Game board"));
    siv.run();
}

fn load_gameplay(s: &mut Cursive) {
    let board_state = BoardState::default();
    s.set_user_data(board_state);
    render_board(s)
}

fn render_board(state: &mut Cursive) {
    let menu_buttons = create_menu_button_layout();
    let game_context: &mut BoardState = state.user_data().unwrap();
    let game_info_bar = ResizedView::with_fixed_width(
        45,
        PaddedView::new(
            Margins::lr(1, 5),
            LinearLayout::vertical()
                .child(TextView::new(format!(
                    "Next move by:: {}  ",
                    game_context.get_label_of_active_player()
                )))
                .child(TextView::new("Walls remaining (player_id, walls_num): "))
                .child(TextView::new(build_remaining_walls_str(
                    game_context.get_player_rem_walls(),
                ))),
        ),
    );
    let board = create_board_layout(game_context);

    render_layer(
        state,
        LinearLayout::vertical().child(board).child(PaddedView::new(
            Margins::tb(1, 1),
            LinearLayout::horizontal()
                .child(game_info_bar)
                .child(menu_buttons),
        )),
        "Quoridor",
    );
}

fn build_remaining_walls_str(rem_walls: &HashMap<PlayerId, u8>) -> String {
    let mut msg: String = "".to_owned();
    for (id, walls_left) in rem_walls.iter() {
        msg += &format!("| {}::{} |", id.to_string(), walls_left);
    }

    msg
}

fn create_board_layout(state: &mut BoardState) -> LinearLayout {
    let mut board = LinearLayout::horizontal();
    for col_id in 0..BOARD_SIZE {
        board.add_child(create_bord_col_layout(state, col_id));
    }
    board.set_focus_index(state.get_pos_of_active_player().position().column as usize);

    match board.focus_view(Selector::Name(consts::ACTIVE_PLAYER_COL).borrow()) {
        Ok(result) => {}
        // todo there should add logging. Cursive logger -- not suitable.
        Err(err) => {}
    }

    board
}

fn create_bord_col_layout(state: &mut BoardState, col: u8) -> NamedView<LinearLayout> {
    let mut column = LinearLayout::vertical();
    for row in 0..BOARD_SIZE {
        // 1 (00) 2 (01)
        // (00)   (01)
        column = create_wall_cell(state, column, WallDirection::Horizontal, col, row);

        column.add_child(create_cell_with_border(state, col, row));
    }
    match column.focus_view(Selector::Name(consts::ACTIVE_PLAYER_CELL).borrow()) {
        Ok(result) => {}
        // todo there should be logger
        Err(err) => {}
    }
    if col == state.get_pos_of_active_player().position().column {
        column.with_name(consts::ACTIVE_PLAYER_COL)
    } else {
        column.with_name("")
    }
}

fn create_cell_with_border(state: &mut BoardState, col: u8, row: u8) -> LinearLayout {
    let curr_pos = BoardPosition::new(col, row);
    let from_cell = state.get_pos_of_active_player();
    let dir = from_cell.direction_to_neighbor(curr_pos);

    let on_press: Box<dyn Fn(&mut Cursive)> = match dir {
        None => Box::new(move |s: &mut Cursive| {
            render_layer(s, create_error_layout(), "Error occurs!!!")
        }),
        Some(..) => {
            Box::new(move |s: &mut Cursive| update_board_state(s, GameMove::MovePawn(dir.unwrap())))
        }
    };

    let pawn = create_cell(false, state.get_pawn_label(curr_pos), on_press);

    let named = if state.is_pawn_active(curr_pos) {
        pawn.with_name(consts::ACTIVE_PLAYER_CELL)
    } else {
        pawn.with_name("")
    };

    let border_cell = LinearLayout::horizontal().child(named);
    create_wall_cell(state, border_cell, WallDirection::Vertical, col, row)
}

fn create_wall_cell(
    state: &mut BoardState,
    layout: LinearLayout,
    wall_dir: WallDirection,
    col: u8,
    row: u8,
) -> LinearLayout {
    let condition = match wall_dir {
        WallDirection::Vertical => col % BOARD_SIZE != 0 || col == 0,
        WallDirection::Horizontal => row != 0,
    };

    let wall = Wall::new(BoardPosition::new(col, row), wall_dir);

    if condition {
        return layout.child(create_cell(
            true,
            state.get_wall_label(BoardPosition::new(col, row), &wall_dir),
            Box::new(move |s| {
                update_board_state(s, GameMove::PlaceWall(wall));
            }),
        ));
    }
    layout
}

fn create_cell(is_blank: bool, state: String, f: Box<dyn Fn(&mut Cursive)>) -> Button {
    Button::new_raw(
        if is_blank {
            state
        } else {
            format!("{}{}{}", "[", state, "]")
        },
        f,
    )
}

fn update_board_state(s: &mut Cursive, game_move: GameMove) {
    let data = s
        .with_user_data(|state: &mut BoardState| state.apply_board_move(game_move))
        .unwrap();
    s.set_user_data(data.clone());
    let winner = data.check_on_winner();
    match winner {
        None => render_board(s),
        Some(..) => render_winner_window(s, winner.unwrap()),
    }
}

fn create_error_layout(// move_error: &MoveError
) -> LinearLayout {
    LinearLayout::vertical()
        .child(TextView::new(format!(
            "Unable do this move. Error with code::{}",
            // move_error.to_string()
            "unknown"
        )))
        .child(Button::new("back", |s| render_board(s)))
}

fn render_winner_window(s: &mut Cursive, winner: (&PlayerId, &String)) {
    render_layer(
        s,
        LinearLayout::vertical()
            .child(TextView::new(format!(
                "Player with id::{} and label::{} won",
                winner.0.to_string(), winner.1,
            )))
            .child(create_menu_button_layout()),
        "Congratulations!!!",
    )
}

fn create_menu_button_layout() -> LinearLayout {
    LinearLayout::vertical()
        .child(Button::new("home", |s| {
            render_layer(s, create_start_window(), "Game board")
        }))
        .child(create_exit_button())
        .child(Button::new("restart", |s| load_gameplay(s)))
}

fn create_menu_layout() -> LinearLayout {
    LinearLayout::vertical()
        .child(TextView::new("Choose play mode ..."))
        .child(Button::new("one player", |s| load_gameplay(s)))
        .child(Button::new("two players", |s| load_gameplay(s)))
}

fn create_start_window() -> LinearLayout {
    LinearLayout::horizontal()
        .child(TextView::new(
            "Here is Quoridor!\nPress <Next> when you're ready.",
        ))
        .child(Button::new("Next", move |s| {
            render_layer(s, create_menu_layout(), "menu")
        }))
}

fn create_exit_button() -> Button {
    Button::new("exit", |s| s.quit())
}

fn render_layer<V>(s: &mut Cursive, view: V, title: &str)
    where
        V: View,
{
    s.pop_layer();
    s.add_layer(wrap_into_dialog(view, title));
}

fn wrap_into_dialog<V>(view: V, title: &str) -> Dialog
    where
        V: View,
{
    Dialog::around(view).title(title)
}

// DON'T REMOVE :: that functions will be useful in future, probably...

// fn show_answer(s: &mut Cursive, msg: &str) {
//     s.pop_layer();
//     s.add_layer(
//         Dialog::text(msg)
//             .title("Results")
//             .button("Finish", |s| s.quit()),
//     );
// }
//
