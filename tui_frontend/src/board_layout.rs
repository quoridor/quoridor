extern crate quoridor_core as q_core;

use std::collections::HashMap;
use self::q_core::player::PlayerId;
use crate::consts::UNWRAP_NEXT_PLAYER;
use q_core::board::{
    Board, GameMove, GameMoveByPlayer, MoveError, Pawn, Wall, WallDirection,
};
use quoridor_core::position::BoardPosition;
use maplit::hashmap;
use std::borrow::{Borrow, BorrowMut};

#[derive(Clone, Debug)]
pub struct BoardState {
    move_err: Option<MoveError>,
    player_labels: HashMap<PlayerId, String>,
    board: Board,
}

fn process_vertical_wall(curr: BoardPosition, wall_pos: BoardPosition) -> Option<String> {
    let projection = vec![
        // 1 | 2    possible labels is (1) and (3)
        // 3 | 4
        BoardPosition::new(wall_pos.column, wall_pos.row - 1),
        BoardPosition::new(wall_pos.column, wall_pos.row),
    ];

    if projection.contains(&curr) {
        return Some(" | ".to_owned());
    }
    Option::None
}

fn process_horizontal_wall(curr: BoardPosition, wall_pos: BoardPosition) -> Option<String> {
    let projection = vec![
        // 1 2     we draw row with walls in iter with previous cell-row
        // - -     so we map to possible cases for (1) and (2)
        // 3 4
        BoardPosition::new(wall_pos.column + 1, wall_pos.row),
        BoardPosition::new(wall_pos.column, wall_pos.row),
    ];

    if projection.contains(&curr) {
        return Some("--- ".to_owned());
    }
    Option::None
}

impl BoardState {
    pub fn default() -> Self {
        BoardState {
            move_err: None,
            player_labels: hashmap![
                PlayerId::new(0) => "X".to_owned(),
                PlayerId::new(1) => "*".to_owned(),
            ],
            board: Board::default(),
        }
    }

    pub fn get_pawn_label(&self, curr_pos: BoardPosition) -> String {
        for (id, player_pos) in self.board.pawns().iter().enumerate() {
            if *player_pos.position() == curr_pos {
                return self.player_labels.get(&PlayerId::new(id)).unwrap().clone();
            }
        }

        " ".to_owned()
    }

    pub fn get_wall_label(&self, curr_pos: BoardPosition, wall_dir: &WallDirection) -> String {
        let typed_walls: Vec<&Wall> = self
            .board
            .walls()
            .iter()
            .filter(|w| w.direction() == wall_dir)
            .collect::<Vec<_>>();

        let process_fn = match wall_dir {
            WallDirection::Vertical => process_vertical_wall,
            WallDirection::Horizontal => process_horizontal_wall,
        };

        for wall in typed_walls.iter() {
            let label = process_fn(curr_pos, *wall.position());
            match label {
                None => (),
                Some(label) => return label,
            }
        }
        "   ".to_owned()
    }

    pub fn apply_board_move(&mut self, game_move: GameMove) -> BoardState {
        let prev = self.board.clone();
        let apply_result = self
            .board
            .apply_move(GameMoveByPlayer::new(self.board.next_move_by().clone(), game_move));
        match apply_result {
            Ok(result) => BoardState {
                move_err: Option::None,
                board: result,
                player_labels: self.player_labels().clone(),
            },
            Err(err) => BoardState {
                move_err: Some(err),
                board: prev,
                player_labels: self.player_labels().clone(),
            },
        }
    }

    pub fn is_pawn_active(&self, position: BoardPosition) -> bool {
        let pawn = self
            .board
            .pawns()
            .iter()
            .find(|p| *p.position() == position);
        match pawn {
            None => false,
            Some(pawn) => pawn.player_id() == self.board.next_move_by(),
        }
    }

    pub fn check_on_winner(&self) -> Option<(&PlayerId, &String)> {
        let winner = self.board.winner();
        winner.map(|winner| (winner, self.player_labels.get(&winner).unwrap()))
    }

    pub fn get_player_rem_walls(&self) -> &HashMap<PlayerId, u8> {
        self.board.remaining_walls()
    }

    pub fn get_pos_of_active_player(&self) -> &Pawn {
        let next_move = self.board.next_move_by();
        self.board.pawns().iter().find(|pawn| pawn.player_id() == next_move).expect(UNWRAP_NEXT_PLAYER)
    }

    pub fn get_label_of_active_player(&self) -> &String {
        let next_move = self.board.next_move_by();
        self.player_labels.get(next_move).unwrap()
    }
    pub fn player_labels(&self) -> &HashMap<PlayerId, String> {
        &self.player_labels
    }

    pub fn move_err(&self) -> &Option<MoveError> {
        &self.move_err
    }
}
