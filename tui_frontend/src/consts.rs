pub(crate) const BOARD_SIZE: u8 = 9;
pub const ACTIVE_PLAYER_CELL: &str = "active";
pub const ACTIVE_PLAYER_COL: &str = "active_col";
pub const UNWRAP_NEXT_PLAYER: &str =
    "I'm sure that we always have the pawn of the next player on the board";
