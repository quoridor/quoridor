# quoridor

Play it online here: [https://quoridor.nikitavbv.com](https://quoridor.nikitavbv.com).

# Features
- The game rules are completely implemented.
- 2 or 4 players can play locally (i.e. from the same device).
- You can play against **random** bots.
    - Well, almost random... We made it a bit smarter so it is not boring.
- You can play online with your friend or even four of you!
    - Just send them a link to join the game (games can be public or private).
- You can spectate games without joining them.
- There is a game log/game chat.
- There is also a terminal UI, if you prefer such things.
- Minimax bot implementation with alpha/beta pruning.
    - It works, but is a bit **slow**.

# How to start a game
- You can join a game room or create a new one from home page.
- Enter or generate fun names for your game rooms or players.
- Choose the style for your pawn.
    - There are four packs available: classic, emoji, memes or anime!
- You can restart the game after it finished.

# How to play
- In order to win, try to reach the other side of the board before your opponents.
- You have 10 bricks (or fences) in 2-player mode, and 5 in 4-player mode, each blocking 2 lines.
    - Use them wisely to obstacle your opponents.
- You can only obstacle the way for your opponents, but you can never block their way.
- If you are moving and a single opponent is facing your way, you can jump over the opponent.
    - If the way you are jumping is blocked by a fence you can jump to the opponent's adjacent square if not blocked by other fences.

# Development notes
In progress:
- [Katya] game history
- [Pasha] minimax bot implementation
- [Pasha] known bug: in some cases you are allowed to place wall to block the path

TODO: [all done!]

Known bugs: [no known bugs!]
