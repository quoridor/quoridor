use std::collections::HashSet;
use rand::prelude::*;
use quoridor_core::player::{Player, PlayerId, PlayerAction};
use quoridor_core::board::{Board, GameMove, MoveDirection};

#[derive(Debug, Clone)]
pub struct AgentRandom {
    id: PlayerId,
    name: String,
}

impl AgentRandom {
    pub fn new(id: PlayerId, name: String) -> Self {
        Self {
            id, name,
        }
    }

    pub fn from_id(id: PlayerId) -> Self {
        Self {
            id, name: "RandomBot".to_owned(),
        }
    }
}

impl Player for AgentRandom {
    fn id(&self) -> &PlayerId {
        &self.id
    }

    fn name(&self) -> String {
        self.name.clone()
    }

    fn perform_move(&mut self, board: &Board) -> PlayerAction {
        let moves = board.get_possible_moves(&self.id);
        let _walls = board.remaining_walls().iter().fold(0, |a, (_, v)| a + v);
        let remaining_walls = board.remaining_walls().iter().find(|(id, _)| **id == self.id).unwrap().1;

        let mut pawn_moves = HashSet::new();
        let mut wall_moves = Vec::new();
        for m in moves.into_iter() {
            match m {
                GameMove::MovePawn(direction) => {
                    pawn_moves.insert(direction);
                },
                GameMove::PlaceWall(wall) => {
                    wall_moves.push(wall);
                },
            };
        }

        let player_position = board.pawns().iter()
            .filter(|pawn| pawn.player_id() != self.id())
            .choose(&mut rand::thread_rng())
            .expect("expected board to contain at least one enemy pawn")
            .position();
        wall_moves.sort_by(|a, b| a.position().distance_to(player_position)
            .cmp(&b.position().distance_to(player_position)));

        if *remaining_walls == 0 || wall_moves.is_empty() {
            // no more walls or it is impossible to place a wall. we MUST move pawn
            return PlayerAction::PerformMove(make_pawn_move(self.id.clone(), board, &pawn_moves));
        }
        let mut rnd = rand::thread_rng();
        return if rnd.gen::<u8>() > 80 {
            PlayerAction::PerformMove(make_pawn_move(self.id.clone(), board, &pawn_moves))
        } else {
            let wall_move = wall_moves[0..5.min(wall_moves.len())].choose(&mut rand::thread_rng())
                .expect("expected at least one wall move to be present");
            PlayerAction::PerformMove(GameMove::PlaceWall(*wall_move))
        }
    }
}

fn make_pawn_move(id: PlayerId, board: &Board, pawn_moves: &HashSet<MoveDirection>) -> GameMove {
    let pawn = board.pawns().iter().find(|p| p.player_id() == &id).unwrap();
    let my_direction = pawn.origin().opposite();

    for pawn_move in pawn_moves.iter() {
        if *pawn_move == my_direction.into() {
            return GameMove::MovePawn(*pawn_move);
        }
    }

    for pawn_move in pawn_moves.iter() {
        if pawn_move.is_45_deg(&my_direction.into()) {
            return GameMove::MovePawn(*pawn_move);
        }
    }

    let my_direction = my_direction.perpendicular();
    for pawn_move in pawn_moves.iter() {
        if pawn_move.is_parallel(&my_direction.into()) {
            return GameMove::MovePawn(*pawn_move);
        }
    }

    GameMove::MovePawn(*pawn_moves.iter().next().unwrap())
}
