use std::env;
use std::collections::{HashMap, HashSet};
use std::hash::Hasher;
use std::sync::{Arc, Mutex};
use std::thread::sleep;
use std::time::Instant;
use rand::prelude::*;
use quoridor_core::player::{Player, PlayerId, PlayerAction};
use quoridor_core::board::{Board, Direction, GameMove, GameMoveByPlayer, MoveDirection, WallDirection};
use quoridor_core::iteration_limiter::IterationLimiter;
use quoridor_core::reachability_checker::astar::check_move_with_players;

#[derive(Debug, Clone)]
pub struct MonteCarloBot {
    id: PlayerId,
    name: String,

    iteration_limiter: Box<dyn IterationLimiter>,

    boards: HashMap<u64, GameStateStats>,
}

#[derive(Debug, Clone)]
pub struct GameStateStats {
    total_wins: usize,
    total_games: usize,
}

impl MonteCarloBot {

    pub fn new(id: PlayerId, name: String, iteration_limiter: Box<dyn IterationLimiter>) -> Self {
        Self {
            id,
            name,
            iteration_limiter: iteration_limiter,
            boards: HashMap::new(),
        }
    }

    pub fn from_id(id: PlayerId, iteration_limiter: Box<dyn IterationLimiter>) -> Self {
        Self::new(id, "MonteCarloBot".to_owned(), iteration_limiter)
    }

    fn simulate_game(&mut self, mut board: Board) -> (GameMove, u64) {
        let mut first_move = None;
        let mut first_board_hash = None;

        let mut states = HashSet::new();

        while board.winner().is_none() {
            board.compute_shortest_routes();
            let game_move = self.random_move_for_board(&board);
            let this_player = board.next_move_by() == self.id();

            let new_board = board.apply_move_unchecked(GameMoveByPlayer::new(board.next_move_by().clone(), game_move.clone()));
            let new_board_hash = new_board.hash();
            if this_player {
                states.insert(new_board_hash);
            }
            board = new_board;

            if first_move.is_none() {
                first_move = Some(game_move.clone());
                first_board_hash = Some(new_board_hash);
            }
        }

        let result = if board.winner().unwrap() == self.id() {
            1
        } else {
            0
        };

        for state_hash in &states {
            if !self.boards.contains_key(state_hash) {
                self.boards.insert(*state_hash, GameStateStats {
                    total_games: 0,
                    total_wins: 0
                });
            }

            let st = self.boards.get_mut(state_hash).unwrap();
            st.total_games += 1;
            st.total_wins += result;
        }

        (first_move.unwrap(), first_board_hash.unwrap())
    }

    fn play_game_until_finish(&self, board: &Board, this_player: &PlayerId) -> bool {
        let mut board = board.clone();

        while board.winner().is_none() {
            board.compute_shortest_routes();
            let game_move = self.random_move_for_board(&board);
            board = board.apply_move_unchecked(GameMoveByPlayer::new(board.next_move_by().clone(), game_move));
        }

        board.winner().is_some() && board.winner().unwrap() == this_player
    }

    fn random_move_for_board(&self, board: &Board) -> GameMove {
        if board.remaining_walls().get(board.next_move_by()).unwrap_or(&0) > &0 && rand::thread_rng().gen_range(0..100) <= 16 {
            let mut board_moves = board.get_likely_possible_wall_moves();

            while board_moves.len() > 0 {
                let move_to_try = board_moves.remove(rand::thread_rng().gen_range(0..board_moves.len()));
                if board.is_likely_move_actually_possible(&move_to_try) {
                    return move_to_try;
                }
            }
        }

        let pawn_moves = board.get_possible_pawn_moves(board.next_move_by());
        if rand::thread_rng().gen_range(0..100) <= 80 {
            let shortest_route = board.shortest_route_for_player(board.next_move_by())
                .expect("Expected shortest route to be pre-computed");
            let direction = shortest_route.head_direction().unwrap();
            *pawn_moves.iter().find(|mv| mv.as_pawn_move().unwrap().get_deg() == direction.get_deg())
                .unwrap_or_else(|| pawn_moves.choose(&mut rand::thread_rng()).unwrap())
        } else {
            *pawn_moves.choose(&mut rand::thread_rng()).unwrap()
        }
    }

    pub fn moves_with_scores(&mut self, board: &Board) -> Vec<(GameMove, f32)> {
       self.iteration_limiter.reset();
        let mut board = board.clone();
        board.compute_shortest_routes();

        let mut result = Vec::new();

        let mut possible_moves: HashMap<GameMove, u64> = HashMap::new();
        while !self.iteration_limiter.has_reached_limit() {
            let (game_move, hash) = self.simulate_game(board.clone());
            possible_moves.insert(game_move, hash);
            self.iteration_limiter.on_iteration_complete();
        }

        for (game_move, hash) in &possible_moves {
            let state = self.boards.get(&hash).unwrap();
            let mut score = (state.total_wins as f32) / (state.total_games as f32);

            if let GameMove::PlaceWall(_) = game_move {
                score = score * 0.7;
            }

            result.push((game_move.clone(), score));
        }

        result
    }
}

impl Player for MonteCarloBot {
    fn id(&self) -> &PlayerId {
        &self.id
    }

    fn name(&self) -> String {
        self.name.clone()
    }

    fn perform_move(&mut self, board: &Board) -> PlayerAction {
        self.iteration_limiter.reset();
        let mut board = board.clone();
        board.compute_shortest_routes();

        let mut possible_moves: HashMap<GameMove, u64> = HashMap::new();
        while !self.iteration_limiter.has_reached_limit() {
            let (game_move, hash) = self.simulate_game(board.clone());
            possible_moves.insert(game_move, hash);
            self.iteration_limiter.on_iteration_complete();
        }

        let mut best_move = None;
        let mut best_move_score = None;

        for (game_move, hash) in &possible_moves {
            let state = self.boards.get(&hash).unwrap();
            let mut score = (state.total_wins as f64) / (state.total_games as f64);

            if let GameMove::PlaceWall(_) = game_move {
                score = score * 0.7;
            }

            if best_move_score.is_none() || score > best_move_score.unwrap() {
                best_move_score = Some(score);
                best_move = Some(game_move)
            }
        }

        PlayerAction::PerformMove(*best_move.unwrap())
    }
}
