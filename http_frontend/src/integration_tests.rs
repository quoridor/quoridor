#[cfg(test)]
mod tests {
    use std::env;
    use reqwest::Client;
    use quoridor_core::board::{GameMove, MoveDirection};
    use quoridor_core::player::PlayerId;
    use quonsensus_core::{ConsumeTaskResponse, CreateTaskRequest};
    use crate::{api::{CreateRoomRequest, CreateRoomResponse, GameStateResponse, JoinRoomRequest, JoinRoomResponse, MakeMoveRequest, PublicRoomMetadata, StartGameRequest}, players::PlayerOptions};

    #[tokio::test]
    pub async fn test_game() {
        if env::var("CI").is_ok() {
            println!("skipped");
            return;
        }

        let app_endpoint = env::var("APP_ENDPOINT").unwrap_or("http://localhost:8080/api/v1".to_owned());
        let client = Client::new();

        let create_room_response: CreateRoomResponse = client.post(format!("{}/games", app_endpoint))
            .json(&CreateRoomRequest {
                room_name: "Hello Quoridor".to_string(),
                is_public: true,
            })
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap();
        let game_id = create_room_response.id.clone();
        let join_token = create_room_response.join_token.clone();

        let public_rooms_response: Vec<PublicRoomMetadata> = client.get(format!("{}/games", app_endpoint))
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap();

        assert!(public_rooms_response.iter().find(|room| room.id == game_id).is_some());

        let join_player_a_response: JoinRoomResponse = client.post(format!("{}/games/{}/join", app_endpoint, game_id.to_string()))
            .json(&JoinRoomRequest {
                join_token: join_token.clone(),
                player: PlayerOptions::Human {
                    name: "Nikita".to_string(),
                    pawn_style: "blue.svg".to_owned(),
                }
            })
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap();
        let player_a_token = join_player_a_response.action_token.unwrap();

        let join_player_b_response: JoinRoomResponse = client.post(format!("{}/games/{}/join", app_endpoint, game_id.to_string()))
            .json(&JoinRoomRequest {
                join_token: join_token.clone(),
                player: PlayerOptions::Human {
                    name: "Pasha".to_string(),
                    pawn_style: "red.svg".to_owned(),
                }
            })
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap();
        let player_b_token = join_player_b_response.action_token.unwrap();

        let start_game_response: GameStateResponse = client.post(format!("{}/games/{}/start", app_endpoint, game_id.to_string()))
            .json(&StartGameRequest {
                action_token: player_a_token.clone(),
                first_move_by: None,
            })
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap();

        let mut state = start_game_response;
        for _ in 0..6 {
            let (action_token, direction) = if state.board.unwrap().next_move_by == PlayerId::new(0) {
                (player_a_token.clone(), MoveDirection::South)
            } else {
                (player_b_token.clone(), MoveDirection::North)
            };

            state = client.post(format!("{}/games/{}/moves", app_endpoint, game_id.to_string()))
                .json(&MakeMoveRequest {
                    action_token,
                    game_move: GameMove::MovePawn(direction)
                })
                .send()
                .await
                .unwrap()
                .json()
                .await
                .unwrap();

                assert_eq!(state.possible_moves.as_ref().unwrap().len(), 2);
                assert!(state.possible_moves.as_ref().unwrap().get(&PlayerId::new(0)).unwrap().len() != 0);
        }

        assert!(state.board.as_ref().unwrap().pawns[0].position().row() != 0 && state.board.as_ref().unwrap().pawns[0].position().row() != 8);
        assert!(state.board.as_ref().unwrap().pawns[0].position().row() != 0 && state.board.as_ref().unwrap().pawns[0].position().row() != 8);
    }
}