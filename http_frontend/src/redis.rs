use std::env;

pub const QUONSENSUS_STATE: &str = "quoridor::quonsensus::state";
pub const SELF_PLAY_DATASET_STATE: &str = "quoridor::self_play::state";

pub fn redis_connection_string() -> String {
    env::var("REDIS_CONNECTION_STRING")
        .unwrap_or_else(|_| "redis://redis.nikitavbv.com".to_owned())
}