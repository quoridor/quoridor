use std::sync::{Arc, Mutex};
use std::env;
use std::ops::Sub;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};
use actix::{Actor, Addr};
use actix_cors::Cors;
use env_logger::Env;
use log::{info, error};
use actix_web::{App, HttpRequest, HttpResponse, HttpServer, Responder, get, post, web};
use actix_web::web::PayloadConfig;
use quoridor_core::board::MoveError;
use thiserror::Error;
use quoridor_core::notation::fen_notation::FenNotationEncoder;
use crate::api::{JoinRoomRequest, JoinRoomResponse, MakeMoveRequest, StartGameRequest};
use crate::ws::GameRoomEventTracker;
use crate::game_state::redis::RedisGameStateStore;
use crate::game_state::events::{ChatMessageEvent, GameEvent, GameEventWithMetadata, GameFinishedEvent, GameMoveByPlayerEvent, GameStartedEvent, PlayerJoinedEvent};
use crate::game_state::store::{GameId, GameRoom, GameRoomError, GameStateStore};
use crate::game_state::in_memory::InMemoryGameStateStore;
use crate::api::{CreateRoomRequest, CreateRoomResponse, PublicRoomMetadata, game_to_state_response, Player, Board, PostChatMessageRequest};
use crate::ws::WsGameRoomSession;
use crate::intro::print_intro;
use crate::quonsensus::{quonsensus_state, consume_task, submit_checkpoint, reset_quonsensus_state, submit_self_play_outcome, save_self_play_data, get_self_play_data_state, get_self_play_data_stats, add_tournament_opponent, submit_tournament_outcome, reset_tournament, remove_tournament_opponent};

const GAME_STATE_STORE_IN_MEMORY: &str = "in_memory";
const GAME_STATE_STORE_REDIS: &str = "redis";

#[derive(Debug, Error, Clone)]
pub enum GameLoopError {
    #[error("Invalid move by one of the players: {0:?}")]
    InvalidMoveByOneOfThePlayers(MoveError),
}

pub async fn run() -> std::io::Result<()> {
    print_intro();

    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();
    info!("starting quoridor server");

    let game_state_store = Arc::new(Mutex::new(init_game_state_store().await));

    let mut game_room_event_tracker = GameRoomEventTracker::new(game_state_store.clone());
    game_room_event_tracker.start_event_tracking_thread().await;
    let game_room_event_tracker = web::Data::new(game_room_event_tracker.start());
    let game_state_store: web::Data<Arc<Mutex<Box<dyn GameStateStore + Send>>>> = web::Data::new(game_state_store);

    #[cfg(feature = "frontend")]
    {
        info!("Opening frontend in default browser...");
        if let Err(err) = webbrowser::open("http://localhost:8080") {
            error!("Failed to open frontend in default browser: {:?}", err);
        }
    }

    HttpServer::new(move || App::new()
        .wrap(Cors::default().allow_any_origin().allow_any_header().allow_any_method())
        .app_data(PayloadConfig::new(150 * 1024 * 1024))
        .app_data(game_room_event_tracker.clone())
        .app_data(game_state_store.clone())
        .default_service(web::route().to(default_service))
        .service(get_rooms)
        .service(create_room)
        .service(room_ws)
        .service(get_room_fen)
        .service(join_room)
        .service(start_game)
        .service(make_move)
        .service(post_chat_message)
        .service(generate_player_name)
        .service(generate_game_name)
        .service(quonsensus_state)
        .service(submit_checkpoint)
        .service(consume_task)
        .service(submit_self_play_outcome)
        .service(reset_quonsensus_state)
        .service(save_self_play_data)
        .service(get_self_play_data_state)
        .service(get_self_play_data_stats)
        .service(add_tournament_opponent)
        .service(submit_tournament_outcome)
        .service(reset_tournament)
        .service(remove_tournament_opponent)
    ).bind("0.0.0.0:8080")?
        .run()
        .await
}

#[cfg(feature = "frontend")]
async fn default_service(req: HttpRequest) -> impl Responder {
    use include_dir::{Dir, include_dir};
    static FRONTEND_DIR: Dir = include_dir!("../web-client/build");

    let path = req.uri().path();
    if let Some(file) = FRONTEND_DIR.get_file(&path[1..]) {
        return HttpResponse::Ok()
            .content_type(mime_guess::from_path(path).first().unwrap())
            .body(file.contents());
    }

    return HttpResponse::Ok()
        .content_type("text/html")
        .body(FRONTEND_DIR.get_file("index.html").unwrap().contents_utf8().unwrap());
}

#[cfg(not(feature = "frontend"))]
async fn default_service(req: HttpRequest) -> impl Responder {
    if req.uri().path() == "/" {
        HttpResponse::Ok().body("ok")
    } else {
        HttpResponse::NotFound().body("not found.")
    }
}

#[get("/api/v1/games")]
async fn get_rooms(game_state_store: web::Data<Arc<Mutex<Box<dyn GameStateStore + Send>>>>) -> impl Responder {
    let mut game_state_store = game_state_store.lock().unwrap();
    let game_ids = game_state_store.get_public_games_ids().await;
    let mut games = Vec::new();

    let game_expiration_time = SystemTime::now()
        .sub(Duration::from_secs(3600))
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();

    for game_id in game_ids {
        let game = match game_state_store.get_game_by_id(&game_id).await {
            Some(v) => v,
            None => continue,
        };

        if game.updated_at() < game_expiration_time {
            game_state_store.delete_game_by_id(&game_id).await;
            continue;
        }

        let can_join = !game.is_started();
        games.push(PublicRoomMetadata {
            id: game_id,
            name: game.name(),
            can_join,
            join_token: if can_join { Some(game.join_token()) } else { None },
        });
    }

    HttpResponse::Ok().json(games)
}

#[post("/api/v1/games")]
async fn create_room(game_state_store: web::Data<Arc<Mutex<Box<dyn GameStateStore + Send>>>>, req: web::Json<CreateRoomRequest>) -> impl Responder {
    let id = game_state_store.lock().unwrap().new_game(&req.room_name, req.is_public).await;
    let game = game_state_store.lock().unwrap().get_game_by_id(&id).await.unwrap();

    HttpResponse::Ok().json(CreateRoomResponse {
        id,
        join_token: game.join_token(),
    })
}

#[get("/api/v1/games/{id}/ws")]
async fn room_ws(tracker_addr: web::Data<Addr<GameRoomEventTracker>>, req: HttpRequest, stream: web::Payload, game_id: web::Path<GameId>) -> Result<HttpResponse, actix_web::Error> {
    actix_web_actors::ws::start(
        WsGameRoomSession {
            game_id: game_id.into_inner(),
            heartbeat: Instant::now(),
            tracker_addr: tracker_addr.get_ref().clone(),
        },
        &req,
        stream
    )
}

#[get("/api/v1/games/{id}/fen")]
async fn get_room_fen(game_state_store: web::Data<Arc<Mutex<Box<dyn GameStateStore + Send>>>>, game_id: web::Path<GameId>) -> impl Responder {
    game_state_store.lock().unwrap().get_game_by_id(&game_id).await.unwrap().board().unwrap().to_fen_notation()
}

#[post("/api/v1/games/{id}/join")]
async fn join_room(game_state_store: web::Data<Arc<Mutex<Box<dyn GameStateStore + Send>>>>, game_id: web::Path<GameId>, req: web::Json<JoinRoomRequest>) -> impl Responder {
    let mut game_state_store = game_state_store.lock().unwrap();

    let mut game = match game_state_store.get_game_by_id(&game_id).await {
        Some(v) => v,
        None => return HttpResponse::NotFound().body("room not found"),
    };

    if req.join_token != game.join_token() {
        return HttpResponse::Forbidden().body("access denied");
    }

    if game.board().is_some() {
        if game.winner().is_none() {
            return HttpResponse::BadRequest().body("cannot add new players once the game has started");
        }

        game.unset_board();
    }

    let (action_token, player, pawn_style) = game.join_player_using_options(&req.player);
    let player_id = player.id().clone();
    let player = Player::from_game_player(
        player,
        pawn_style,
    );
    let admin_player = game.admin_player_id().cloned();

    game_state_store.save_game_by_id(&game_id, game).await;
    game_state_store.publish_event(&GameEventWithMetadata {
        game_id: game_id.clone(),
        event: GameEvent::PlayerJoined(PlayerJoinedEvent {
            player,
            admin_player,
        })
    }).await;

    HttpResponse::Ok().json(JoinRoomResponse {
        player_id,
        action_token
    })
}

#[post("/api/v1/games/{id}/start")]
async fn start_game(game_state_store: web::Data<Arc<Mutex<Box<dyn GameStateStore + Send>>>>, game_id: web::Path<GameId>, req: web::Json<StartGameRequest>) -> impl Responder {
    let (mut game, player_id) = {
        let mut game_state_store = game_state_store.lock().unwrap();

        let game = match game_state_store.get_game_by_id(&game_id).await {
            Some(v) => v,
            None => return HttpResponse::NotFound().body("room not found"),
        };

        let player_id = match game.player_id_by_action_token(&req.action_token) {
            Some(v) => v,
            None => return HttpResponse::BadRequest().body("invalid action token"),
        }.clone();

        (game, player_id)
    };

    let admin_player_id = match game.admin_player_id() {
        Some(v) => v,
        None => return HttpResponse::BadRequest().body("there are no players yet"),
    };

    if &player_id != admin_player_id {
        return HttpResponse::Forbidden().body("access denied");
    }

    if let Err(err) = game.start(req.first_move_by.clone()) {
        return match err {
            GameRoomError::InvalidPlayersCount(_) => HttpResponse::BadRequest().body("invalid players count"),
        };
    }

    if let Err(err) = run_game_loop(&game_state_store, &game_id, &mut game).await {
        match err {
            GameLoopError::InvalidMoveByOneOfThePlayers(err) => return HttpResponse::BadRequest()
                .body(format!("invalid move by one of players: {:?}", err)),
        }
    }

    let mut game_state_store = game_state_store.lock().unwrap();
    let res = game_to_state_response(&game);
    let board = Board::from(game.board().unwrap());
    let possible_moves = game.possible_moves_by_player().unwrap();

    game_state_store.save_game_by_id(&game_id, game).await;
    game_state_store.publish_event(&GameEventWithMetadata {
        game_id: game_id.clone(),
        event: GameEvent::GameStarted(GameStartedEvent {
            board,
            possible_moves,
        })
    }).await;

    info!("started new game with id {}", game_id.to_string());

    HttpResponse::Ok().json(res)
}

#[post("/api/v1/games/{id}/moves")]
async fn make_move(game_state_store: web::Data<Arc<Mutex<Box<dyn GameStateStore + Send>>>>, game_id: web::Path<GameId>, req: web::Json<MakeMoveRequest>) -> impl Responder {
    let (mut game, player_id) = {
        let mut game_state_store = game_state_store.lock().unwrap();

        let game = match game_state_store.get_game_by_id(&game_id).await {
            Some(v) => v,
            None => return HttpResponse::NotFound().body("room not found"),
        };

        let player_id = match game.player_id_by_action_token(&req.action_token) {
            Some(v) => v.clone(),
            None => return HttpResponse::BadRequest().body("invalid action token"),
        };

        (game, player_id)
    };

    if game.winner().is_some() {
        return HttpResponse::BadRequest().body("game finished");
    }

    info!("setting move for player: {:?} while current move by is {:?}", player_id, game.board().unwrap().next_move_by());
    for player in game.players_mut() {
        if player.id() == &player_id && player.can_set_next_move() {
            player.set_next_move(req.game_move.clone());
            break;
        }
    }

    if let Err(err) = run_game_loop(&game_state_store, &game_id, &mut game).await {
        match err {
            GameLoopError::InvalidMoveByOneOfThePlayers(err) => return HttpResponse::BadRequest()
                .body(format!("invalid move by one of players: {:?}", err)),
        }
    }

    let res = game_to_state_response(&game);

    let mut game_state_store = game_state_store.lock().unwrap();
    game_state_store.save_game_by_id(&game_id, game).await;

    HttpResponse::Ok().json(res)
}

#[post("/api/v1/games/{id}/messages")]
async fn post_chat_message(game_state_store: web::Data<Arc<Mutex<Box<dyn GameStateStore + Send>>>>, game_id: web::Path<GameId>, req: web::Json<PostChatMessageRequest>) -> impl Responder {
    let mut game_state_store = game_state_store.lock().unwrap();

    let game = match game_state_store.get_game_by_id(&game_id).await {
        Some(v) => v,
        None => return HttpResponse::NotFound().body("room not found"),
    };

    let player_id = match game.player_id_by_action_token(&req.action_token) {
        Some(v) => v.clone(),
        None => return HttpResponse::BadRequest().body("invalid action token"),
    };

    game_state_store.publish_event(&GameEventWithMetadata {
        game_id: game_id.clone(),
        event: GameEvent::ChatMessage(ChatMessageEvent {
            author: player_id,
            message: req.message.clone(),
        })
    }).await;

    HttpResponse::Ok().body("ok")
}

#[get("/api/v1/names/player")]
async fn generate_player_name() -> impl Responder {
    let rng = rnglib::RNG::new(&rnglib::Language::Fantasy).unwrap();
    let first_name = rng.generate_name();
    let last_name = rng.generate_name();
    format!("{} {}", first_name, last_name)
}

#[get("/api/v1/names/game")]
async fn generate_game_name() -> impl Responder {
    names::Generator::default().next().unwrap()
}

async fn run_game_loop(game_state_store: &Arc<Mutex<Box<dyn GameStateStore + Send>>>, game_id: &GameId, game: &mut GameRoom) -> Result<(), GameLoopError> {
    loop {
        match game.act_with_player(&game.next_move_by().clone()) {
            Ok(v) => match v {
                Some(v) => {
                    game_state_store.lock().unwrap().publish_event(&GameEventWithMetadata {
                        game_id: game_id.clone(),
                        event: GameEvent::GameMoveByPlayer(GameMoveByPlayerEvent {
                            remaining_walls: *game.board().unwrap().remaining_walls().get(&v.player_id).unwrap(),
                            game_move: v,
                            next_player: game.next_move_by().clone(),
                            possible_moves_for_next_player: game.possible_moves(&game.next_move_by()).unwrap(),
                        })
                    }).await;

                    if let Some(winner) = game.winner() {
                        game_state_store.lock().unwrap().publish_event(&GameEventWithMetadata {
                            game_id: game_id.clone(),
                            event: GameEvent::GameFinished(GameFinishedEvent {
                                winner: winner.clone(),
                            })
                        }).await;
                        break;
                    }
                },
                None => break
            },
            Err(err) => {
                error!("failed to act with a player: {:?}", err);
                return Err(GameLoopError::InvalidMoveByOneOfThePlayers(err));
            }
        }
    }

    Ok(())
}

async fn init_game_state_store() -> Box<dyn GameStateStore + Send> {
    let game_state_store_name = env::var("GAME_STATE_STORE")
        .unwrap_or_else(|_| GAME_STATE_STORE_IN_MEMORY.to_owned());
    match game_state_store_name.as_str() {
        GAME_STATE_STORE_IN_MEMORY => {
            info!("Using in-memory game state store");
            Box::new(InMemoryGameStateStore::new())
        }
        GAME_STATE_STORE_REDIS => {
            info!("Using redis game state store");
            Box::new(RedisGameStateStore::new().await)
        },
        other => panic!("Unknown game state store: {}", other),
    }
}