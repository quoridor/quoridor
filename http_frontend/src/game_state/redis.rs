use std::{env, sync::{Arc, RwLock}, thread::{self, JoinHandle}, time::Duration};
use async_trait::async_trait;
use crossbeam::channel::{Receiver, Sender};
use redis::{AsyncCommands, FromRedisValue};
use log::error;
use uuid::Uuid;
use log::info;
use super::events::GameEventWithMetadata;
use super::store::{GameRoom, GameStateStore, GameId};
use crate::redis::redis_connection_string;

/*
Structure of keys in redis:
quoridor::events - pub/sub channel for all game events
quoridor::state::public_games - set of public game ids
quoridor::state::game::{id} - game state with {id}
*/

const GAME_EVENTS_CHANNEL: &str = "quoridor::events";

pub struct RedisGameStateStore {
    client: redis::Client,
    event_subscriptions: Arc<RwLock<Vec<Sender<GameEventWithMetadata>>>>,
    event_subscription_thread: Option<JoinHandle<()>>,
}

impl RedisGameStateStore {
    pub async fn new() -> Self {
        Self {
            client: redis::Client::open(redis_connection_string()).unwrap(),
            event_subscriptions: Arc::new(RwLock::new(Vec::new())),
            event_subscription_thread: None,
        }
    }

    pub fn run_pubsub_subscriber(event_subscriptions: Arc<RwLock<Vec<Sender<GameEventWithMetadata>>>>) {
        let client = redis::Client::open(redis_connection_string()).unwrap();
        let mut connection = client.get_connection().unwrap();
        let mut pubsub = connection.as_pubsub();
        pubsub.subscribe(GAME_EVENTS_CHANNEL).unwrap();

        loop {
            let msg = pubsub.get_message();
            let msg = match msg {
                Ok(v) => v,
                Err(err) => {
                    if err.is_connection_dropped() {
                        info!("redis connection dropped, reconnecting...");
                        thread::sleep(Duration::from_secs(1));
                        Self::run_pubsub_subscriber(event_subscriptions);
                        return;
                    } else if err.is_io_error() {
                        info!("pubsub redis io error: {:?}, reconnecting...", err);
                        thread::sleep(Duration::from_secs(1));
                        Self::run_pubsub_subscriber(event_subscriptions);
                        return;
                    } else {
                        info!("pubsub redis error: {:?}", err);
                        continue;
                    }
                }
            };
            
            let payload: String = msg.get_payload().unwrap();
            let event: GameEventWithMetadata = match serde_json::from_str(&payload) {
                Ok(v) => v,
                Err(err) => {
                    panic!("failed to decode game event from redis: {:?} for payload: {}", err, payload);
                }
            };
            for sub in event_subscriptions.read().unwrap().iter() {
                sub.send(event.clone()).unwrap();
            }
        }
    }
}

#[async_trait(?Send)]
impl GameStateStore for RedisGameStateStore {
    async fn new_game(&mut self, name: &str, is_public: bool) -> GameId {
        let new_game_id = GameId(Uuid::new_v4().to_string());
        self.save_game_by_id(&new_game_id, GameRoom::new(is_public, name)).await;
        new_game_id
    }

    async fn save_game_by_id(&mut self, id: &GameId, game: GameRoom) {
        let game_as_json = serde_json::to_string(&game).unwrap();
        let mut connection = self.client.get_async_connection().await.unwrap();

        connection.set::<String, String, ()>(
            redis_key_for_game(id),
            game_as_json
        ).await.unwrap();

        if game.is_public() {
            connection.sadd::<String, String, ()>(
                redis_key_for_public_games_set(),
                id.to_string()
            ).await.unwrap();
        }
    }
    
    async fn get_game_by_id(&mut self, id: &GameId) -> Option<GameRoom> {
        let mut connection = self.client.get_async_connection().await.unwrap();
        let redis_key = redis_key_for_game(id);
        let game_as_json = connection.get::<&str, Option<String>>(
            &redis_key
        ).await.unwrap()?;

        let mut game_room: GameRoom = match serde_json::from_str(&game_as_json) {
            Ok(v) => v,
            Err(err) => {
                error!("failed to deserialize game snapshot: {:?}", err);
                connection.del::<&str, ()>(&redis_key).await.unwrap();
                return None
            }
        };

        game_room.handle_post_deserialize();

        Some(game_room)
    }

    async fn delete_game_by_id(&mut self, id: &GameId) {
        let mut connection = self.client.get_async_connection().await.unwrap();
        let redis_key = redis_key_for_game(id);
        connection.del::<&str, ()>(&redis_key).await.unwrap();
        connection.srem::<String, String, ()>(
            redis_key_for_public_games_set(),
            id.to_string()
        ).await.unwrap();
    }

    async fn publish_event(&mut self, event: &GameEventWithMetadata) {
        let mut connection = self.client.get_async_connection().await.unwrap();
        connection.publish::<&str, String, ()>(GAME_EVENTS_CHANNEL, serde_json::to_string(event).unwrap()).await.unwrap();
    }

    async fn subscribe_to_events(&mut self) -> Receiver<GameEventWithMetadata> {
        info!("initializing new event subscription");
        let (sender, receiver): (Sender<GameEventWithMetadata>, Receiver<GameEventWithMetadata>) = crossbeam::channel::unbounded();
        self.event_subscriptions.write().unwrap().push(sender);

        if self.event_subscription_thread.is_none() {
            let event_subscriptions = self.event_subscriptions.clone();
            self.event_subscription_thread = Some(thread::spawn(move || Self::run_pubsub_subscriber(event_subscriptions)));
        }

        receiver
    }

    async fn get_public_games_ids(&self) -> Vec<GameId> {
        self.client.get_async_connection().await.unwrap().smembers::<String, Vec<GameId>>(
            redis_key_for_public_games_set()
        ).await.unwrap()
    }
}

impl FromRedisValue for GameId {
    fn from_redis_value(v: &redis::Value) -> redis::RedisResult<Self> {
        String::from_redis_value(v).map(Self)
    }
}

fn redis_key_for_public_games_set() -> String {
    format!("{}public_games", redis_key_prefix())
}

fn redis_key_for_game(id: &GameId) -> String {
    format!("{}game::{}", redis_key_prefix(), id.to_string())
}

fn redis_key_prefix() -> String {
    env::var("REDIS_KEY_PREFIX")
        .unwrap_or_else(|_| "quoridor::state::".to_owned())
}

#[cfg(test)]
mod tests {
    use crate::api::Board;
    use crate::game_state::events::{GameEvent, GameEventWithMetadata};

    #[test]
    fn deserialize_game_started_event_with_metadata() {
        let event_json = r#"{"game_id":"ef8e947b-200f-421a-9483-e7ec7cb58a6f","event":{"event":"GameStarted","possible_moves": {}, "board":{"next_move_by":"1","pawns":[{"player_id":"0","origin":"North","position":{"column":4,"row":0}},{"player_id":"1","origin":"South","position":{"column":4,"row":8}}],"walls":[],"remaining_walls":{"0":10,"1":10}}}}"#;
        serde_json::from_str::<GameEventWithMetadata>(event_json).unwrap();
    }

    #[test]
    fn deserialize_game_started_event() {
        let event_json = r#"{"event":"GameStarted","possible_moves": {}, "board":{"next_move_by":"1","pawns":[{"player_id":"0","origin":"North","position":{"column":4,"row":0}},{"player_id":"1","origin":"South","position":{"column":4,"row":8}}],"walls":[],"remaining_walls":{"0":10,"1":10}}}"#;
        serde_json::from_str::<GameEvent>(event_json).unwrap();
    }

    #[test]
    fn deserialize_board() {
        let event_json = r#"{"next_move_by":"1","pawns":[{"player_id":"0","origin":"North","position":{"column":4,"row":0}},{"player_id":"1","origin":"South","position":{"column":4,"row":8}}],"walls":[],"remaining_walls":{"0":10,"1":10}}"#;
        serde_json::from_str::<Board>(event_json).unwrap();
    }
}