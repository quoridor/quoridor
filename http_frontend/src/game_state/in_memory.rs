use std::collections::HashMap;
use async_trait::async_trait;
use crossbeam::channel::{Receiver, Sender};
use uuid::Uuid;
use crate::game_state::store::{GameRoom, GameStateStore, GameId};
use super::events::GameEventWithMetadata;

pub struct InMemoryGameStateStore {
    games: HashMap<GameId, GameRoom>,
    event_subscriptions: Vec<Sender<GameEventWithMetadata>>,
}

impl InMemoryGameStateStore {
    pub fn new() -> Self {
        Self {
            games: HashMap::new(),
            event_subscriptions: Vec::new(),
        }
    }
}

#[async_trait(?Send)]
impl GameStateStore for InMemoryGameStateStore {
    async fn new_game(&mut self, name: &str, is_public: bool) -> GameId {
        let new_game_id = GameId(Uuid::new_v4().to_string());
        self.save_game_by_id(&new_game_id, GameRoom::new(is_public, name)).await;
        new_game_id
    }

    async fn get_game_by_id(&mut self, id: &GameId) -> Option<GameRoom> {
        self.games.get(id).cloned()
    }

    async fn delete_game_by_id(&mut self, id: &GameId) {
        self.games.remove(id);
    }

    async fn save_game_by_id(&mut self, id: &GameId, game: GameRoom) {
        self.games.insert(id.clone(), game);
    }

    async fn publish_event(&mut self, event: &GameEventWithMetadata) {
        for subscription in &self.event_subscriptions {
            subscription.send(event.clone()).unwrap();
        }
    }

    async fn subscribe_to_events(&mut self) -> Receiver<GameEventWithMetadata> {
        let (sender, receiver): (Sender<GameEventWithMetadata>, Receiver<GameEventWithMetadata>) = crossbeam::channel::unbounded();
        self.event_subscriptions.push(sender);
        receiver
    }

    async fn get_public_games_ids(&self) -> Vec<GameId> {
        self.games.iter()
            .filter(|(_, room)| room.is_public())
            .map(|(game_id, _)| game_id)
            .cloned()
            .collect()
    }
}