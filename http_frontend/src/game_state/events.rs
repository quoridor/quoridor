use std::collections::{HashMap, HashSet};

use quoridor_core::{board::{GameMove, GameMoveByPlayer}, player::PlayerId};
use serde::{Serialize, Deserialize};
use crate::api::{Board, Player};

use super::store::GameId;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GameEventWithMetadata {
    pub game_id: GameId,
    pub event: GameEvent,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "event")]
pub enum GameEvent {
    PlayerJoined(PlayerJoinedEvent),
    GameStarted(GameStartedEvent),
    GameMoveByPlayer(GameMoveByPlayerEvent),
    GameFinished(GameFinishedEvent),
    ChatMessage(ChatMessageEvent),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PlayerJoinedEvent {
    pub player: Player,
    pub admin_player: Option<PlayerId>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GameStartedEvent {
    pub board: Board,
    pub possible_moves: HashMap<PlayerId, HashSet<GameMove>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GameMoveByPlayerEvent {
    pub game_move: GameMoveByPlayer,
    pub remaining_walls: u8,

    pub next_player: PlayerId,
    pub possible_moves_for_next_player: HashSet<GameMove>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GameFinishedEvent {
    pub winner: PlayerId, // no draws
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChatMessageEvent {
    pub author: PlayerId,
    pub message: String,
}