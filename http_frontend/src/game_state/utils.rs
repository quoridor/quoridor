use rand::{Rng, distributions::Alphanumeric};
use serde::{Serialize, Deserialize};

pub type JoinToken = String;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct ActionToken(String);

pub fn generate_join_token() -> JoinToken {
    generate_token(32)
}

pub fn generate_action_token() -> ActionToken {
    ActionToken(generate_token(32))
}

fn generate_token(length: usize) -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length)
        .map(char::from)
        .collect()
}