use std::collections::{HashMap, HashSet};
use std::time::{SystemTime, UNIX_EPOCH};
use crossbeam::channel::Receiver;
use rand::prelude::SliceRandom;
use thiserror::Error;
use quoridor_core::{board::{Board, GameMove, GameMoveByPlayer, MoveError}, player::{Player, PlayerAction, PlayerId}};
use async_trait::async_trait;
use serde::{Serialize, Deserialize};
use crate::players::PlayerOptions;
use crate::game_state::utils::{ActionToken, JoinToken, generate_action_token, generate_join_token};
use super::events::GameEventWithMetadata;

const CLASSIC_PAWNS: [&str; 5] = ["blue.svg", "red.svg", "green.svg", "black.svg", "orange.svg"];

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct GameId(pub String);

#[async_trait(?Send)]
pub trait GameStateStore {
    async fn new_game(&mut self, name: &str, is_public: bool) -> GameId;
    async fn get_game_by_id(&mut self, id: &GameId) -> Option<GameRoom>;
    async fn save_game_by_id(&mut self, id: &GameId, game: GameRoom);
    async fn delete_game_by_id(&mut self, id: &GameId);

    async fn publish_event(&mut self, event: &GameEventWithMetadata);
    async fn subscribe_to_events(&mut self) -> Receiver<GameEventWithMetadata>;

    async fn get_public_games_ids(&self) -> Vec<GameId>;
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GameRoom {
    board: Option<Board>,
    is_public: bool,
    name: String,
    updated_at: u64,

    #[serde(skip)]
    players: Vec<Box<dyn Player + Send>>,
    admin_player: Option<PlayerId>,
    player_options: Vec<PlayerOptions>, // basically player config for persistence

    join_token: String,
    action_tokens: HashMap<ActionToken, PlayerId>,
    pawn_styles: HashMap<PlayerId, String>,
}

#[derive(Error, Debug)]
pub enum GameRoomError {
    #[error("There should be either two or four players in the room to start the game, instead got: {0}")]
    InvalidPlayersCount(usize),
}

impl ToString for GameId {
    fn to_string(&self) -> String {
        self.0.clone()
    }
}

impl GameRoom {
    pub fn new(is_public: bool, name: &str) -> Self {
        Self {
            board: None,
            players: Vec::new(),
            admin_player: None,
            player_options: Vec::new(),
            is_public,
            name: name.to_owned(),
            updated_at: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs(),
            join_token: generate_join_token(),
            action_tokens: HashMap::new(),
            pawn_styles: HashMap::new(),
        }
    }

    pub fn handle_post_deserialize(&mut self) {
        for player in self.player_options.clone() {
            self.join_player(player.instantiate(self.next_player_id()));
        }
    }

    pub fn join_player_using_options(&mut self, player_options: &PlayerOptions) -> (Option<ActionToken>, &Box<dyn Player + Send>, String) {
        self.player_options.push(player_options.clone());
        let player = player_options.instantiate(self.next_player_id());
        let player_id = player.id().clone();
        let pawn_style = select_pawn_style(player_options);
        self.pawn_styles.insert(player_id.clone(), pawn_style.clone());
        (self.join_player(player), self.player_by_id(&player_id).unwrap(), pawn_style)
    }

    pub fn join_player(&mut self, player: Box<dyn Player + Send>) -> Option<ActionToken> {
        let action_token = if player.can_set_next_move() {
            let action_token = self.action_tokens.iter()
                .find(|(_, v)| &v == &&player.id())
                .map(|(k, _)| k);

            Some(action_token.cloned().unwrap_or_else(|| {
                let action_token = generate_action_token();
                self.action_tokens.insert(action_token.clone(), player.id().clone());
                action_token
            }))
        } else {
            None
        };

        if self.admin_player.is_none() && player.can_be_game_admin() {
            self.admin_player = Some(player.id().clone());
        }

        self.players.push(player);

        action_token
    }

    pub fn join_token(&self) -> JoinToken {
        self.join_token.to_owned()
    }

    pub fn players(&self) -> &Vec<Box<dyn Player + Send>> {
        &self.players
    }

    pub fn players_mut(&mut self) -> &mut Vec<Box<dyn Player + Send>> {
        &mut self.players
    }

    pub fn player_by_id(&self, id: &PlayerId) -> Option<&Box<dyn Player + Send>> {
        self.players().iter().find(|v| &v.id() == &id)
    }

    pub fn board(&self) -> Option<&Board> {
        self.board.as_ref()
    }

    pub fn unset_board(&mut self) {
        self.board = None;
    }

    pub fn player_id_by_action_token(&self, action_token: &ActionToken) -> Option<&PlayerId> {
        self.action_tokens.get(action_token)
    }

    pub fn admin_player_id(&self) -> Option<&PlayerId> {
        self.admin_player.as_ref()
    }

    pub fn choose_random_player(&self) -> Option<&PlayerId> {
        self.players.choose(&mut rand::thread_rng()).map(|v| v.id())
    }

    pub fn next_player_id(&self) -> PlayerId {
        self.players.iter().map(|v| v.id())
            .max()
            .map(|v| v.generate_next())
            .unwrap_or(PlayerId::new(0))
    }

    pub fn start(&mut self, first_move_by: Option<PlayerId>) -> Result<(), GameRoomError> {
        self.board = Some(match self.players.len() {
            2 => Board::new_for_two_players(
                self.players[0].id().clone(),
                self.players[1].id().clone(),
                first_move_by.unwrap_or_else(|| self.choose_random_player().unwrap().clone())
            ),
            4 => Board::new_for_four_players(
                self.players[0].id().clone(),
                self.players[1].id().clone(),
                self.players[2].id().clone(),
                self.players[3].id().clone(),
                first_move_by.unwrap_or_else(|| self.choose_random_player().unwrap().clone())
            ),
            other => return Err(GameRoomError::InvalidPlayersCount(other))
        });

        Ok(())
    }

    pub fn next_move_by(&self) -> &PlayerId {
        self.board.as_ref().unwrap().next_move_by()
    }

    pub fn act_with_player(&mut self, player_id: &PlayerId) -> Result<Option<GameMoveByPlayer>, MoveError> {
        let player = self.players.iter_mut().find(|v| v.id() == player_id).unwrap();
        match player.perform_move(self.board.as_ref().unwrap()) {
            PlayerAction::PerformMove(game_move) => {
                let game_move = GameMoveByPlayer {
                    player_id: player_id.clone(),
                    game_move,
                };
                self.perform_move(game_move.clone())?;
                Ok(Some(game_move))
            },
            PlayerAction::Wait => Ok(None),
            PlayerAction::Error(err) => panic!("Bot error: {:?}", err),
        }
    }

    pub fn perform_move(&mut self, game_move: GameMoveByPlayer) -> Result<(), MoveError> {
        self.board = Some(self.board.as_ref().unwrap().apply_move(game_move)?);
        Ok(())
    }

    pub fn name(&self) -> String {
        self.name.clone()
    }

    pub fn is_public(&self) -> bool {
        self.is_public
    }

    pub fn possible_moves(&self, player_id: &PlayerId) -> Option<HashSet<GameMove>> {
        self.board.as_ref().map(|board| board.get_possible_moves(player_id))
    }

    pub fn possible_moves_by_player(&self) -> Option<HashMap<PlayerId, HashSet<GameMove>>> {
        if self.board.is_none() {
            return None;
        }

        let mut possible_moves = HashMap::new();
        for player in &self.players {
            possible_moves.insert(player.id().clone(), self.possible_moves(&player.id()).unwrap());
        }

        Some(possible_moves)
    }

    pub fn winner(&self) -> Option<&PlayerId> {
        self.board.as_ref()?.winner()
    }

    pub fn is_started(&self) -> bool {
        self.board().is_some()
    }

    pub fn pawn_style_for_player(&self, player_id: &PlayerId) -> Option<String> {
        self.pawn_styles.get(player_id).cloned()
    }

    pub fn updated_at(&self) -> u64 {
        self.updated_at
    }
}

fn select_pawn_style(options: &PlayerOptions) -> String {
    match options {
        PlayerOptions::Human {
            name: _name,
            pawn_style
        } => pawn_style.to_owned(),
        _ => CLASSIC_PAWNS.choose(&mut rand::thread_rng()).unwrap().to_owned().to_owned()
    }
}