use std::time::Duration;
use agent_minimax::AgentMinimax;
use agent_random::AgentRandom;
use quoridor_core::iteration_limiter::IterationTimeLimiter;
use quoridor_core::player::{InteractivePlayer, Player, PlayerId};
use serde::{Serialize, Deserialize};
use agent_monte_carlo::MonteCarloBot;
use quortex::agent::QuortexBot;
use quortex::nets::t2::QuortexNetT2;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag="type")]
pub enum PlayerOptions {
    Human {
        name: String,
        pawn_style: String,
    },
    RandomBot,
    MinimaxBot,
    MonteCarloBot,
    Quortex,
}

impl PlayerOptions {
    pub fn instantiate(&self, id: PlayerId) -> Box<dyn Player + Send> {
        match &self {
            Self::Human {
                name,
                pawn_style: _pawn_style
            } => Box::new(InteractivePlayer::new(id, name)),
            Self::RandomBot => Box::new(AgentRandom::from_id(id)),
            Self::MinimaxBot => Box::new(AgentMinimax::from_id(id)),
            Self::MonteCarloBot => Box::new(MonteCarloBot::from_id(id, Box::new(IterationTimeLimiter::new(Duration::from_secs(5))))),
            Self::Quortex => Box::new(QuortexBot::new_with_net(id, Box::new(IterationTimeLimiter::new(Duration::from_secs(5))), Box::new(QuortexNetT2::best()), false)),
        }
    }
}