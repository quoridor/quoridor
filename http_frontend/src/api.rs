use std::collections::{HashMap, HashSet};
use quoridor_core::{board::GameMove, player::PlayerId};
use serde::{Serialize, Deserialize};
use crate::game_state::store::{GameId as GameStateGameId, GameRoom};
use crate::{game_state::utils::ActionToken as GameStateActionToken, players::PlayerOptions};

pub type ActionToken = GameStateActionToken;
pub type GameId = GameStateGameId;

// Room metadata
#[derive(Debug, Serialize, Deserialize)]
pub struct PublicRoomMetadata {
    pub id: GameId,
    pub name: String,
    pub can_join: bool,
    pub join_token: Option<String>,
}

// Room creation
#[derive(Serialize, Deserialize)]
pub struct CreateRoomRequest {
    pub room_name: String,
    pub is_public: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateRoomResponse {
    pub id: GameId,
    pub join_token: String,
}

// Joining rooms and inviting bots
#[derive(Serialize, Deserialize)]
pub struct JoinRoomRequest {
    pub join_token: String,
    pub player: PlayerOptions,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JoinRoomResponse {
    pub player_id: PlayerId,
    pub action_token: Option<ActionToken>,
}

// start game
#[derive(Debug, Serialize, Deserialize)]
pub struct StartGameRequest {
    pub action_token: ActionToken,
    pub first_move_by: Option<PlayerId>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GameStateResponse {
    pub name: String,
    pub is_public: bool,
    pub admin_player: Option<PlayerId>,
    pub players: Vec<Player>,
    pub winner: Option<PlayerId>,
    pub board: Option<Board>,
    pub possible_moves: Option<HashMap<PlayerId, HashSet<GameMove>>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Player {
    id: PlayerId,
    name: String,
    pawn_style: String,
}

impl Player {

    pub fn from_game_player(player: &Box<dyn quoridor_core::player::Player + Send>, pawn_style: String) -> Self {
        Self {
            id: player.id().clone(),
            name: player.name(),
            pawn_style,
        }
    }
}

// subset of the type from the core trait
// we have this type so we don't accidentally leak something to the client
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Board {
    pub next_move_by: PlayerId,
    pub pawns: Vec<quoridor_core::board::Pawn>,
    pub walls: Vec<quoridor_core::board::Wall>,
    pub remaining_walls: HashMap<PlayerId, u8>,
}

impl From<&quoridor_core::board::Board> for Board {
    fn from(board: &quoridor_core::board::Board) -> Self {
        Self {
            next_move_by: board.next_move_by().clone(),
            pawns: board.pawns().clone(),
            walls: board.walls().clone(),
            remaining_walls: board.remaining_walls().clone(),
        }
    }
}

// make move
#[derive(Serialize, Deserialize)]
pub struct MakeMoveRequest {
    pub action_token: ActionToken,
    pub game_move: GameMove,
}

#[derive(Serialize, Deserialize)]
pub struct PostChatMessageRequest {
    pub action_token: ActionToken,
    pub message: String,
}

pub fn game_to_state_response(game: &GameRoom) -> GameStateResponse {
    GameStateResponse {
        name: game.name(),
        is_public: game.is_public(),
        admin_player: game.admin_player_id().cloned(),
        players: game.players().iter()
            .map(|v| Player::from_game_player(
                v,
                game.pawn_style_for_player(v.id()).unwrap()
            ))
            .collect(),
        winner: game.winner().cloned(),
        board: game.board().map(|v| Board::from(v)),
        possible_moves: game.possible_moves_by_player(),
    }
}