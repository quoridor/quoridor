mod api;
mod app;
mod game_state;
mod integration_tests;
mod intro;
mod players;
mod quonsensus;
mod redis;
mod ws;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    app::run().await
}
