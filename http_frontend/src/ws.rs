use std::collections::HashMap;
use std::sync::{Arc, Mutex, RwLock};
use std::thread;
use std::time::{Duration, Instant};
use actix::prelude::*;
use log::{error, info, warn};
use serde::{Serialize, Deserialize};
use crate::api::{GameStateResponse, game_to_state_response};
use crate::game_state::events::GameEvent;
use crate::game_state::store::{GameId, GameRoom, GameStateStore};

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

pub struct GameRoomEventTracker {
    game_state_store: Arc<Mutex<Box<dyn GameStateStore + Send>>>,
    game_subscribers: Arc<RwLock<HashMap<GameId, Vec<Addr<WsGameRoomSession>>>>>,
    event_tracker_thread: Option<thread::JoinHandle<()>>,
}

pub struct WsGameRoomSession {
    pub game_id: GameId,
    pub heartbeat: Instant,
    pub tracker_addr: Addr<GameRoomEventTracker>,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct SubscribeToGame {
    pub game_id: GameId,
    pub addr: Addr<WsGameRoomSession>,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct GameRoomStateMessage {
    game: GameRoom,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct GameEventMessage {
    event: GameEvent,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum WebsocketMessage {
    GameStateUpdated(GameStateResponse),
    GameEvent(GameEvent),
}

impl Actor for GameRoomEventTracker {
    type Context = Context<Self>;
}

impl GameRoomEventTracker {

    pub fn new(game_state_store: Arc<Mutex<Box<dyn GameStateStore + Send>>>) -> Self {
        Self {
            game_state_store,
            game_subscribers: Arc::new(RwLock::new(HashMap::new())),
            event_tracker_thread: None,
        }
    }

    pub async fn start_event_tracking_thread(&mut self) {
        let receiver = self.game_state_store.lock().unwrap().subscribe_to_events().await;
        let subscribers = self.game_subscribers.clone();

        self.event_tracker_thread = Some(thread::spawn(move || {
            info!("event tracker thread started");
            
            loop {
                let msg = match receiver.recv() {
                    Ok(v) => v,
                    Err(err) => {
                        error!("failed to read from events receiver: {:?}", err);
                        return;
                    }
                };


                for (game_id, subs) in subscribers.read().unwrap().iter() {
                    if game_id != &msg.game_id {
                        continue;
                    }

                    for sub in subs {
                        sub.do_send(GameEventMessage {
                            event: msg.event.clone(),
                        });
                    }
                }
            }
        }));
    }
}

impl Handler<SubscribeToGame> for GameRoomEventTracker {
    type Result = ();

    fn handle(&mut self, msg: SubscribeToGame, ctx: &mut Self::Context) {
        let mut game_subscribers = self.game_subscribers.write().unwrap();

        if !game_subscribers.contains_key(&msg.game_id) {
            game_subscribers.insert(msg.game_id.clone(), Vec::new());
        }
        game_subscribers.get_mut(&msg.game_id).unwrap().push(msg.addr.clone());

        let game_state_store = self.game_state_store.clone();
        async move {
            let game = match game_state_store.lock().expect("failed to lock game state store").get_game_by_id(&msg.game_id).await {
                Some(v) => v,
                None => {
                    warn!("cannot subscribe to game with id: {:?} - game not found.", msg.game_id);
                    return;
                }
            };
            msg.addr.do_send(GameRoomStateMessage {
                game,
            });
        }.into_actor(self).wait(ctx)
    }
}

impl Actor for WsGameRoomSession {
    type Context = actix_web_actors::ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.heartbeat(ctx);

        let addr = ctx.address();
        self.tracker_addr
            .send(SubscribeToGame {
                game_id: self.game_id.clone(),
                addr: addr.clone(),
            })
            .into_actor(self)
            .then(|res, _act, ctx| {
                if res.is_err() {
                    ctx.stop();
                }
                fut::ready(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _ctx: &mut Self::Context) -> Running {
        Running::Stop
    }
}

impl Handler<GameRoomStateMessage> for WsGameRoomSession {
    type Result = ();

    fn handle(&mut self, msg: GameRoomStateMessage, ctx: &mut Self::Context) -> Self::Result {
        ctx.text(serde_json::to_string(&WebsocketMessage::GameStateUpdated(game_to_state_response(&msg.game))).unwrap());
    }
}

impl Handler<GameEventMessage> for WsGameRoomSession {
    type Result = ();

    fn handle(&mut self, msg: GameEventMessage, ctx: &mut Self::Context) -> Self::Result {
        ctx.text(serde_json::to_string(&WebsocketMessage::GameEvent(msg.event)).unwrap())
    }
}

impl StreamHandler<Result<actix_web_actors::ws::Message, actix_web_actors::ws::ProtocolError>> for WsGameRoomSession {
    fn handle(&mut self, msg: Result<actix_web_actors::ws::Message, actix_web_actors::ws::ProtocolError>, ctx: &mut Self::Context) {
        let msg = match msg {
            Err(_) => {
                ctx.stop();
                return;
            },
            Ok(msg) => msg,
        };

        match msg {
            actix_web_actors::ws::Message::Ping(msg) => {
                self.heartbeat = Instant::now();
                ctx.pong(&msg);
            },
            actix_web_actors::ws::Message::Pong(_) => {
                self.heartbeat = Instant::now();
            },
            actix_web_actors::ws::Message::Text(text) => {
                info!("got text message: {:?}", text);
            },
            actix_web_actors::ws::Message::Binary(_) => {
                error!("unexpected binary from websocket");
            },
            actix_web_actors::ws::Message::Close(reason) => {
                ctx.close(reason);
                ctx.stop();
            },
            actix_web_actors::ws::Message::Continuation(_) => {
                ctx.stop();
            },
            actix_web_actors::ws::Message::Nop => (),
        }
    }
}

impl WsGameRoomSession {

    fn heartbeat(&self, ctx: &mut actix_web_actors::ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.heartbeat) > CLIENT_TIMEOUT {
                info!("websocket client heartbeat timeout, disconnecting");

                ctx.stop();

                return;
            }

            ctx.ping(b"");
        });
    }
}
