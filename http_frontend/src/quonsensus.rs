use std::collections::HashMap;
use actix_web::{HttpRequest, HttpResponse, post, get, Responder, web::Json, web};
use actix_web::web::Bytes;
use redis::AsyncCommands;
use serde::{Serialize, Deserialize};
use s3::{Bucket, Region, creds::Credentials};
use rpds::Vector;
use log::info;
use quonsensus_core::{ConsumeTaskResponse, Task, SubmitCheckpointRequest, CheckpointMetadata, SubmitSelfPlayOutcomeRequest, SubmitTournamentOutcomeRequest};
use crate::redis::{redis_connection_string, QUONSENSUS_STATE, SELF_PLAY_DATASET_STATE};

#[derive(Serialize, Deserialize, Clone)]
struct QuonsensusState {
    best_checkpoint: Option<CheckpointV2>,
    checkpoints: Vector<Checkpoint>,
    history: Vector<CheckpointV2>,

    self_play_entries_by_era: Option<HashMap<String, u32>>,
    current_era: Option<u32>,
    current_era_checkpoint: Option<CheckpointV2>,

    tournament_stats: Option<Vec<TournamentStats>>,
}

#[derive(Serialize, Deserialize, Clone)]
struct SelfPlayDatasetState {
    entries: Vec<SelfPlayDatasetEntry>,
}

#[derive(Serialize, Deserialize, Clone)]
struct SelfPlayDatasetEntry {
    era: u32,
    asset_id: String,
    total_games: u32,
}

#[derive(Serialize, Clone)]
struct SelfPlayDatasetStats {
    era_stats: Vec<SelfPlayEraStats>,
}

#[derive(Serialize, Clone)]
struct SelfPlayEraStats {
    era: u32,
    total_games: u32,
    total_entries: u32,
}

#[derive(Serialize, Deserialize, Clone)]
struct Checkpoint {
    net_name: String,
    run_id: String,
    epoch: u32,
    value_loss: f32,
    policy_loss: f32,
    checkpoint_asset_id: String,

    total_games: u32,
    total_wins: u32,
}

#[derive(Serialize, Deserialize, Clone, Eq, PartialEq)]
struct CheckpointV2 {
    net_name: String,
    run_id: String,
    epoch: u32,
    checkpoint_asset_id: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct TournamentStats {
    opponent: String,
    total_games: u32,
    games_in_progress: u32,
    total_wins: u32,
    errors: u32,
}

#[derive(Deserialize)]
struct AddTournamentOpponentRequest {
    opponent: String,
}

#[derive(Deserialize)]
struct RemoveOpponentRequest {
    opponent: String,
}

impl Default for QuonsensusState {
    fn default() -> Self {
        Self {
            best_checkpoint: None,
            checkpoints: Vector::new(),
            history: Vector::new(),

            self_play_entries_by_era: Some(HashMap::new()),
            current_era: None,
            current_era_checkpoint: None,

            tournament_stats: Some(Vec::new()),
        }
    }
}

impl QuonsensusState {

    pub fn add_checkpoint(self, checkpoint: Checkpoint) -> Self {
        Self {
            checkpoints: self.checkpoints.push_back(checkpoint),
            ..self
        }
    }
}

impl Default for SelfPlayDatasetState {
    fn default() -> Self {
        Self {
            entries: Vec::new(),
        }
    }
}

#[get("/api/v1/quonsensus")]
pub async fn quonsensus_state() -> impl Responder {
    HttpResponse::Ok().json(&load_quonsensus_state().await)
}

#[post("/api/v1/quonsensus/checkpoints")]
pub async fn submit_checkpoint(req: Json<SubmitCheckpointRequest>) -> impl Responder {
    let state = load_quonsensus_state().await;
    let state = state.add_checkpoint(Checkpoint {
        net_name: req.net_name.clone(),
        run_id: req.run_id.to_owned(),
        epoch: req.epoch,
        value_loss: req.value_loss,
        policy_loss: req.policy_loss,
        checkpoint_asset_id: req.checkpoint_asset_id.to_owned(),

        total_games: 0,
        total_wins: 0,
    });
    save_quonsensus_state(state).await;

    HttpResponse::Ok().body("ok")
}

#[post("/api/v1/quonsensus/tasks/consume")]
pub async fn consume_task(req: HttpRequest) -> impl Responder {
    let quonsensus_client_version = req.headers().get("x-quonsensus-version").map(|v| v.to_str().unwrap().parse().unwrap()).unwrap_or(0);
    if quonsensus_client_version < 3 {
        return HttpResponse::Ok().json(&ConsumeTaskResponse::NoTasksAvailable);
    }

    let is_windows = req.headers().get("x-quonsensus-platform").map(|v| v.to_str().unwrap().to_owned().to_owned()).unwrap_or("linux".to_string()) == "windows";

    let state = load_quonsensus_state().await;

    if is_windows {
        if let Some(tournament_stats) = &state.tournament_stats {
            let opponent = tournament_stats.iter()
                .reduce(|a, b| if a.total_games + a.games_in_progress < b.total_games + b.games_in_progress {
                    a
                } else {
                    b
                })
                .map(|v| v.opponent.clone());

            if let Some(opponent) = opponent {
                let state = QuonsensusState {
                    tournament_stats: Some(tournament_stats
                        .iter()
                        .map(|v| if v.opponent == opponent {
                            TournamentStats {
                                games_in_progress: v.games_in_progress + 1,
                                ..v.clone()
                            }
                        } else {
                            v.clone()
                        })
                        .collect()),
                    ..state
                };

                save_quonsensus_state(state).await;

                return HttpResponse::Ok().json(&ConsumeTaskResponse::Ok {
                    task: Task::Tournament {
                        opponent,
                    }
                })
            }
        }
    }

    if state.checkpoints.len() > 0 {
        let state = if state.best_checkpoint.is_none() {
            let best_checkpoint = CheckpointV2 {
                net_name: state.checkpoints[0].net_name.clone(),
                run_id: state.checkpoints[0].run_id.clone(),
                epoch: state.checkpoints[0].epoch,
                checkpoint_asset_id: state.checkpoints[0].checkpoint_asset_id.clone(),
            };
            let state = QuonsensusState {
                best_checkpoint: Some(best_checkpoint),
                checkpoints: state.checkpoints.iter().skip(1).cloned().collect(),
                ..state
            };
            save_quonsensus_state(state.clone()).await;
            state
        } else {
            state
        };

        if state.checkpoints.len() == 0 {
            return HttpResponse::Ok().json(&ConsumeTaskResponse::NoTasksAvailable)
        }

        let baseline = CheckpointMetadata {
            net_name: state.best_checkpoint.as_ref().unwrap().net_name.clone(),
            run_id: state.best_checkpoint.as_ref().unwrap().run_id.clone(),
            epoch: state.best_checkpoint.as_ref().unwrap().epoch,
            checkpoint_asset_id: state.best_checkpoint.as_ref().unwrap().checkpoint_asset_id.clone(),
        };

        let opponent = CheckpointMetadata {
            net_name: state.checkpoints[0].net_name.clone(),
            run_id: state.checkpoints[0].run_id.clone(),
            epoch: state.checkpoints[0].epoch,
            checkpoint_asset_id: state.checkpoints[0].checkpoint_asset_id.clone(),
        };

        return HttpResponse::Ok().json(&ConsumeTaskResponse::Ok {
            task: Task::SelfPlay {
                baseline,
                opponent,
                net_turn_iterations: 120,
            }
        });
    }

    let state = match &state.current_era_checkpoint {
        None => QuonsensusState {
            current_era_checkpoint: state.best_checkpoint.clone(),
            current_era: Some(0),
            ..state
        },
        Some(checkpoint) => if checkpoint == state.best_checkpoint.as_ref().unwrap() {
            state
        } else {
            QuonsensusState {
                current_era_checkpoint: state.best_checkpoint.clone(),
                current_era: Some(state.current_era.unwrap_or(0) + 1),
                ..state
            }
        }
    };
    save_quonsensus_state(state.clone()).await;

    let best_checkpoint = state.best_checkpoint.clone().unwrap();
    HttpResponse::Ok().json(&ConsumeTaskResponse::Ok {
        task: Task::GenerateSelfPlayDataset {
            checkpoint: CheckpointMetadata {
                net_name: best_checkpoint.net_name.clone(),
                run_id: best_checkpoint.run_id.clone(),
                epoch: best_checkpoint.epoch.clone(),
                checkpoint_asset_id: best_checkpoint.checkpoint_asset_id.clone(),
            },
            era: state.current_era.unwrap(),
            net_turn_iterations: 120,
        }
    })
}

#[post("/api/v1/quonsensus/self-play")]
pub async fn submit_self_play_outcome(body: Json<SubmitSelfPlayOutcomeRequest>) -> impl Responder {
    let state = load_quonsensus_state().await;

    if body.baseline.net_name != state.best_checkpoint.as_ref().unwrap().net_name
        || body.baseline.run_id != state.best_checkpoint.as_ref().unwrap().run_id
        || body.baseline.epoch != state.best_checkpoint.as_ref().unwrap().epoch {
        return HttpResponse::Ok().body("expired best checkpoint");
    }

    let opponent_checkpoint = &state.checkpoints[0];
    if body.opponent.net_name != opponent_checkpoint.net_name
        || body.opponent.run_id != opponent_checkpoint.run_id
        || body.opponent.epoch != opponent_checkpoint.epoch {
        return HttpResponse::Ok().body("expired opponent checkpoint");
    }

    let state = QuonsensusState {
        checkpoints: state.checkpoints.iter()
            .map(|v| {
                if v.net_name == body.opponent.net_name &&
                    v.run_id == body.opponent.run_id &&
                    v.epoch == body.opponent.epoch {
                    Checkpoint {
                        total_games: v.total_games  + 1,
                        total_wins: v.total_wins + if body.outcome {
                            1
                        } else {
                            0
                        },
                        ..v.clone()
                    }
                } else {
                    v.clone()
                }
            })
            .collect(),
        ..state
    };

    let win_rate = state.checkpoints[0].total_wins as f32 / state.checkpoints[0].total_games as f32;
    let mut test_finished = false;
    let mut new_best = false;

    if state.checkpoints[0].total_games >= 400 {
        test_finished = true;
        new_best = win_rate >= 0.55;
    }

    if state.checkpoints[0].total_games >= 300 && win_rate <= 0.45 {
        test_finished = true;
        new_best = false;
    }

    if state.checkpoints[0].total_games >= 200 && win_rate <= 0.35 {
        test_finished = true;
        new_best = false;
    }

    if state.checkpoints[0].total_games >= 100 && win_rate <= 0.25 {
        test_finished = true;
        new_best = false;
    }

    let state = if test_finished {
        let mut best_checkpoint = state.best_checkpoint.clone();
        let mut history = state.history.clone();

        if new_best {
            if best_checkpoint.is_some() {
                history = history.push_back(best_checkpoint.unwrap().clone());
            }
            best_checkpoint = Some(CheckpointV2 {
                net_name: state.checkpoints[0].net_name.clone(),
                run_id: state.checkpoints[0].run_id.clone(),
                epoch: state.checkpoints[0].epoch,
                checkpoint_asset_id: state.checkpoints[0].checkpoint_asset_id.clone(),
            });
        }

        QuonsensusState {
            best_checkpoint,
            checkpoints: state.checkpoints.iter().skip(1).cloned().collect(),
            history,
            ..state
        }
    } else {
        state
    };

    save_quonsensus_state(state).await;

    HttpResponse::Ok().body("ok")
}

#[post("/api/v1/quonsensus/reset")]
pub async fn reset_quonsensus_state() -> impl Responder {
    let state = load_quonsensus_state().await;
    let state = QuonsensusState {
        best_checkpoint: state.best_checkpoint,
        checkpoints: state.checkpoints.iter()
            // .filter(|checkpoint| checkpoint.run_id == "e8819b60-4997-4109-804a-39c2e8a581d3")
            .map(|checkpoint| Checkpoint {
                total_games: 0,
                total_wins: 0,
                ..checkpoint.clone()
            })
            .collect(),
        ..state
    };
    save_quonsensus_state(state).await;

    HttpResponse::Ok().body("ok")
}

#[get("/api/v1/quonsensus/self-play")]
pub async fn get_self_play_data_state() -> impl Responder {
    HttpResponse::Ok().json(&load_self_play_dataset_state().await)
}

#[get("/api/v1/quonsensus/self-play/stats")]
pub async fn get_self_play_data_stats() -> impl Responder {
    let mut stats: HashMap<u32, SelfPlayEraStats> = HashMap::new();

    for entry in load_self_play_dataset_state().await.entries {
        if !stats.contains_key(&entry.era) {
            stats.insert(entry.era, SelfPlayEraStats {
                era: entry.era,
                total_games: 0,
                total_entries: 0,
            });
        }

        let era_stats = stats.get_mut(&entry.era).unwrap();
        era_stats.total_games += entry.total_games;
        era_stats.total_entries += 1;
    }

    HttpResponse::Ok().json(SelfPlayDatasetStats {
        era_stats: stats.values().cloned().collect(),
    })
}

#[post("/api/v1/quonsensus/self-play/{era}")]
pub async fn save_self_play_data(req: HttpRequest, body: Bytes, era: web::Path<u32>) -> impl Responder {
    let total_games = req.headers().get("x-quortex-total-games").unwrap().to_str().unwrap().parse().unwrap();

    let bucket = Bucket::new("quoridor", Region::Custom {
        region: "europe-central2".to_owned(),
        endpoint: "https://storage.googleapis.com".to_owned(),
    }, Credentials::from_env().unwrap()).unwrap();

    let file_id = uuid::Uuid::new_v4().to_string();
    bucket.put_object_blocking(format!("quortex/self-play/dataset/{}", file_id), &body.to_vec()).unwrap();

    let mut state = load_self_play_dataset_state().await;
    state.entries.push(SelfPlayDatasetEntry {
        era: *era,
        total_games,
        asset_id: file_id.clone(),
    });
    save_self_play_dataset_state(state).await;

    file_id
}

#[post("/api/v1/quonsensus/tournament/opponents")]
pub async fn add_tournament_opponent(body: Json<AddTournamentOpponentRequest>) -> impl Responder {
    let state = load_quonsensus_state().await;
    let mut tournament_stats = state.tournament_stats.clone().unwrap_or(Vec::new());
    tournament_stats.push(TournamentStats {
        opponent: body.opponent.clone(),
        total_games: 0,
        games_in_progress: 0,
        total_wins: 0,
        errors: 0,
    });

    save_quonsensus_state(QuonsensusState {
        tournament_stats: Some(tournament_stats),
        ..state
    }).await;

    HttpResponse::Ok().body("ok")
}

#[post("/api/v1/quonsensus/tournament/opponents/remove")]
pub async fn remove_tournament_opponent(body: Json<RemoveOpponentRequest>) -> impl Responder {
    let state = load_quonsensus_state().await;
    let tournament_stats = state.tournament_stats.clone().unwrap_or(Vec::new())
        .into_iter()
        .filter(|v| v.opponent != body.opponent)
        .collect();

    save_quonsensus_state(QuonsensusState {
        tournament_stats: Some(tournament_stats),
        ..state
    }).await;

    HttpResponse::Ok().body("ok")
}

#[post("/api/v1/quonsensus/tournament")]
pub async fn submit_tournament_outcome(body: Json<SubmitTournamentOutcomeRequest>) -> impl Responder {
    let state = load_quonsensus_state().await;
    let tournament_stats = state.tournament_stats.clone().unwrap_or(Vec::new());
    let tournament_stats = tournament_stats.into_iter()
        .map(|v| if v.opponent == body.opponent {
            TournamentStats {
                total_games: v.total_games + body.total_games,
                games_in_progress: v.games_in_progress - body.total_games.min(v.games_in_progress),
                total_wins: v.total_wins + body.total_wins,
                errors: v.errors + body.errors,
                ..v
            }
        } else {
            v
        })
        .collect();

    save_quonsensus_state(QuonsensusState {
        tournament_stats: Some(tournament_stats),
        ..state
    }).await;

    HttpResponse::Ok().body("ok")
}

#[post("/api/v1/quonsensus/tournament/reset")]
pub async fn reset_tournament() -> impl Responder {
    let state = load_quonsensus_state().await;
    let tournament_stats = state.tournament_stats.clone().unwrap_or(Vec::new());
    let tournament_stats = tournament_stats.into_iter()
        .map(|v| TournamentStats {
            total_wins: 0,
            total_games: 0,
            errors: 0,
            games_in_progress: 0,
            ..v
        })
        .collect();

    info!("tournament stats after reset: {:?}", tournament_stats);

    save_quonsensus_state(QuonsensusState {
        tournament_stats: Some(tournament_stats),
        ..state
    }).await;

    HttpResponse::Ok().body("ok")
}

async fn load_self_play_dataset_state() -> SelfPlayDatasetState {
    let redis_client = redis::Client::open(redis_connection_string()).unwrap();
    let mut redis_connection = redis_client.get_async_connection().await.unwrap();

    match redis_connection.get::<&str, Option<String>>(SELF_PLAY_DATASET_STATE).await.unwrap() {
        Some(v) => serde_json::from_str(&v).unwrap(),
        None => return Default::default(),
    }
}

async fn save_self_play_dataset_state(state: SelfPlayDatasetState) {
    let redis_client = redis::Client::open(redis_connection_string()).unwrap();
    let mut redis_connection = redis_client.get_async_connection().await.unwrap();

    let state_json = serde_json::to_string(&state).unwrap();
    redis_connection.set::<&str, String, ()>(SELF_PLAY_DATASET_STATE, state_json).await.unwrap()
}

async fn load_quonsensus_state() -> QuonsensusState {
    let redis_client = redis::Client::open(redis_connection_string()).unwrap();
    let mut redis_connection = redis_client.get_async_connection().await.unwrap();

    match redis_connection.get::<&str, Option<String>>(QUONSENSUS_STATE).await.unwrap() {
        Some(v) => serde_json::from_str(&v).unwrap(),
        None => return Default::default(),
    }
}

async fn save_quonsensus_state(state: QuonsensusState) {
    let redis_client = redis::Client::open(redis_connection_string()).unwrap();
    let mut redis_connection = redis_client.get_async_connection().await.unwrap();

    let state_json = serde_json::to_string(&state).unwrap();
    redis_connection.set::<&str, String, ()>(QUONSENSUS_STATE, state_json).await.unwrap()
}
