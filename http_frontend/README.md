Online game flow:
- [done] Create game with POST /games
  [done] Each game room can be public or private
  [done] Each game room has a name
  [done] Each game room has join and spectate links
- [done] Join game as remote player with /games/{id}/join
  [done] The same link is used to invite bots to the game
- [done] Start game with /games/{id}/start
  [done] The oldest player in a room has rights for that
- Make moves with /games/{id}/moves