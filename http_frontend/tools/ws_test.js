const WebSocketClient = require('websocket').client;

const ws = new WebSocketClient();
ws.on('connect', connection => {
    connection.on('message', message => console.log('received message:', message.utf8Data));
    connection.on('error', err => console.error('error', err));
    connection.on('close', console.log);

    connection.send('hello world');
});

const host = 'ws://localhost:8080'; // 'wss://quoridor-api.nikitavbv.com';
ws.connect(`${host}/api/v1/games/7dfb6e32-a53f-40ff-a117-e44223b4c4a0/ws`);