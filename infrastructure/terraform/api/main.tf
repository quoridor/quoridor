terraform {
    backend "gcs" {
        bucket = "nikitavbv_tf_state"
        prefix = "quoridor/api"
    }
}

provider "google" {
    project = "nikitavbv"
    region = "europe-central2"
}

provider "google-beta" {
    project = "nikitavbv"
    region = "europe-central2"
}

variable "service_version" {
    type = string
    description = "Version of image to deploy"
    default = "0.1.51"
}

resource "google_cloud_run_service" "main_service" {
    provider = google-beta

    name = "quoridor-api"
    location = "europe-central2"

    autogenerate_revision_name = true

    metadata {
        annotations = {
            generated-by = "magic-modules"
            "run.googleapis.com/ingress" = "internal-and-cloud-load-balancing"
        }
    }


    template {
        metadata {
            annotations = {
                "autoscaling.knative.dev/maxScale" = "5"
                "run.googleapis.com/vpc-access-connector" = "projects/nikitavbv/locations/europe-central2/connectors/cloud-run-api-connector-w"
                "run.googleapis.com/vpc-access-egress" = "all-traffic"
            }
        }

        spec {
            service_account_name = "916750455653-compute@developer.gserviceaccount.com"
    
            containers {
                image = "eu.gcr.io/nikitavbv/quoridor/quoridor-api:${var.service_version}"

                resources {
                    limits = {
                        "cpu" = "4000m"
                        "memory" = "2048Mi"
                    }
                }

                env {
                    name = "GAME_STATE_STORE"
                    value = "redis"
                }

                env {
                    name = "RUST_LOG"
                    value = "info"
                }

                env {
                    name = "AWS_ACCESS_KEY_ID"

                    value_from {
                        secret_key_ref {
                            key = "1"
                            name = "quoridor-gcs-access-key-id"
                        }
                    }
                }

                env {
                    name = "AWS_SECRET_ACCESS_KEY"

                    value_from {
                        secret_key_ref {
                            key = "1"
                            name = "quoridor-gcs-secret-access-key"
                        }
                    }
                }
            }
        }
    }
}
