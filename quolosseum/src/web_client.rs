use quoridor_core::board::GameMove;
use rand::prelude::*;
use quoridor_core::player::PlayerId;
use http_frontend::api::{
    CreateRoomRequest,
    CreateRoomResponse,
    GameStateResponse,
    JoinRoomRequest,
    JoinRoomResponse,
    MakeMoveRequest,
    StartGameRequest,
    ActionToken,
    GameId,
};
use http_frontend::players::PlayerOptions;

pub struct WebClient {
    endpoint: String,
    frontend_endpoint: String,
    client: reqwest::blocking::Client,
}

impl WebClient {

    pub fn prod_client() -> Self {
        Self::new(
            "https://quoridor-api.nikitavbv.com".to_owned(),
            "https://quoridor.nikitavbv.com".to_string()
        )
    }

    pub fn local_client() -> Self {
        Self::new(
            "http://localhost:8080".to_string(),
            "http://localhost:8080".to_string(),
        )
    }

    pub fn new(api_endpoint: String, frontend_endpoint: String) -> Self {
        Self {
            endpoint: api_endpoint,
            frontend_endpoint: frontend_endpoint,
            client: reqwest::blocking::Client::new(),
        }
    }

    pub fn new_room(&self) -> CreateRoomResponse {
        let res = self.client.post(format!("{}/api/v1/games", self.endpoint))
            .json(&CreateRoomRequest {
                room_name: format!("quolosseum-{}", rand::thread_rng().gen_range(0..1000000)),
                is_public: false,
            })
            .send()
            .unwrap()
            .text()
            .unwrap();

        match serde_json::from_str(&res) {
            Ok(v) => v,
            Err(err) => panic!("Failed to decode create room request response as json: {:?}. Response: {}", err, res),
        }
    }

    pub fn join_room(&self, game_id: &GameId, join_token: &str, player_name: &str, pawn_style: &str) -> JoinRoomResponse {
        self.client.post(format!("{}/api/v1/games/{}/join", self.endpoint, game_id.to_string()))
            .json(&JoinRoomRequest {
                join_token: join_token.to_owned(),
                player: PlayerOptions::Human { // impostors, hahaha!
                    name: player_name.to_owned(),
                    pawn_style: pawn_style.to_owned(),
                }
            })
            .send()
            .unwrap()
            .json()
            .unwrap()
    }

    pub fn start_game(&self, game_id: &GameId, action_token: &ActionToken, first_move_by: Option<PlayerId>) -> GameStateResponse {
        self.client.post(format!("{}/api/v1/games/{}/start", self.endpoint, game_id.to_string()))
            .json(&StartGameRequest {
                action_token: action_token.clone(),
                first_move_by,
            })
            .send()
            .unwrap()
            .json()
            .unwrap()
    }

    pub fn make_move(&self, game_id: &GameId, action_token: &ActionToken, game_move: &GameMove) {
        let res = self.client.post(format!("{}/api/v1/games/{}/moves", self.endpoint, game_id.to_string()))
            .json(&MakeMoveRequest {
                action_token: action_token.clone(),
                game_move: game_move.clone(),
            })
            .send()
            .unwrap();
        let status = res.status();

        if !status.is_success() {
            panic!("Request to make a move failed: {:?}, response is: {}", game_move, res.text().unwrap());
        }
    }

    pub fn frontend_endpoint(&self) -> String {
        self.frontend_endpoint.clone()
    }
}