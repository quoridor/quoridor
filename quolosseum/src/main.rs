use std::fs;
use std::collections::HashMap;
use std::fs::DirEntry;
use std::thread::current;
use std::time::{Instant, Duration};
use std::sync::{Arc, Mutex};
use std::thread;
use agent_external::ExternalBot;
use agent_minimax::AgentMinimax;
use env_logger::Env;
use log::{info, warn, error};
use rand::Rng;
use agent_random::AgentRandom;
use histogram::Histogram;
use serde::Serialize;
use stretto::{Cache, DefaultKeyBuilder};
use quoridor_core::board::{Board, Direction, GameMoveByPlayer};
use quoridor_core::iteration_limiter::{IterationCountLimiter, IterationLimiter, IterationTimeLimiter};
use quoridor_core::notation::fen_notation::FenNotationEncoder;
use quoridor_core::player::{Player, PlayerAction, PlayerId};
use http_frontend::api::{CreateRoomRequest, CreateRoomResponse, GameStateResponse, JoinRoomRequest, JoinRoomResponse, MakeMoveRequest, StartGameRequest};
use http_frontend::players::PlayerOptions;
use agent_monte_carlo::MonteCarloBot;
use http_frontend::players::PlayerOptions::MinimaxBot;
use quortex::agent::QuortexBot;
use quortex::nets::fake::QuortexNetFake;
use quortex::quortex::{QuortexNet};
use quortex::nets::t0::QuortexNetT0;
use quortex::nets::t1::QuortexNetT1;
use quortex::nets::t2::QuortexNetT2;
use quortex::nets::net_cache::QuortexNetCacheLocal;
use crate::web_client::WebClient;

mod errors;
mod intro;
mod replay;
mod web_client;

const TOTAL_FIGHTS: u32 = 10;
const MAX_TOTAL_TURNS: u32 = 100;

struct QuolosseumState {
    total_fights: u32,

    win_stats: HashMap<PlayerId, usize>,
    total_draws: u32,
    turn_time: HashMap<PlayerId, Histogram>,
}

enum GameResult {
    Ok(Option<PlayerId>),
    Error((PlayerId, String)),
}

#[derive(Clone, Debug, Eq, PartialEq, Hash, Serialize)]
enum QuolosseumParticipant {
    Quortex,
    MonteCarlo,
    Minimax,
    External {
        name: String,
        command: String,
    },
}

#[derive(Serialize)]
struct WinStats {
    total_wins: u32,
    total_games: u32,
    total_errors: u32,
}

//noinspection DuplicatedCode
fn main() {
    intro::print_intro();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let time_per_move = Duration::from_secs(5);
    let player: QuolosseumParticipant = QuolosseumParticipant::Quortex;
    let opponents: Vec<QuolosseumParticipant> = external_participants(); /*vec![
        QuolosseumParticipant::External {
            name: "quompetitor-1".to_owned(),
            command: "external/quompetitor-1.exe".to_owned(),
        },
        QuolosseumParticipant::MonteCarlo,
        QuolosseumParticipant::Minimax,
    ];*/

    let mut win_stats: HashMap<QuolosseumParticipant, WinStats> = HashMap::new();

    for opponent in opponents {
        info!("running against opponent: {:?}", opponent);

        if !win_stats.contains_key(&opponent) {
            win_stats.insert(opponent.clone(), WinStats {
                total_wins: 0,
                total_games: 0,
                total_errors: 0,
            });
        }

        for _ in 0..TOTAL_FIGHTS {
            let mut stats = win_stats.get_mut(&opponent.clone()).unwrap();
            let player_is_white = rand::thread_rng().gen_bool(0.5);

            let player = instantiate_quolosseum_participant(
                &player,
                PlayerId::new(0),
                init_iteration_limiter(time_per_move.clone()),
                player_is_white,
            );
            let opponent = instantiate_quolosseum_participant(
                &opponent,
                PlayerId::new(1),
                init_iteration_limiter(time_per_move.clone()),
                !player_is_white,
            );
            let player_id = player.id().clone();
            let player_name = player.name();
            let opponent_name = opponent.name();

            let winner = match run_game(player, opponent, player_is_white) {
                GameResult::Ok(v) => v,
                GameResult::Error(err) => {
                    let player_name = if &player_id == &err.0 {
                        player_name.clone()
                    } else {
                        opponent_name.clone()
                    };
                    error!("Player {:?} errored: {}", player_name, err.1);
                    stats.total_errors += 1;
                    continue;
                }
            };

            stats.total_games += 1;
            stats.total_wins += if winner.is_some() && winner.unwrap() == player_id {
                1
            } else {
                0
            };

            print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
            let mut total_wins = 0;
            let mut total_games = 0;

            let mut all_results = "".to_owned();

            for (participant, win_stats) in &win_stats {
                all_results = format!("{}\n{}", all_results, format!("{: >16?} {:?} {}/{}, errors: {}", participant, win_stats.total_wins as f32 / win_stats.total_games as f32, win_stats.total_games, TOTAL_FIGHTS, win_stats.total_errors));
                total_wins += win_stats.total_wins;
                total_games += win_stats.total_games;
                println!("{: >16?} {:?} {}/{}, errors: {}", participant, win_stats.total_wins as f32 / win_stats.total_games as f32, win_stats.total_games, TOTAL_FIGHTS, win_stats.total_errors);
            }

            println!("global winrate: {}, total games: {}", total_wins as f32 / total_games as f32, total_games);
            fs::write("quolosseum_results", all_results);
        }
    }
}

fn init_iteration_limiter(time_per_move: Duration) -> Box<dyn IterationLimiter> {
    Box::new(IterationTimeLimiter::new(time_per_move))
    // Box::new(IterationCountLimiter::new(120))
}

fn run_game(player1: Box<dyn Player>, player2: Box<dyn Player>, player1_is_white: bool) -> GameResult {
    let mut players = if player1_is_white {
        vec![player2, player1]
    } else {
        vec![player1, player2]
    };

    let mut board = Board::new_for_two_players(
        players[0].id().clone(),
        players[1].id().clone(),
        players[1].id().clone(),
    );

    let mut total_turns = 0;

    while board.winner().is_none() {
        let started_at = Instant::now();
        let next_move_by = board.next_move_by();

        let player_move = {
            let player_for_next_move = players.iter_mut()
                .find(|player| player.id() == next_move_by)
                .unwrap();

            match player_for_next_move.perform_move(&board) {
                PlayerAction::PerformMove(v) => v,
                PlayerAction::Wait => panic!("Bots participating in quolosseum are not supposed to wait!"),
                PlayerAction::Error(err) => {
                    return GameResult::Error((player_for_next_move.id().clone(), err));
                }
            }
        };

        let player_pawn = board.pawns().iter()
            .find(|pawn| pawn.player_id() == next_move_by)
            .unwrap();
        if next_move_by == players[0].id() {
            if let Err(err) = players[1].on_opponent_move(player_move.clone(), player_pawn) {
                return GameResult::Error((players[1].id().clone(), err));
            }
        } else {
            if let Err(err) = players[0].on_opponent_move(player_move.clone(), player_pawn) {
                return GameResult::Error((players[0].id().clone(), err));
            }
        }

        board = match board.apply_move(
            GameMoveByPlayer::new(next_move_by.clone(), player_move.clone())
        ) {
            Ok(v) => v,
            Err(err) => {
                let err = format!("Failed to apply move ({:?}) to the board: {:?}, board fen is: {}", player_move, err, board.to_fen_notation());
                return GameResult::Error((next_move_by.clone(), err));
            },
        };

        total_turns += 1;
        if total_turns >= MAX_TOTAL_TURNS {
            break;
        }
    }

    GameResult::Ok(board.winner().cloned())
}

fn instantiate_quolosseum_participant(participant: &QuolosseumParticipant, player_id: PlayerId, iteration_limiter: Box<dyn IterationLimiter>, is_white: bool) -> Box<dyn Player> {
    match participant {
        QuolosseumParticipant::Quortex => Box::new(QuortexBot::new_with_net(
            player_id,
            iteration_limiter,
            Box::new(QuortexNetCacheLocal::new(
                Box::new(QuortexNetT2::best()),
                Arc::new(Cache::new(1_000_000, 1_000_000_000, DefaultKeyBuilder::default()).unwrap()),
                "".to_owned()
            )),
            true,
        )),
        QuolosseumParticipant::MonteCarlo => Box::new(MonteCarloBot::new(
            player_id,
            "MonteCarlo".to_owned(),
            iteration_limiter,
        )),
        QuolosseumParticipant::Minimax => Box::new(AgentMinimax::new(
            player_id,
            "Minimax".to_owned(),
        )),
        QuolosseumParticipant::External { name, command } => Box::new(ExternalBot::new(
            player_id,
            name.clone(),
            command.clone(),
            is_white
        )),
    }
}

fn external_participants() -> Vec<QuolosseumParticipant> {
    let mut result = Vec::new();
    let paths = fs::read_dir("external").unwrap();

    for path in paths {
        let path = path.unwrap();
        if !path.file_type().unwrap().is_dir() {
            continue;
        }

        let subpaths = fs::read_dir(&path.path()).unwrap();
        let exe_path: Vec<String> = subpaths.into_iter()
            .filter(|v| v.as_ref().unwrap().file_name().to_string_lossy().to_string().ends_with(".exe"))
            .map(|v| v.unwrap().path().to_string_lossy().to_string())
            .collect();

        let exe_path = if exe_path.len() == 1 {
            exe_path[0].clone()
        } else {
            let exe_path: Vec<String> = exe_path.iter()
                .filter(|v| !v.to_lowercase().contains("aitester"))
                .cloned()
                .collect();

            if exe_path.len() == 1 {
                exe_path[0].clone()
            } else {
                let ai_tester: Vec<String> = exe_path.iter()
                    .filter(|v| v.to_lowercase().contains("aiproject"))
                    .cloned()
                    .collect();

                if ai_tester.len() == 1 {
                    ai_tester[0].clone()
                } else {
                    panic!("Could not find executable: {:?}", exe_path);
                }
            }
        };

        result.push(QuolosseumParticipant::External {
            name: path.file_name().to_string_lossy().to_string(),
            command: exe_path,
        })
    }

    result
}