use std::thread;
use std::time::Duration;
use std::fs;
use log::info;
use quoridor_core::board::{Board, GameMove, GameMoveByPlayer};
use quoridor_core::notation::ai_tester::AiTesterNotationDecoder;
use quoridor_core::player::PlayerId;
use crate::web_client::WebClient;

pub struct ReplayLog {
    player_pawn_is_white: bool,
    moves: Vec<String>,
}

pub fn run_replay() {
    info!("running replay");

    let replay_log = read_loss_from_log().unwrap();

    // ids are reversed because http-frontend works so...
    let white = PlayerId::new(1);
    let black = PlayerId::new(0);

    // origin for white is south
    // origin for black is north
    let mut board = Board::new_for_two_players(
        black,
        white.clone(),
        white.clone(),
    );

    let client = WebClient::prod_client();
    let room = client.new_room();
    // black is north
    let player_black = client.join_room(&room.id, &room.join_token, "Black", "black.svg");
    let player_white = client.join_room(&room.id, &room.join_token, "White", "blue.svg");
    client.start_game(&room.id, player_black.action_token.as_ref().unwrap(), Some(white.clone()));

    info!("Started online game: {}/games/{}", client.frontend_endpoint(), room.id.to_string());
    info!("This player has {} pawn", if replay_log.player_pawn_is_white { "white" } else { "black" });
    thread::sleep(Duration::from_secs(5));

    for mv in replay_log.moves {
        let next_move_by = board.next_move_by().clone();
        let next_move_by_pawn = board.pawns().iter().find(|p| p.player_id() == &next_move_by).unwrap();
        let game_move = GameMove::from_ai_tester_notation(&mv, next_move_by_pawn).unwrap();
        board = board.apply_move(GameMoveByPlayer {
            player_id: next_move_by.clone(),
            game_move
        }).unwrap();

        info!("Making move: {:?} with player {:?}", game_move, next_move_by);
        let action_token = if &next_move_by == &white {
            player_white.action_token.as_ref().unwrap()
        } else {
            player_black.action_token.as_ref().unwrap()
        };
        client.make_move(&room.id, action_token, &game_move);

        thread::sleep(Duration::from_secs(2));
    }

    info!("replay finished");
}

fn read_loss_from_log() -> Option<ReplayLog> {
    let mut moves_str: Vec<String> = Vec::new();
    let mut pawn_is_white = false;

    for line in  fs::read_to_string("run_results.log").unwrap().lines() {
        if line.contains("->") || line.contains("<-") {
            let line: String = line.split("|").last().unwrap().to_owned()[2..].trim().to_owned();
            moves_str.push(line);
        } else if line.contains("Black") {
            pawn_is_white = false;
        } else if line.contains("White") {
            pawn_is_white = true;
        } else if line.contains("Win") {
            moves_str.clear();
        } else if line.contains("Loss") {
            return Some(ReplayLog {
                player_pawn_is_white: pawn_is_white,
                moves: moves_str
            })
        }
    }

    return None
}