use thiserror::Error;

#[derive(Debug, Error, Clone)]
pub enum QuolosseumError {
    #[error("Failed to parse move: {0:?}")]
    FailedToParseMove(String),
}