package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"cloud.google.com/go/logging"
)

type TelemetryIngressRequest struct {
	Logs []interface{}
}

type TelemetryHandler struct {
	cloudLoggingLogger *logging.Logger
}

func main() {
	ctx := context.Background()
	projectID := "nikitavbv"

	client, err := logging.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	defer client.Close()

	logName := "quortex"

	logger := client.Logger(logName)
	http.Handle("/telemetry", &TelemetryHandler{
		cloudLoggingLogger: logger,
	})

	log.Println("telemetry server started")
	http.ListenAndServe(":8080", nil)
}

func (h *TelemetryHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	decoder := json.NewDecoder(req.Body)
	var telemetry TelemetryIngressRequest
	err := decoder.Decode(&telemetry)
	if err != nil {
		panic(err)
	}

	for _, s := range telemetry.Logs {
		h.cloudLoggingLogger.Log(logging.Entry{
			Payload: s,
		})
	}

	fmt.Fprintf(w, "ok")
}
