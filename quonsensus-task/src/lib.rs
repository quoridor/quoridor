use quonsensus_core::QuonsensusTask;

struct HelloWorldTask {
}

impl QuonsensusTask for HelloWorldTask {
    fn run(&self) {
        println!("Hello World!");
    }
}

impl HelloWorldTask {
    pub fn new() -> Self {
        Self {
        }
    }
}

#[no_mangle]
pub fn _quonsensus_init() -> Box<dyn QuonsensusTask> {
    Box::new(HelloWorldTask::new())
}