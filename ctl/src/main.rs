use serde::Deserialize;

#[derive(Deserialize, Debug)]
struct QuonsensusState {
    tournament_stats: Vec<TournamentStats>,
}

#[derive(Deserialize, Debug)]
struct TournamentStats {
    opponent: String,
    total_games: u32,
    total_wins: u32,
}

fn main() {
    let res: QuonsensusState = reqwest::blocking::get("https://quoridor.nikitavbv.com/api/v1/quonsensus")
        .unwrap()
        .json()
        .unwrap();

    //let res: QuonsensusState = serde_json::from_str(&std::fs::read_to_string("/Users/nikitavbv/t1_backup.json").unwrap()).unwrap();

    let top1 = vec![
        "victor-v2".to_owned(),
        "zurich".to_owned(),
        "florida".to_owned(),
        "king".to_owned()
    ];

    let mut total_games = 0;
    let mut total_wins = 0;

    let mut top1_games = 0;
    let mut top1_wins = 0;

    let mut min_games = None;

    for stats in res.tournament_stats {
        /*if stats.opponent == "victor-v2" || stats.opponent == "bravo" {
            continue;
        }*/

        let winrate = stats.total_wins as f64 / stats.total_games as f64;

        if winrate < 0.6 {
            println!("low winrate: {} {}", stats.opponent, winrate);
        }

        total_games += stats.total_games;
        total_wins += stats.total_wins;

        if top1.contains(&stats.opponent) {
            top1_games += stats.total_games;
            top1_wins += stats.total_wins;
        }

        if min_games.is_none() || stats.total_games < min_games.unwrap() {
            min_games = Some(stats.total_games);
        }
    }

    println!("top1 winrate: {:?}", top1_wins as f64 / top1_games as f64);
    println!("global winrate: {:?}", total_wins as f64 / total_games as f64);
    println!("min games: {}", min_games.unwrap());
}
