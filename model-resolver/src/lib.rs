use std::env::current_exe;
use std::path::Path;

pub fn resolve(model_type: &str, model_id: &str) -> String {
    let local_model = model_id.to_string();
    if Path::new(&local_model).exists() {
        return local_model;
    }

    let quortex_models = format!("../quortex/models/{}/{}", model_type, model_id);
    if Path::new(&quortex_models).exists() {
        return quortex_models;
    }

    let current_exe_path = format!("{}/{}", Path::new(&current_exe().unwrap()).parent().unwrap().to_string_lossy().to_string(), model_id);
    if Path::new(&current_exe_path).exists() {
        return current_exe_path;
    }

    panic!("Could not find model file");
}