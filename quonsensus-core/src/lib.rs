use serde::{Serialize, Deserialize};

// tasks
#[derive(Serialize, Deserialize)]
pub struct CreateTaskRequest {
    pub task: Task,
    pub copies: u32,
}

#[derive(Serialize, Deserialize)]
#[serde(tag="status")]
pub enum ConsumeTaskResponse {
    NoTasksAvailable,
    Ok { task: Task }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag="type")]
pub enum Task {
    SelfPlay {
        baseline: CheckpointMetadata,
        opponent: CheckpointMetadata,
        net_turn_iterations: u64,
    },
    GenerateSelfPlayDataset {
        checkpoint: CheckpointMetadata,
        net_turn_iterations: u64,
        era: u32,
    },
    Tournament {
        opponent: String,
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CheckpointMetadata {
    pub net_name: String,
    pub run_id: String,
    pub epoch: u32,
    pub checkpoint_asset_id: String,
}

pub trait QuonsensusTask {
    fn run(&self);
}

// checkpoints
#[derive(Serialize, Deserialize)]
pub struct SubmitCheckpointRequest {
    pub net_name: String,
    pub run_id: String,
    pub epoch: u32,
    pub value_loss: f32,
    pub policy_loss: f32,
    pub checkpoint_asset_id: String,
}

// fighting
#[derive(Serialize, Deserialize, Clone)]
pub struct SubmitSelfPlayOutcomeRequest {
    pub baseline: CheckpointMetadata,
    pub opponent: CheckpointMetadata,
    pub outcome: bool,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SubmitTournamentOutcomeRequest {
    pub opponent: String,
    pub total_games: u32,
    pub total_wins: u32,
    pub errors: u32,
}