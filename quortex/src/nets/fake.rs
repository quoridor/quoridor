use std::collections::HashMap;
use quoridor_core::board::Board;
use quoridor_core::iteration_limiter::IterationCountLimiter;
use quoridor_core::player::PlayerId;
use tch::nn::VarStore;
use tch::Tensor;
use agent_monte_carlo::MonteCarloBot;
use crate::quortex::{BoardEvaluation, QuortexNet};
use crate::tensor_utils::policy_to_tensor;
use crate::training_data::TrainingDataEntry;

#[derive(Debug)]
pub struct QuortexNetFake {
}

impl QuortexNetFake {

    pub fn new() -> Self {
        Self {
        }
    }
}

impl QuortexNet for QuortexNetFake {

    fn name(&self) -> String {
        "fake".to_owned()
    }

    fn evaluate_board(&self, board: &Board) -> BoardEvaluation {
        if board.winner().is_some() {
            panic!("Unexpected board with winner");
        }

        let mut bot = MonteCarloBot::new(
            board.next_move_by().clone(),
            "MonteCarlo".to_owned(),
            Box::new(IterationCountLimiter::new(4000))
        );

        let moves_with_scores = bot.moves_with_scores(board);
        let policy = policy_to_tensor(&moves_with_scores);
        let value = moves_with_scores.iter().map(|v| v.1).reduce(f32::max).unwrap() * 2.0 - 1.0;

        BoardEvaluation::new(value, policy)
    }

    fn var_store(&self) -> &VarStore {
        panic!("You cannot call var_store() on fake net")
    }

    fn load_model(&mut self, _path: &str) {
        panic!("You cannot call load_model() on fake net")
    }

    fn forward(&self, _xs: &Tensor, _train: bool) -> (Tensor, Tensor) {
        panic!("You cannot call forward() on fake net")
    }
}