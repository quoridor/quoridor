use tch::{Device, Kind, Tensor};
use tch::nn::{self, BatchNorm, Conv2D, Linear, VarStore, ModuleT, ConvConfig};
use quoridor_core::board::Board;
use quoridor_core::player::PlayerId;
use crate::quortex::QuortexNet;

#[derive(Debug)]
pub struct QuortexNetT3 {
    vs: VarStore,

    input_conv: Conv2D,
    input_batchnorm: BatchNorm,

    res1_conv1: Conv2D,
    res1_batchnorm1: BatchNorm,
    res1_conv2: Conv2D,
    res1_batchnorm2: BatchNorm,

    res2_conv1: Conv2D,
    res2_batchnorm1: BatchNorm,
    res2_conv2: Conv2D,
    res2_batchnorm2: BatchNorm,

    res3_conv1: Conv2D,
    res3_batchnorm1: BatchNorm,
    res3_conv2: Conv2D,
    res3_batchnorm2: BatchNorm,

    res4_conv1: Conv2D,
    res4_batchnorm1: BatchNorm,
    res4_conv2: Conv2D,
    res4_batchnorm2: BatchNorm,

    res5_conv1: Conv2D,
    res5_batchnorm1: BatchNorm,
    res5_conv2: Conv2D,
    res5_batchnorm2: BatchNorm,

    res6_conv1: Conv2D,
    res6_batchnorm1: BatchNorm,
    res6_conv2: Conv2D,
    res6_batchnorm2: BatchNorm,

    res7_conv1: Conv2D,
    res7_batchnorm1: BatchNorm,
    res7_conv2: Conv2D,
    res7_batchnorm2: BatchNorm,

    policy1_conv: Conv2D,
    policy1_batchnorm: BatchNorm,
    policy2_conv: Conv2D,
    policy2_batchnorm: BatchNorm,
    policy_out: Linear,

    value_conv: Conv2D,
    value_batchnorm: BatchNorm,
    value_dense: Linear,
    value_out: Linear,
}

impl QuortexNetT3 {

    pub fn new() -> Self {
        let vs = VarStore::new(Device::cuda_if_available());
        let root = vs.root();

        let input_conv = nn::conv2d(&root, 6, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let input_batchnorm = nn::batch_norm2d(&root, 256, Default::default());

        let res1_conv1 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res1_batchnorm1 = nn::batch_norm2d(&root, 256, Default::default());
        let res1_conv2 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res1_batchnorm2 = nn::batch_norm2d(&root, 256, Default::default());

        let res2_conv1 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res2_batchnorm1 = nn::batch_norm2d(&root, 256, Default::default());
        let res2_conv2 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res2_batchnorm2 = nn::batch_norm2d(&root, 256, Default::default());

        let res3_conv1 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res3_batchnorm1 = nn::batch_norm2d(&root, 256, Default::default());
        let res3_conv2 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res3_batchnorm2 = nn::batch_norm2d(&root, 256, Default::default());

        let res4_conv1 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res4_batchnorm1 = nn::batch_norm2d(&root, 256, Default::default());
        let res4_conv2 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res4_batchnorm2 = nn::batch_norm2d(&root, 256, Default::default());

        let res5_conv1 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res5_batchnorm1 = nn::batch_norm2d(&root, 256, Default::default());
        let res5_conv2 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res5_batchnorm2 = nn::batch_norm2d(&root, 256, Default::default());

        let res6_conv1 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res6_batchnorm1 = nn::batch_norm2d(&root, 256, Default::default());
        let res6_conv2 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res6_batchnorm2 = nn::batch_norm2d(&root, 256, Default::default());

        let res7_conv1 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res7_batchnorm1 = nn::batch_norm2d(&root, 256, Default::default());
        let res7_conv2 = nn::conv2d(&root, 256, 256, 3, ConvConfig {
            padding: 1,
            ..Default::default()
        });
        let res7_batchnorm2 = nn::batch_norm2d(&root, 256, Default::default());

        let policy1_conv = nn::conv2d(&root, 256, 256, 1, Default::default());
        let policy1_batchnorm = nn::batch_norm2d(&root, 256, Default::default());
        let policy2_conv = nn::conv2d(&root, 256, 80, 1, Default::default());
        let policy2_batchnorm = nn::batch_norm2d(&root, 80, Default::default());
        let policy_out = nn::linear(&root, 6480, 12 + 81 * 2, Default::default());

        let value_conv = nn::conv2d(&root, 256, 4, 1, Default::default());
        let value_batchnorm = nn::batch_norm2d(&root, 4, Default::default());
        let value_dense = nn::linear(&root, 324, 256, Default::default());
        let value_out = nn::linear(&root, 256, 1, Default::default());

        Self {
            vs,

            input_conv,
            input_batchnorm,

            res1_conv1,
            res1_batchnorm1,
            res1_conv2,
            res1_batchnorm2,

            res2_conv1,
            res2_batchnorm1,
            res2_conv2,
            res2_batchnorm2,

            res3_conv1,
            res3_batchnorm1,
            res3_conv2,
            res3_batchnorm2,

            res4_conv1,
            res4_batchnorm1,
            res4_conv2,
            res4_batchnorm2,

            res5_conv1,
            res5_batchnorm1,
            res5_conv2,
            res5_batchnorm2,

            res6_conv1,
            res6_batchnorm1,
            res6_conv2,
            res6_batchnorm2,

            res7_conv1,
            res7_batchnorm1,
            res7_conv2,
            res7_batchnorm2,

            policy1_conv,
            policy1_batchnorm,
            policy2_conv,
            policy2_batchnorm,
            policy_out,

            value_conv,
            value_batchnorm,
            value_dense,
            value_out,
        }
    }

    pub fn load_model(&mut self, path: &str) {
        self.vs.load(path).unwrap();
    }

    pub fn best() -> Self {
        let mut net = Self::new();
        net.load_model("../quortex/models/t0/checkpoint-2");
        net
    }
}

impl QuortexNet for QuortexNetT3 {

    fn name(&self) -> String {
        "t3".to_owned()
    }

    fn var_store(&self) -> &VarStore {
        &self.vs
    }

    fn load_model(&mut self, path: &str) {
        self.vs.load(path).unwrap()
    }

    fn forward(&self, xs: &Tensor, train: bool) -> (Tensor, Tensor) {
        let s = xs
            .apply(&self.input_conv)
            .apply_t(&self.input_batchnorm, train)
            .relu();

        let r = s
            .apply(&self.res1_conv1)
            .apply_t(&self.res1_batchnorm1, train)
            .relu()
            .apply(&self.res1_conv2)
            .apply_t(&self.res1_batchnorm2, train);
        let s = (s + r).relu();

        let r = s
            .apply(&self.res2_conv1)
            .apply_t(&self.res2_batchnorm1, train)
            .relu()
            .apply(&self.res2_conv2)
            .apply_t(&self.res2_batchnorm2, train);
        let s = (s + r).relu();

        let r = s
            .apply(&self.res3_conv1)
            .apply_t(&self.res3_batchnorm1, train)
            .relu()
            .apply(&self.res3_conv2)
            .apply_t(&self.res3_batchnorm2, train);
        let s = (s + r).relu();

        let r = s
            .apply(&self.res4_conv1)
            .apply_t(&self.res4_batchnorm1, train)
            .relu()
            .apply(&self.res4_conv2)
            .apply_t(&self.res4_batchnorm2, train);
        let s = (s + r).relu();

        let r = s
            .apply(&self.res5_conv1)
            .apply_t(&self.res5_batchnorm1, train)
            .relu()
            .apply(&self.res5_conv2)
            .apply_t(&self.res5_batchnorm2, train);
        let s = (s + r).relu();

        let r = s
            .apply(&self.res6_conv1)
            .apply_t(&self.res6_batchnorm1, train)
            .relu()
            .apply(&self.res6_conv2)
            .apply_t(&self.res6_batchnorm2, train);
        let s = (s + r).relu();

        let r = s
            .apply(&self.res7_conv1)
            .apply_t(&self.res7_batchnorm1, train)
            .relu()
            .apply(&self.res7_conv2)
            .apply_t(&self.res7_batchnorm2, train);
        let s = (s + r).relu();

        let value = s
            .apply(&self.value_conv)
            .apply_t(&self.value_batchnorm, train)
            .relu()
            .view([-1, 324])
            .apply(&self.value_dense)
            .apply(&self.value_out);

        let policy = s
            .apply(&self.policy1_conv)
            .apply_t(&self.policy1_batchnorm, train)
            .relu()
            .apply(&self.policy2_conv)
            .apply_t(&self.policy2_batchnorm, train)
            .view([-1, 6480])
            .apply(&self.policy_out);

        (
            policy,
            value.tanh()
        )
    }

    fn apply_softmax_to_policy(&self) -> bool {
        false
    }
}
