use std::fmt::{Debug, Formatter};
use std::sync::{Arc, Mutex};
use quoridor_core::board::Board;
use tch::nn::VarStore;
use tch::Tensor;
use stretto::{Cache, DefaultKeyBuilder};
use crate::quortex::{BoardEvaluation, QuortexNet};
use crate::training_data::TrainingDataEntry;

pub struct QuortexNetCacheLocal {
    net: Box<dyn QuortexNet + Send>,
    cache: Arc<Cache<(String, u64), BoardEvaluation, DefaultKeyBuilder>>,
    cache_prefix: String,
}

impl QuortexNetCacheLocal {

    pub fn new(net: Box<dyn QuortexNet + Send>, cache: Arc<Cache<(String, u64), BoardEvaluation, DefaultKeyBuilder>>, cache_prefix: String) -> Self {
        Self {
            net,
            cache,
            cache_prefix,
        }
    }
}

impl QuortexNet for QuortexNetCacheLocal {

    fn name(&self) -> String {
        self.net.name()
    }

    fn var_store(&self) -> &VarStore {
        self.net.var_store()
    }

    fn load_model(&mut self, path: &str) {
        self.net.load_model(path)
    }

    fn forward(&self, xs: &Tensor, train: bool) -> (Tensor, Tensor) {
        self.net.forward(xs, train)
    }

    fn evaluate_board(&self, board: &Board) -> BoardEvaluation {
        let board_hash = board.hash();
        let cache_key = (self.cache_prefix.clone(), board_hash);

        if let Some(v) = self.cache.get(&cache_key) {
            return v.value().clone();
        }

        let evaluation = self.net.evaluate_board(board);
        self.cache.insert(cache_key, evaluation.clone(), 1);
        evaluation
    }

    fn apply_softmax_to_policy(&self) -> bool {
        self.net.apply_softmax_to_policy()
    }
}

impl Debug for QuortexNetCacheLocal {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        "QuortexNetCacheLocal {}".fmt(f)
    }
}