use std::{fs, thread};
use std::fs::OpenOptions;
use std::io::{BufWriter, Cursor, Write};
use std::process::exit;
use std::time::{Duration, Instant};
use log::*;
use quoridor_core::player::{Player, PlayerAction, PlayerId};
use rand::prelude::*;
use agent_monte_carlo::MonteCarloBot;
use quoridor_core::board::{Board, Direction, GameMoveByPlayer, WallDirection};
use quoridor_core::iteration_limiter::IterationCountLimiter;
use s3::{Bucket, Region};
use s3::creds::Credentials;
use serde::{Serialize, Deserialize};
use crate::tensor_utils::{board_to_tensor, policy_to_tensor};

const MAX_TURNS: usize = 250;

#[derive(Debug, Serialize, Deserialize)]
pub struct TrainingDataEntry {
    pub state: Vec<f32>,
    pub policy: Vec<f32>,
    pub value: f32,
}

pub fn combine_v2() {
    let mut combined_data = Vec::new();
    let mut total_parts = 0;
    let mut chunk_index = 0;

    for path in fs::read_dir("./dataset/raw").unwrap() {
        let path = path.unwrap().path();
        let compressed = fs::read(&path).unwrap();
        let uncompressed = match zstd::stream::decode_all(Cursor::new(compressed)) {
            Ok(v) => v,
            Err(err) => {
                error!("failed to read part: {:?}", err);
                continue;
            },
        };
        let part: Vec<TrainingDataEntry> = bincode::deserialize(&uncompressed).unwrap();
        total_parts += part.len();

        let mut filtered_parts = Vec::new();
        for entry in part.into_iter() {
            let mut skip = false;

            for t in &entry.policy {
                if t.is_nan() {
                    skip = true;
                    break;
                }
            }

            if skip {
                continue;
            }

            filtered_parts.push(entry);
        }

        combined_data.append(&mut filtered_parts);

        if combined_data.len() >= 100_000 {
            let source = bincode::serialize(&combined_data).unwrap();
            let compressed = zstd::stream::encode_all(Cursor::new(source), 10).unwrap();
            fs::write(format!("dataset/parts/{}", chunk_index), &compressed).unwrap();
            chunk_index += 1;
            combined_data.clear();
        }

        info!("Reading, total entries: {}", total_parts);
    }
}

pub fn generate_training_data_v2() {
    let mut handles = Vec::new();

    for _ in 0..num_cpus::get() {
        handles.push(thread::spawn(|| {
            let mut training_data = Vec::new();
            let mut last_dump_time = Instant::now();

            let bucket = Bucket::new("quoridor", Region::Custom {
                region: "europe-central2".to_owned(),
                endpoint: "https://storage.googleapis.com".to_owned(),
            }, Credentials::from_env().unwrap()).unwrap();

            loop {
                training_data.append(&mut generate_training_data_entry_v2());

                let current_time = Instant::now();
                if current_time - last_dump_time > Duration::from_secs(600) {
                    last_dump_time = current_time;
                    let file_id = uuid::Uuid::new_v4().to_string();
                    info!("saving {} entries to {}", training_data.len(), file_id);

                    let source = bincode::serialize(&training_data).unwrap();
                    let compressed = zstd::stream::encode_all(Cursor::new(source), 10).unwrap();
                    bucket.put_object_blocking(format!("quortex/dataset-v2/{}", file_id), &compressed).unwrap();
                    training_data.clear();
                }
            }
        }))
    }

    for handle in handles {
        handle.join().unwrap();
    }
}

pub fn generate_training_data_entry_v2() -> Vec<TrainingDataEntry> {
    let player1 = PlayerId::new(0);
    let player2 = PlayerId::new(1);
    let mut board = Board::new_for_two_players(
        player1.clone(),
        player2.clone(),
        if thread_rng().gen_bool(0.5) {
            player1.clone()
        } else {
            player2.clone()
        }
    );

    let north_player_id = board.pawns().iter()
        .find(|pawn| pawn.origin() == &Direction::North)
        .unwrap()
        .player_id()
        .clone();

    let mut monte_carlo_1 = MonteCarloBot::from_id(player1.clone(), Box::new(IterationCountLimiter::new(35000)));
    let mut monte_carlo_2 = MonteCarloBot::from_id(player2.clone(), Box::new(IterationCountLimiter::new(35000)));

    let mut training_data = Vec::new();

    while board.winner().is_none() {
        if training_data.len() > MAX_TURNS {
            return Vec::new();
        }

        let next_move_by = board.next_move_by();

        let bot = if next_move_by == &player1 {
            &mut monte_carlo_1
        } else {
            &mut monte_carlo_2
        };
        // TODO: better approach for random moves
        /*bot.set_time_per_move(if rand::thread_rng().gen_bool(0.05) {
            100
        } else {
            1000
        });*/

        let mv = if next_move_by == &north_player_id {
            let moves_with_scores = bot.moves_with_scores(&board);
            training_data.push(TrainingDataEntry {
                state: board_to_tensor(&board),
                policy: policy_to_tensor(&moves_with_scores),
                value: 0.0,
            });

            let mut best_move = None;
            let mut best_score = 0.0;
            for entry in moves_with_scores {
                if best_move.is_none() || entry.1 > best_score {
                    best_score = entry.1;
                    best_move = Some(entry.0);
                }
            }

            best_move.unwrap()
        } else {
            match bot.perform_move(&board) {
                PlayerAction::PerformMove(v) => v,
                PlayerAction::Wait => panic!("Bots don't wait"),
                PlayerAction::Error(err) => panic!("Bot error: {}", err),
            }
        };

        board = board.apply_move_unchecked(GameMoveByPlayer {
            game_move: mv.clone(),
            player_id: next_move_by.clone(),
        });
    }

    let winner = board.pawns().iter()
        .find(|pawn| pawn.player_id() == board.winner().unwrap()).unwrap()
        .origin();

    training_data.iter().map(|t| TrainingDataEntry {
        state: t.state.clone(),
        policy: t.policy.clone(),
        value: match winner {
            Direction::South => 1.0,
            Direction::North => -1.0,
            other => panic!("Unexpected origin: {:?}", other)
        }
    }).collect()
}
