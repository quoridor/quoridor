use quoridor_core::board::{Board, Direction, GameMove, MoveDirection, WallDirection};

pub fn game_move_policy_tensor_index(game_move: &GameMove) -> usize {
    match game_move {
        GameMove::MovePawn(direction) => {
            match direction {
                MoveDirection::North => 0,
                MoveDirection::JumpNorth => 1,
                MoveDirection::NorthEast => 2,
                MoveDirection::JumpEast => 3,
                MoveDirection::NorthWest => 4,
                MoveDirection::East => 5,
                MoveDirection::SouthEast => 6,
                MoveDirection::JumpSouth => 7,
                MoveDirection::SouthWest => 8,
                MoveDirection::South => 9,
                MoveDirection::West => 10,
                MoveDirection::JumpWest => 11,
            }
        },
        GameMove::PlaceWall(wall) => {
            let position = wall.position();
            12 + position.row() as usize * 9 + position.column() as usize + if wall.direction() == &WallDirection::Vertical {
                81
            } else {
                0
            }
        }
    }
}

pub fn board_to_tensor(board: &Board) -> Vec<f32> {
    // turn is always performed by the north player
    let layers = 2 + 2 + 2; // horizontal walls, vertical walls, n player position, s player position, n player walls, s player walls
    let board_size = 9;
    let mut tensor = vec![0.0; layers * board_size * board_size];

    for wall in board.walls() {
        let position = wall.position();
        let index = position.row() as usize * 9 + position.column() as usize + if wall.direction() == &WallDirection::Vertical {
            81
        } else {
            0
        };
        tensor[index] = 1.0;
    }

    let north_pawn = board.pawns().iter()
        .find(|pawn| pawn.origin() == &Direction::North)
        .unwrap();
    let north_pawn_position = north_pawn.position();
    tensor[north_pawn_position.row() as usize * 9 + north_pawn_position.column() as usize + 2 * 81] = 1.0;

    let south_pawn = board.pawns().iter()
        .find(|pawn| pawn.origin() == &Direction::South)
        .unwrap();
    let south_pawn_position = south_pawn.position();
    tensor[south_pawn_position.row() as usize * 9 + south_pawn_position.column() as usize + 3 * 81] = 1.0;

    let north_remaining_walls = *board.remaining_walls().get(north_pawn.player_id()).unwrap_or(&0);
    for i in 0..81 {
        tensor[4 * 81 + i] = north_remaining_walls as f32 / 10.0;
    }

    let south_remaining_walls = *board.remaining_walls().get(south_pawn.player_id()).unwrap_or(&0);
    for i in 0..81 {
        tensor[5 * 81 + i] = south_remaining_walls as f32 / 10.0;
    }

    tensor
}

pub fn policy_to_tensor(moves: &Vec<(GameMove, f32)>) -> Vec<f32> {
    let mut policy = vec![0.0; 12 + 81 * 2]; // 12 pawn moves, 81 horizontal walls, 81 vertical walls

    let mut total_denominator = 0.0;
    for mv in moves {
        total_denominator += mv.1;
    }

    for mv in moves {
        policy[game_move_policy_tensor_index(&mv.0)] = mv.1 / total_denominator;
    }

    policy
}

pub fn mirror_policy(policy: &Vec<f32>) -> Vec<f32> {
    let mut mirrored = vec![0.0; policy.len()];
    mirrored[0] = policy[9];
    mirrored[1] = policy[7];
    mirrored[2] = policy[8];
    mirrored[3] = policy[11];
    mirrored[4] = policy[6];
    mirrored[5] = policy[10];
    mirrored[6] = policy[4];
    mirrored[7] = policy[1];
    mirrored[8] = policy[2];
    mirrored[9] = policy[0];
    mirrored[10] = policy[5];
    mirrored[11] = policy[3];

    // those cells are meaningless, but we fill them for consistency:
    for i in 0..9 {
        mirrored[12 + i] = policy[12 + i];
        mirrored[12 + i + 81] = policy[12 + i + 81];
    }

    for row in 1..9 {
        let mirrored_row = 9 - row;

        for column in 0..8 {
            let mirrored_column = 7 - column;

            mirrored[12 + row * 9 + column] = policy[12 + mirrored_row * 9 + mirrored_column];
            mirrored[12 + row * 9 + column + 81] = policy[12 + mirrored_row * 9 + mirrored_column + 81];
        }

        // those cells are meaningless, but we fill them for consistency:
        mirrored[12 + row * 9 + 8] = policy[12 + mirrored_row * 9 + 8];
        mirrored[12 + row * 9 + 8 + 81] = policy[12 + mirrored_row * 9 + 8 + 81];
    }

    mirrored
}
