use std::cmp::Ordering;
use std::fmt::{Debug, Formatter};
use std::process::exit;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};
use log::info;
use rand::prelude::*;
use quoridor_core::board::{Board, Direction, GameMove, GameMoveByPlayer, MoveDirection, Wall, WallDirection};
use quoridor_core::iteration_limiter::{IterationLimiter, IterationTimeLimiter};
use quoridor_core::player::{Player, PlayerAction, PlayerId};
use quoridor_core::position::BoardPosition;
use rand_distr::Dirichlet;
use crate::nets::fake::QuortexNetFake;
use crate::quortex::QuortexNet;
use crate::nets::t0::QuortexNetT0;
use crate::nets::t1::QuortexNetT1;
use crate::nets::t2::QuortexNetT2;

#[derive(Clone)]
pub struct QuortexBot {
    id: PlayerId,
    net: Arc<Mutex<Box<dyn QuortexNet + Send>>>,
    iteration_limiter: Box<dyn IterationLimiter>,
    self_play_mode: bool,
}

#[derive(Clone, Debug)]
pub struct QuortexNode {
    game_move: Option<GameMove>,
    prior: f32,

    visit_count: u32,
    value_sum: f32,

    children: Vec<QuortexNode>,
}

impl QuortexBot {

    pub fn new_with_net(id: PlayerId, iteration_limiter: Box<dyn IterationLimiter>, net: Box<dyn QuortexNet + Send>, self_play_mode: bool) -> Self {
        Self {
            id,
            net: Arc::new(Mutex::new(net)),
            iteration_limiter,
            self_play_mode,
        }
    }

    pub fn new(id: PlayerId, iteration_limiter: Box<dyn IterationLimiter>, self_play_mode: bool) -> Self {
        Self::new_with_net(id, iteration_limiter, Box::new(QuortexNetT1::best()), self_play_mode)
    }

    pub fn perform_move_and_return_root(&mut self, board: &Board) -> (GameMove, QuortexNode) {
        self.iteration_limiter.reset();

        let net = self.net.clone();
        let net = net.lock().unwrap();
        let mut node = QuortexNode::root();

        while !self.iteration_limiter.has_reached_limit() || node.children.is_empty() {
            node.explore(&net, &board, self.self_play_mode);
            self.iteration_limiter.on_iteration_complete();
        }

        //for child in &node.children {
        //    println!("child visit count: {:?} {}", child.game_move, child.visit_count);
        //}

        let most_visited = node.most_visited_child();
        // TODO: can we get rid of this eventually?
        // Note: routes are sub-optimal, we can loose due to a bad one, lol
        if most_visited.game_move.unwrap().is_move_pawn() {
            let mut board = board.clone();
            board.force_compute_shortest_routes();

            let pawn_moves = board.get_possible_pawn_moves(board.next_move_by());

            let shortest_route = board.shortest_route_for_player(board.next_move_by())
                .expect("Expected shortest route to be pre-computed");
            let direction = shortest_route.head_direction().unwrap();
            return (*pawn_moves.iter().find(|mv| mv.as_pawn_move().unwrap().get_deg() == direction.get_deg())
                .unwrap_or_else(|| pawn_moves.choose(&mut rand::thread_rng()).unwrap()), node);
        }

        (most_visited.game_move.unwrap(), node)
    }
}

impl Debug for QuortexBot {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        "QuortexBot {}".fmt(f)
    }
}

impl Player for QuortexBot {

    fn id(&self) -> &PlayerId {
        &self.id
    }

    fn name(&self) -> String {
        "Quortex".to_owned()
    }

    fn perform_move(&mut self, board: &Board) -> PlayerAction {
        PlayerAction::PerformMove(self.perform_move_and_return_root(board).0)
    }
}

impl QuortexNode {

    pub fn from_scored_move(game_move: GameMove, prior: f32) -> Self {
        Self {
            game_move: Some(game_move),
            prior,

            visit_count: 0,
            value_sum: 0.0,

            children: Vec::new(),
        }
    }

    pub fn root() -> Self {
        Self {
            game_move: None,
            prior: 1.0,

            visit_count: 0,
            value_sum: 0.0,

            children: Vec::new(),
        }
    }

    // Returns value of this position. Value is prediction of winner:
    // 1 means the current turn player wins
    // -1 means the other player wins
    pub fn explore(&mut self, net: &Box<dyn QuortexNet + Send>, board: &Board, self_play_mode: bool) -> f32 {
        self.visit_count += 1;

        if board.winner().is_some() {
            return -1.0; // previous player is a winner
        }

        if self.children.is_empty() {
            let evaluation = net.evaluate_board(board);
            let possible_moves: Vec<GameMove> = board.get_possible_moves(&board.next_move_by()).into_iter().collect();

            let dirichlet = if board.moves_history().len() <= 10 {
                Some(Dirichlet::new(&possible_moves.iter().map(|_| 0.016).collect::<Vec<f32>>()).unwrap())
            } else {
                None
            };
            let samples = dirichlet.map(|d| d.sample(&mut rand::thread_rng()));

            let mut scores = Vec::new();

            for i in 0..possible_moves.len() {
                let mv = possible_moves[i];
                scores.push(0.75 * evaluation.move_score(&mv) + 0.25 * samples.as_ref().map(|s| s[i]).unwrap_or(0.0));
            }

            let mut min_score = scores.iter().reduce(|a, b| if a < b { a } else { b }).unwrap_or(&0.0);

            for i in 0..possible_moves.len() {
                let mv = possible_moves[i];
                let score = scores[i] - min_score;
                self.children.push(QuortexNode::from_scored_move(mv, score));
            }

            evaluation.value()
        } else {
            let mut best_child = None;
            let mut best_child_score = 0.0;

            for i in 0..self.children.len() {
                let score = self.children[i].ucb_score(&self);
                if best_child.is_none() || score > best_child_score {
                    best_child = Some(i);
                    best_child_score = score;
                }
            }

            let mut best_child = self.children.remove(best_child.unwrap());

            let board = board.apply_move_unchecked(GameMoveByPlayer {
                player_id: board.next_move_by().clone(),
                game_move: best_child.game_move.unwrap().clone(),
            });

            let score = -best_child.explore(net, &board, self_play_mode);
            self.value_sum += score;
            self.children.push(best_child);
            score
        }
    }

    pub fn most_visited_child(&self) -> &QuortexNode {
        let mut best_child = None;
        let mut best_child_score = 0;

        for child in &self.children {
            if best_child.is_none() || child.visit_count > best_child_score {
                best_child = Some(child);
                best_child_score = child.visit_count;
            }
        }

        best_child.unwrap()
    }

    fn ucb_score(&self, parent: &QuortexNode) -> f32 {
        let prior_score = 0.4 * self.prior * (parent.visit_count as f32).sqrt() / (self.visit_count + 1) as f32;
        let value_score = if self.visit_count > 0 {
            -self.value()
        } else {
            0.0
        };

        value_score + prior_score
    }

    fn value(&self) -> f32 {
        if self.visit_count == 0 {
            0.0
        } else {
            self.value_sum / (self.visit_count as f32)
        }
    }

    pub fn game_move(&self) -> Option<&GameMove> {
        self.game_move.as_ref()
    }

    pub fn visit_count(&self) -> u32 {
        self.visit_count
    }

    pub fn children(&self) -> &Vec<QuortexNode> {
        &self.children
    }
}
