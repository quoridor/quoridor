use std::env::current_exe;
use std::fs;
use std::path::Path;
use std::io::Cursor;
use std::ops::{Mul, Sub};
use std::thread::current;
use std::time::{Duration, Instant};
use env_logger::Env;
use tch::{Tensor, nn::{self, OptimizerConfig, ModuleT}, Kind, Reduction};
use tch::data::Iter2;
use log::{info, error};
use rand::prelude::*;
use indicatif::{ProgressBar, ProgressStyle};
use quonsensus_core::{CreateTaskRequest, SubmitCheckpointRequest, Task};
use s3::bucket::Bucket;
use s3::creds::Credentials;
use s3::Region;
use serde::Deserialize;
use quoridor_core::iteration_limiter::IterationCountLimiter;
use quoridor_core::player::{Player, PlayerId};
use agent_monte_carlo::MonteCarloBot;
use crate::agent::QuortexBot;
use crate::data_loader::DataLoader;
use crate::quortex::QuortexNet;
use crate::nets::{
    t0::QuortexNetT0,
    t1::QuortexNetT1,
    t2::QuortexNetT2,
    t3::QuortexNetT3,
    t4::QuortexNetT4,
    t5::QuortexNetT5,
};
use crate::training_data::TrainingDataEntry;

mod nets;
mod agent;
mod data_loader;
mod intro;
mod quortex;
mod training_data;
mod tensor_utils;

#[derive(Deserialize)]
struct QuortexConfig {
    s3_access_key: String,
    s3_secret_key: String,
}

#[derive(Deserialize)]
struct QuonsensusState {
    best_checkpoint: CheckpointMetadata,
}

#[derive(Deserialize)]
struct CheckpointMetadata {
    net_name: String,
    run_id: String,
    epoch: u32,
    checkpoint_asset_id: String,
}

fn main() {
    intro::print_intro();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let config: QuortexConfig = serde_json::from_slice(&fs::read("./quortex_config.json").unwrap()).unwrap();

    let args: Vec<String> = std::env::args().collect();
    if args.contains(&"generate-training-data".to_owned()) {
        training_data::generate_training_data_v2();
    } else if args.contains(&"combine-training-data".to_owned())  {
        training_data::combine_v2();
    }

    train_net(&config, &fetch_best_checkpoint_net());
}

fn train_net(config: &QuortexConfig, net: &Box<dyn QuortexNet>) {
    info!("training net {}", net.name());
    fs::create_dir_all(format!("models/{}", net.name())).unwrap();

    let run_id = uuid::Uuid::new_v4().to_string();

    let bucket = Bucket::new("quoridor", Region::Custom {
        region: "europe-central2".to_owned(),
        endpoint: "https://storage.googleapis.com".to_owned(),
    }, Credentials::new(Some(&config.s3_access_key), Some(&config.s3_secret_key), None, None, None).unwrap()).unwrap();

    // let mut opt = nn::Adam::default().build(net.var_store(), 0.01).unwrap();
    let mut opt = nn::Sgd::default().build(net.var_store(), 0.01).unwrap();

    let mut epoch = 0;
    let epoch_length = 1024; // in batches

    let batch_size = 3000;

    let mut data_loader = DataLoader::new();

    info!("starting training, run id: {}", run_id);
    loop {
        epoch += 1;

        let pb = ProgressBar::new(epoch_length);
        pb.set_style(ProgressStyle::default_bar()
            .template("[{prefix}] {bar:40.cyan/blue} {pos:>7}/{len:7} {msg}")
            .progress_chars("##-"));
        pb.set_prefix(format!("Epoch {}", epoch));

        let mut value_loss_sum = 0.0;
        let mut value_loss_total = 0;
        let mut policy_loss_sum = 0.0;
        let mut policy_loss_total = 0;

        for _ in 0..epoch_length {
            let batch = data_loader.next_batch(batch_size);
            let state = net.state_tensor_from_training_data(&batch);
            let true_policy = net.policy_tensor_from_training_data(&batch);
            let true_value = net.value_tensor_from_training_data(&batch);

            opt.zero_grad();
            let (policy_prediction, value_prediction) = net.forward(&state, true);
            let policy_loss = policy_loss(&policy_prediction, &true_policy);
            let value_loss = value_loss(&value_prediction, &true_value);

            policy_loss_sum += f32::from(&policy_loss);
            policy_loss_total += 1;
            value_loss_sum += f32::from(&value_loss);
            value_loss_total += 1;

            let total_loss = policy_loss + value_loss;
            opt.backward_step(&total_loss);

            pb.inc(1);
        }
        pb.finish_and_clear();

        let model_checkpoint_path = format!("models/{}/checkpoint-{}", net.name(), epoch);
        net.var_store().save(&model_checkpoint_path).unwrap();

        info!("Sending epoch {} to quonsensus", epoch);
        let asset_id = uuid::Uuid::new_v4().to_string();
        let asset_data = fs::read(&model_checkpoint_path).unwrap();
        bucket.put_object_blocking(format!("quonsensus/assets/{}", &asset_id), &asset_data).unwrap();

        submit_to_quonsensus(
            net.name(),
            run_id.clone(),
            epoch,
            value_loss_sum / (value_loss_total as f32),
            policy_loss_sum / (policy_loss_total as f32),
            asset_id
        );
    }
}

fn policy_loss(prediction: &Tensor, actual: &Tensor) -> Tensor {
    prediction.cross_entropy_loss::<Tensor>(actual, None, Reduction::Mean, -1, 0.0)
}

fn value_loss(prediction: &Tensor, actual: &Tensor) -> Tensor {
    0.1 * prediction.mse_loss(actual, Reduction::Mean)
}

fn load_dataset_part(part_index: u64) -> Vec<TrainingDataEntry> {
    let compressed = fs::read(format!("dataset/parts/{}", part_index)).unwrap();
    let data = zstd::stream::decode_all(Cursor::new(compressed)).unwrap();
    let dataset: Vec<TrainingDataEntry> = bincode::deserialize(&data).unwrap();

    dataset
}

fn is_running_on_gpu_instance() -> bool {
    std::env::var("TORCH_CUDA_VERSION").is_ok()
}

fn fetch_best_checkpoint_net() -> Box<dyn QuortexNet> {
    let client = reqwest::blocking::Client::new();
    let quonsensus_state: QuonsensusState = client.get("https://quoridor-api.nikitavbv.com/api/v1/quonsensus")
        .send()
        .unwrap()
        .json()
        .unwrap();

    let asset_id = quonsensus_state.best_checkpoint.checkpoint_asset_id.as_str();
    if !asset_exists(asset_id) {
        download_asset(asset_id);
    }

    let mut net = match quonsensus_state.best_checkpoint.net_name.as_str() {
        "t2" => Box::new(QuortexNetT2::new()),
        other => panic!("Unexpected net name: {:?}", other),
    };

    net.load_model(&asset_path(asset_id));

    net
}

fn submit_to_quonsensus(net_name: String, run_id: String, epoch: u32, value_loss: f32, policy_loss: f32, checkpoint_asset_id: String) {
    let client = reqwest::blocking::Client::new();
    client.post("https://quoridor-api.nikitavbv.com/api/v1/quonsensus/checkpoints")
        .json(&SubmitCheckpointRequest {
            net_name,
            run_id,
            epoch,
            value_loss,
            policy_loss,
            checkpoint_asset_id,
        })
        .send()
        .unwrap();
}

fn asset_exists(asset_id: &str) -> bool {
    Path::new(&asset_path(asset_id)).exists()
}

fn download_asset(asset_id: &str) -> Result<(), ()> {
    info!("Downloading asset {}", asset_id);

    let assets_dir = Path::new("assets");
    if !assets_dir.exists() {
        fs::create_dir_all(assets_dir).unwrap();
    }

    let client = reqwest::blocking::Client::new();
    let res = client.get(format!("https://quoridor.nikitavbv.com/quonsensus/assets/{}", asset_id))
        .send()
        .unwrap()
        .bytes()
        .unwrap()
        .to_vec();

    match fs::write(asset_path(asset_id), &res) {
        Ok(v) => Ok(()),
        Err(err) => {
            error!("failed to write asset to disk: {:?}", err);
            Err(())
        }
    }
}

fn asset_path(asset_id: &str) -> String {
    format!("assets/{}", asset_id)
}
