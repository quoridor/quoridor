use std::fs;
use std::io::Cursor;
use std::path::Path;
use log::{info, error};
use rand::prelude::*;
use serde::Deserialize;
use crate::training_data::TrainingDataEntry;

pub struct DataLoader {
    client: reqwest::blocking::Client,

    dataset_parts_queue: Vec<SelfPlayDatasetEntry>,
    buffer: Vec<TrainingDataEntry>,
}

#[derive(Deserialize)]
struct QuonsensusSelfPlayState {
    entries: Vec<SelfPlayDatasetEntry>,
}

#[derive(Deserialize)]
struct SelfPlayDatasetEntry {
    era: u32,
    asset_id: String,
    total_games: u32,
}

impl DataLoader {

    pub fn new() -> Self {
        Self {
            client: reqwest::blocking::Client::new(),

            dataset_parts_queue: Vec::new(),
            buffer: Vec::new(),
        }
    }

    pub fn next_batch(&mut self, batch_size: usize) -> Vec<TrainingDataEntry> {
        while self.buffer.len() < batch_size {
            self.extend_buffer().unwrap();
        }

        self.buffer.drain(0..batch_size).collect()
    }

    fn extend_buffer(&mut self) -> Result<(), ()> {
        if self.dataset_parts_queue.is_empty() {
            self.dataset_parts_queue = self.fetch_parts_list();
            self.dataset_parts_queue.shuffle(&mut rand::thread_rng());
        }

        let asset_id = &self.dataset_parts_queue.pop().unwrap().asset_id;
        self.buffer.append(&mut self.load_dataset_part(asset_id)?);
        self.buffer.shuffle(&mut rand::thread_rng());

        Ok(())
    }

    fn load_dataset_part(&self, asset_id: &str) -> Result<Vec<TrainingDataEntry>, ()> {
        if !asset_exists(asset_id) {
            download_asset(asset_id)?
        }

        let compressed = match fs::read(asset_path(asset_id)) {
            Ok(v) => v,
            Err(err) => {
                error!("failed to read dataset part: {:?}", err);
                return Err(());
            }
        };

        let data = zstd::stream::decode_all(Cursor::new(compressed)).unwrap();
        let dataset: Vec<TrainingDataEntry> = bincode::deserialize(&data).unwrap();

        Ok(dataset)
    }

    fn fetch_parts_list(&self) -> Vec<SelfPlayDatasetEntry> {
        let state: QuonsensusSelfPlayState = self.client.get("https://quoridor-api.nikitavbv.com/api/v1/quonsensus/self-play")
            .send()
            .unwrap()
            .json()
            .unwrap();


        state.entries
    }
}

fn asset_exists(asset_id: &str) -> bool {
    Path::new(&asset_path(asset_id)).exists()
}

fn download_asset(asset_id: &str) -> Result<(), ()> {
    info!("Downloading asset {}", asset_id);

    let assets_dir = Path::new("assets");
    if !assets_dir.exists() {
        fs::create_dir_all(assets_dir).unwrap();
    }

    let client = reqwest::blocking::Client::new();
    let res = client.get(format!("https://quoridor.nikitavbv.com/quortex/self-play/dataset/{}", asset_id))
        .send()
        .unwrap()
        .bytes()
        .unwrap()
        .to_vec();

    match fs::write(asset_path(asset_id), &res) {
        Ok(v) => Ok(()),
        Err(err) => {
            error!("failed to write asset to disk: {:?}", err);
            Err(())
        }
    }
}

fn asset_path(asset_id: &str) -> String {
    format!("dataset/{}", asset_id)
}
