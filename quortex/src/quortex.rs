use std::ops::Sub;
use std::sync::Arc;
use std::time::{Duration, Instant};
use std::collections::HashSet;
use tch::nn::{BatchNorm, Conv2D, ConvConfig, Linear, OptimizerConfig};
use tch::{nn::{self, VarStore, ModuleT}, Device, Tensor, Kind, no_grad};
use quoridor_core::board::{Board, Direction, GameMove, GameMoveByPlayer, WallDirection};
use quoridor_core::player::{Player, PlayerAction, PlayerId};
use crate::training_data::TrainingDataEntry;
use crate::tensor_utils::{board_to_tensor, game_move_policy_tensor_index, mirror_policy};

pub trait QuortexNet {

    fn name(&self) -> String;

    fn var_store(&self) -> &VarStore;

    fn load_model(&mut self, path: &str);

    fn forward(&self, xs: &Tensor, train: bool) -> (Tensor, Tensor); // (policy, value)

    fn evaluate_board(&self, board: &Board) -> BoardEvaluation {
        let next_move_by = board.next_move_by();
        let next_move_pawn = board.pawns().iter()
            .find(|pawn| pawn.player_id() == next_move_by)
            .unwrap();

        // net is trained when the current player is north, so we need to mirror if current player is South
        let state_tensor = if next_move_pawn.origin() == &Direction::North {
            board_to_tensor(board)
        } else {
            board_to_tensor(&board.mirror())
        };

        let state_tensor = self.xs_tensor_from_slice(&state_tensor, 1);
        let (policy_tensor, value_tensor) = self.forward(&state_tensor, false);
        let policy: Vec<f32> = Vec::from(if self.apply_softmax_to_policy() {
            policy_tensor.softmax(1, Kind::Float)
        } else {
            policy_tensor
        });
        let policy = if next_move_pawn.origin() == &Direction::North {
            policy
        } else {
            mirror_policy(&policy)
        };

        // multiply by -1 because value if set to be -1 for north player
        let value = -f32::from(value_tensor);

        BoardEvaluation {
            value,
            policy,
        }
    }

    fn apply_softmax_to_policy(&self) -> bool {
        true
    }

    fn state_tensor_from_training_data(&self, batch: &[TrainingDataEntry]) -> Tensor {
        self.xs_tensor_from_slice(
            &batch.iter().flat_map(|v| v.state.iter().cloned()).collect::<Vec<f32>>(),
            batch.len() as i64,
        )
    }

    fn xs_tensor_from_slice(&self, xs: &[f32], batch_size: i64) -> Tensor {
        Tensor::of_slice(&xs)
            .reshape(&[batch_size as i64, 6, 9, 9])
            .to_device(self.var_store().device())
    }

    fn policy_tensor_from_training_data(&self, batch: &[TrainingDataEntry]) -> Tensor {
        self.policy_tensor_from_slice(
            &batch.iter().flat_map(|v| v.policy.iter().cloned()).collect::<Vec<f32>>(),
            batch.len() as i64,
        )
    }

    fn policy_tensor_from_slice(&self, ys: &[f32], batch_size: i64) -> Tensor {
        Tensor::of_slice(&ys)
            .reshape(&[batch_size as i64, 12 + 81 * 2])
            .to_device(self.var_store().device())
    }

    fn value_tensor_from_training_data(&self, batch: &[TrainingDataEntry]) -> Tensor {
        self.value_tensor_from_slice(
            &batch.iter().map(|v| v.value).collect::<Vec<f32>>(),
            batch.len() as i64,
        )
    }

    fn value_tensor_from_slice(&self, ys: &[f32], batch_size: i64) -> Tensor {
        Tensor::of_slice(&ys)
            .reshape(&[batch_size as i64, 1])
            .to_device(self.var_store().device())
    }
}

#[derive(Clone)]
pub struct BoardEvaluation {
    value: f32,
    policy: Vec<f32>,
}

impl BoardEvaluation {

    pub fn new(value: f32, policy: Vec<f32>) -> Self {
        Self {
            value,
            policy,
        }
    }

    pub fn move_score(&self, game_move: &GameMove) -> f32 {
        self.policy[game_move_policy_tensor_index(game_move)]
    }

    pub fn value(&self) -> f32 {
        self.value
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use quoridor_core::board::{Board, Direction, Pawn, Wall, WallDirection};
    use quoridor_core::player::PlayerId;
    use quoridor_core::position::BoardPosition;
    use crate::nets::t0::QuortexNetT0;
    use crate::nets::t2::QuortexNetT2;
    use crate::quortex::{mirror_policy, QuortexNet};
    use crate::tensor_utils::board_to_tensor;

    #[test]
    fn value_for_north_simple() {
        let mut net = QuortexNetT2::best();

        let north_pawn = Pawn::construct(PlayerId::new(0), Direction::North, BoardPosition::new(3, 7));
        let south_pawn = Pawn::construct(PlayerId::new(1), Direction::South, BoardPosition::new(4, 7));

        let mut board = Board::construct(
            PlayerId::new(0),
            Vec::new(),
            vec![north_pawn, south_pawn],
            {
                let mut remaining_walls = HashMap::new();
                remaining_walls.insert(PlayerId::new(0), 10);
                remaining_walls.insert(PlayerId::new(1), 10);
                remaining_walls
            },
            Vec::new(),
        );

        let evaluation = net.evaluate_board(&board);
        assert!(evaluation.value() > 0.1);
    }

    #[test]
    fn value_for_south_simple() {
        let mut net = QuortexNetT2::best();

        let north_pawn = Pawn::construct(PlayerId::new(0), Direction::North, BoardPosition::new(3, 7));
        let south_pawn = Pawn::construct(PlayerId::new(1), Direction::South, BoardPosition::new(4, 7));

        let board = Board::construct(
            PlayerId::new(1),
            Vec::new(),
            vec![north_pawn, south_pawn],
            {
                let mut remaining_walls = HashMap::new();
                remaining_walls.insert(PlayerId::new(0), 10);
                remaining_walls.insert(PlayerId::new(1), 10);
                remaining_walls
            },
            Vec::new(),
        );

        let evaluation = net.evaluate_board(&board);
        assert!(evaluation.value() < -0.1);
    }

    #[test]
    fn board_mirroring() {
        let normal_north_pawn = Pawn::construct(PlayerId::new(0), Direction::North, BoardPosition::new(3, 7));
        let normal_south_pawn = Pawn::construct(PlayerId::new(1), Direction::South, BoardPosition::new(4, 7));
        let normal_board = Board::construct(
            PlayerId::new(0),
            Vec::new(),
            vec![normal_north_pawn, normal_south_pawn],
            {
                let mut remaining_walls = HashMap::new();
                remaining_walls.insert(PlayerId::new(0), 10);
                remaining_walls.insert(PlayerId::new(1), 10);
                remaining_walls
            },
            Vec::new(),
        );
        let normal_tensor = board_to_tensor(&normal_board);

        let mirrored_north_pawn = Pawn::construct(PlayerId::new(0), Direction::South, BoardPosition::new(5, 1));
        let mirrored_south_pawn = Pawn::construct(PlayerId::new(1), Direction::North, BoardPosition::new(4, 1));
        let mirrored_board = Board::construct(
            PlayerId::new(0),
            Vec::new(),
            vec![mirrored_north_pawn, mirrored_south_pawn],
            {
                let mut remaining_walls = HashMap::new();
                remaining_walls.insert(PlayerId::new(0), 10);
                remaining_walls.insert(PlayerId::new(1), 10);
                remaining_walls
            },
            Vec::new(),
        );
        let mirrored_mirror_tensor = board_to_tensor(&mirrored_board.mirror());

        assert_eq!(normal_tensor, mirrored_mirror_tensor);
    }

    #[test]
    fn policy_mirroring() {
        let mut net = QuortexNetT2::best();

        let normal_north_pawn = Pawn::construct(PlayerId::new(0), Direction::North, BoardPosition::new(3, 7));
        let normal_south_pawn = Pawn::construct(PlayerId::new(1), Direction::South, BoardPosition::new(4, 7));
        let normal_board = Board::construct(
            PlayerId::new(0),
            vec![Wall::new(BoardPosition::new(2, 3), WallDirection::Horizontal)],
            vec![normal_north_pawn, normal_south_pawn],
            {
                let mut remaining_walls = HashMap::new();
                remaining_walls.insert(PlayerId::new(0), 10);
                remaining_walls.insert(PlayerId::new(1), 10);
                remaining_walls
            },
            Vec::new(),
        );

        let mirrored_north_pawn = Pawn::construct(PlayerId::new(0), Direction::South, BoardPosition::new(5, 1));
        let mirrored_south_pawn = Pawn::construct(PlayerId::new(1), Direction::North, BoardPosition::new(4, 1));
        let mirrored_board = Board::construct(
            PlayerId::new(0),
            vec![Wall::new(BoardPosition::new(5, 6), WallDirection::Horizontal)],
            vec![mirrored_north_pawn, mirrored_south_pawn],
            {
                let mut remaining_walls = HashMap::new();
                remaining_walls.insert(PlayerId::new(0), 10);
                remaining_walls.insert(PlayerId::new(1), 10);
                remaining_walls
            },
            Vec::new(),
        );

        let normal_policy = net.evaluate_board(&normal_board).policy;
        let mirrored_policy = mirror_policy(&net.evaluate_board(&mirrored_board).policy);

        assert_eq!(normal_policy, mirrored_policy);
    }
}