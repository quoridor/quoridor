# Notes

- Integrate Quortex into http frontend and web-client.
- Update Quolosseum to run single-threaded tests against multiple implementations and produce a report
- Setup training data generation

- Wait for current training iteration to finish.
- Calculate 250 games.
- Pick candidates for new training iteration. Play against the best opponent we currently have according to quolosseum.
