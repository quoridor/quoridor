use std::cell::Cell;
use std::io::{BufRead, BufReader, Read, Write};
use std::path::Path;
use std::process::{Child, Command, Stdio};
use std::rc::Rc;
use std::sync::{Arc, mpsc, Mutex};
use std::thread;
use std::thread::sleep;
use std::time::Duration;
use quoridor_core::board::{Board, GameMove, Pawn};
use quoridor_core::notation::ai_tester::{AiTesterNotationDecoder, AiTesterNotationEncoder};
use quoridor_core::player::{Player, PlayerAction, PlayerId};

#[derive(Clone, Debug)]
pub struct ExternalBot {
    id: PlayerId,
    name: String,
    child_process: Arc<Mutex<Child>>,
}

impl ExternalBot {

    pub fn new(id: PlayerId, name: String, command: String, is_white: bool) -> Self {
        let child_process = Command::new(&command)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .current_dir(Path::new(&command).parent().unwrap())
            .spawn().unwrap();
        child_process.stdin.as_ref().unwrap()
            .write(if is_white { "white\n".to_owned() } else { "black\n".to_owned() }.as_bytes())
            .unwrap();

        Self {
            id,
            name,
            child_process: Arc::new(Mutex::new(child_process)),
        }
    }

    fn read_next_child_command(&mut self) -> Option<String> {
        let mut child_process = self.child_process.lock().unwrap();
        let mut buffer = vec![0u8; 1024];

        let stdout = child_process.stdout.as_mut().unwrap();
        let len = stdout.read(&mut buffer).unwrap();
        let res = String::from_utf8_lossy(&buffer[0..len]).to_string().replace("\r\n", "").replace("\n", "");
        Some(res)
    }
}

impl Player for ExternalBot {

    fn id(&self) -> &PlayerId {
        &self.id
    }

    fn name(&self) -> String {
        self.name.clone()
    }

    fn perform_move(&mut self, board: &Board) -> PlayerAction {
        let self_pawn = board.pawns().iter()
            .find(|pawn| pawn.player_id() == self.id())
            .unwrap();

        let mut command: Option<String> = None;
        loop {
            let next_command = match self.read_next_child_command() {
                Some(v) => v,
                None => return PlayerAction::Error("timed out waiting for external bot move".to_owned()),
            };
            if !next_command.starts_with("//") {
                command = Some(next_command);
                break;
            }
        }

        let command = command.unwrap();
        PlayerAction::PerformMove(match GameMove::from_ai_tester_notation(&command, self_pawn) {
            Ok(v) => v,
            Err(err) => return PlayerAction::Error(format!("failed to parse ai tester notation: {:?}, full output is: {}", err, command)),
        })
    }

    fn on_opponent_move(&self, opponent_move: GameMove, opponent_pawn_before_move: &Pawn) -> Result<(), String> {
        let mut child_process = self.child_process.lock().unwrap();
        let move_str = format!("{}\n", opponent_move.to_ai_tester_notation(opponent_pawn_before_move));

        match child_process.stdin.as_mut() {
            Some(v) => {
                if let Err(err) = v.write(move_str.as_bytes()) {
                    return Err(format!("Failed to write to child stdin because error: {:?}", err));
                }
            },
            None => {
                return Err(format!("Failed to write to child stdin because stdin is None"));
            }
        };

        Ok(())
    }

    fn shutdown(&self) {
        self.child_process.lock().unwrap().kill();
    }
}