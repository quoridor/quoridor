use quoridor_core::board::Board;
use quoridor_core::player::PlayerId;
use quoridor_core::reachability_checker::DistanceChecker;
use std::fmt::Debug;

pub trait Heuristic: Debug {
    fn heuristic(&self, board: &Board) -> f32;

    fn clone(&self) -> Box<dyn Heuristic + Send>;
}

#[derive(Debug)]
pub struct SimpleHeuristic {
    player_id: PlayerId,
    distance_checker: Box<dyn DistanceChecker + Send>,
}

impl SimpleHeuristic {
    pub fn new(player_id: PlayerId, distance_checker: Box<dyn DistanceChecker + Send>) -> Self {
        Self {
            player_id,
            distance_checker,
        }
    }
}

impl Heuristic for SimpleHeuristic {
    fn heuristic(&self, board: &Board) -> f32 {
        let mut others = 0.0;
        let mut own = 0.0;
        for pawn in board.pawns().iter() {
            let distance = board.shortest_route_for_pawn(pawn)
                .expect("Expected shortest route to be pre-computed")
                .length() as f32;

            if pawn.player_id() == &self.player_id {
                own = distance;
            } else {
                others += distance;
            }
        }
        others / (own * own * own)
    }

    fn clone(&self) -> Box<dyn Heuristic + Send> {
        Box::new(SimpleHeuristic::new(
            self.player_id.clone(),
            self.distance_checker.clone(),
        ))
    }
}
