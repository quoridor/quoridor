pub mod heuristic;

use crate::heuristic::{Heuristic, SimpleHeuristic};
use quoridor_core::board::{Board, GameMove, GameMoveByPlayer};
use quoridor_core::player::{Player, PlayerAction, PlayerId};
use quoridor_core::reachability_checker::astar::AStarChecker;
use rand::rngs::StdRng;
use rand::SeedableRng;
use std::cmp::Ordering;

const MAX_DEPTH: u8 = 3;

#[derive(Debug)]
pub struct AgentMinimax {
    id: PlayerId,
    name: String,
    random: StdRng,
    game_move: Option<GameMove>,
    heuristic: Box<dyn Heuristic + Send>,
}

impl Clone for AgentMinimax {
    fn clone(&self) -> Self {
        Self {
            id: self.id.clone(),
            name: self.name.clone(),
            random: self.random.clone(),
            game_move: self.game_move.clone(),
            heuristic: self.heuristic.clone(),
        }
    }
}

impl AgentMinimax {
    pub fn new(id: PlayerId, name: String) -> Self {
        Self {
            id: id.clone(),
            name,
            random: SeedableRng::from_entropy(),
            game_move: None,
            heuristic: Box::new(SimpleHeuristic::new(id, Box::new(AStarChecker::new()))),
        }
    }

    pub fn from_id(id: PlayerId) -> Self {
        Self {
            id: id.clone(),
            name: "MinimaxBot".to_owned(),
            random: SeedableRng::from_entropy(),
            game_move: None,
            heuristic: Box::new(SimpleHeuristic::new(id, Box::new(AStarChecker::new()))),
        }
    }

    fn make_move(&mut self, board: Board, depth: u8, mut alpha: f32, mut beta: f32) -> f32 {
        if depth >= MAX_DEPTH {
            return self.heuristic.heuristic(&board);
        }
        let moves = board.get_possible_moves(&self.id);

        // if our turn then max. when enemy turn then min
        let (comparator, mut cur_h_val, is_max): (fn(f32, f32) -> Ordering, f32, bool) =
            if board.next_move_by() == &self.id {
                (|f, s| f.partial_cmp(&s).unwrap(), -1.0, true)
            } else {
                (|f, s| f.partial_cmp(&s).unwrap().reverse(), 10000.0, false)
            };

        let mut max_vals = Vec::new();
        let self_id = self.id.clone();
        for (board, game_move) in moves
            .into_iter()
            .map(|game_move| {
                let mut board = board
                    .clone()
                    .apply_move_unchecked(GameMoveByPlayer::new(self_id.clone(), game_move));
                board.compute_shortest_routes();

                (
                    board,
                    game_move,
                )
            })
        {
            let h_val = self.make_move(board.clone(), depth + 1, alpha, beta);
            match comparator(h_val, cur_h_val) {
                Ordering::Greater => {
                    cur_h_val = h_val;
                    max_vals.clear();
                    max_vals.push(game_move);
                }
                Ordering::Equal => {
                    max_vals.push(game_move);
                }
                Ordering::Less => {}
            };
            if is_max {
                if cur_h_val >= beta {
                    break;
                }
                alpha = alpha.max(cur_h_val);
            } else {
                if cur_h_val <= alpha {
                    break;
                }
                beta = beta.min(cur_h_val);
            }
        }
        if depth == 0 {
            self.game_move = Some(max_vals[0]);
        }
        cur_h_val
    }
}

impl Player for AgentMinimax {
    fn id(&self) -> &PlayerId {
        &self.id
    }

    fn name(&self) -> String {
        self.name.clone()
    }

    fn perform_move(&mut self, board: &Board) -> PlayerAction {
        let mut board = board.clone();
        board.compute_shortest_routes();
        self.make_move(board, 0, f32::MIN, f32::MAX);
        PlayerAction::PerformMove(self.game_move.unwrap())
    }
}

#[cfg(test)]
pub mod tests {
    use crate::AgentMinimax;
    use quoridor_core::board::{Board, BoardPosition, Direction, Pawn};
    use quoridor_core::player::{Player, PlayerId};
    use quoridor_core::reachability_checker::astar::AStarChecker;
    use quoridor_core::reachability_checker::DistanceChecker;
    use std::collections::HashMap;
    use std::iter::FromIterator;

    #[test]
    fn test_start_bot_first() {
        let board = Board::construct(
            PlayerId::new(0),
            vec![],
            vec![
                Pawn::construct(PlayerId::new(0), Direction::North, BoardPosition::new(4, 0)),
                Pawn::construct(PlayerId::new(1), Direction::South, BoardPosition::new(4, 8)),
            ],
            HashMap::from_iter(vec![(PlayerId::new(0), 10), (PlayerId::new(1), 10)].into_iter()),
        );
        let checker = AStarChecker::new();
        let mut bot = AgentMinimax::from_id(PlayerId::new(0));

        let m = bot.perform_move(&board);
        println!("{:?}", m);
    }
}
